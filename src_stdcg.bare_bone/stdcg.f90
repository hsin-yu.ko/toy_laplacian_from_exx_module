subroutine stdcg(iter, n, eps, rho, pot)
    use laplacian_natan_kronik, only : drct_aa, drct_bc, nord2
    use laplacian_natan_kronik, only : output_prefactor
    use laplacian_natan_kronik, only : output_stencil_coefficients
    implicit none
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    real(8), parameter   :: pi = acos(-1.d0)
    integer, intent(out) :: iter
    integer, intent(in)  :: n(3)
    real(8), intent(in)  :: eps
    real(8), intent(in)  :: rho(n(1),n(2),n(3))
    real(8), intent(out) :: pot(n(1),n(2),n(3))
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- local variables ---
    !------------------------------------------------------------------------
    integer              :: itr
    !integer              :: nord2
    integer              :: npt
    integer              :: nd_d(6)
    integer              :: nb_d(6)
    real(8)              :: nro
    real(8)              :: nr
    real(8)              :: mnr
    real(8)              :: alfa
    real(8), allocatable :: x_d (:,:,:)
    real(8), allocatable :: r_d (:,:,:)
    real(8), allocatable :: d0_d(:,:,:)
    real(8), allocatable :: d1_d(:,:,:)
    !------------------------------------------------------------------------
    real(8), external :: ddot
    !
    real(8) :: prefac_(drct_aa:drct_bc)
    real(8) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    !
    ! prepare derivatives
    call output_prefactor(prefac_)
    call output_stencil_coefficients(stencil_coe)
    stencil_coe = stencil_coe/(-4.d0*pi) ! add the -1/4pi to stencil_coe


    !------------------------------------------------------------------------
    ! --- allocate arrays ---
    !------------------------------------------------------------------------
    !
    nd_d(1)=1
    nd_d(2)=1
    nd_d(3)=1
    nd_d(4)=n(1)
    nd_d(5)=n(2)
    nd_d(6)=n(3)
    !
    nb_d(1)=1-nord2
    nb_d(2)=1-nord2
    nb_d(3)=1-nord2
    nb_d(4)=n(1)+nord2
    nb_d(5)=n(2)+nord2
    nb_d(6)=n(3)+nord2
    !
    npt=(n(1)+2*nord2)*(n(2)+2*nord2)*(n(3)+2*nord2)
    !
    ALLOCATE( x_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE( r_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE(d0_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE(d1_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    !
    ! erho
    call set_boundary_condition
    d1_d = 0.d0
    call padx_new(nd_d,nb_d,prefac_,stencil_coe,x_d,d1_d)
    !
    !
    r_d = 0.d0; x_d = 0.d0;
    r_d(1:n(1),1:n(2),1:n(3))=rho(:,:,:) ! TODO: memcp in the future...
    !x_d(1:n(1),1:n(2),1:n(3))=pot(:,:,:) ! TODO: memcp in the future...
    !
    !erho part 2
    r_d(1:n(1),1:n(2),1:n(3)) = r_d(1:n(1),1:n(2),1:n(3)) - d1_d(1:n(1),1:n(2),1:n(3))  !erho part 2
    x_d = 0.d0
    x_d(1:n(1),1:n(2),1:n(3))=pot(:,:,:) ! TODO: memcp in the future...
    !
    ! better init those as well, because of thread exit condition it 
    ! could happen that boundaries are not initialized:
    d0_d = 0.d0; d1_d = 0.d0;
    !
    call padx_new(nd_d,nb_d,prefac_,stencil_coe,x_d,d1_d)
    !CALL PADX(nd_d,nb_d,coeke,x_d,d1_d)
    call Daxpy(npt,-1.0d0,d1_d,1,r_d,1) ! we get the initial residual in r_d ! step 1 : r = b - A * x;
    call Dcopy(npt, r_d, 1, d0_d, 1)    ! we copy the residual in d0_d       ! step 2 : p = r
    nro = Ddot(npt,r_d,1,r_d,1) ! we get the squared norm of the residual    ! step 3 : rsold = r' * r
    DO itr=0,1000 ! loop over the dimension of the problem                   ! step 4 : for i = 1:length(b)
        call padx_new(nd_d,nb_d,prefac_,stencil_coe,d0_d,d1_d)               ! step 5 : Ap = A * p; % p = d0_d; Ap = d1_d
        alfa=nro/Ddot(npt,d0_d,1,d1_d,1)                                     ! step 6 : alpha = rsold / (p' * Ap);
        call Daxpy(npt,alfa,d0_d,1,x_d,1)                                    ! step 7 : x = x + alpha * p;
        call Daxpy(npt,-alfa,d1_d,1,r_d,1)                                   ! step 8 : r = r - alpha * Ap;
        nr = Ddot(npt,r_d,1,r_d,1)                                           ! step 9 : rsnew = r' * r;
        IF (nr < eps*eps*(1.d0+nro)) EXIT                                    ! step 10-12 : if (converge) : break
        CALL Dscal(npt,nr/nro,d0_d,1)                                        ! step 13 : p = r + (rsnew / rsold) * p;
        call Daxpy(npt,1.d0,r_d,1,d0_d,1)                                    ! step 13 : p = r + (rsnew / rsold) * p;
        nro = nr
        print *,'cg: iter=',itr, 'nr =', nr
    END DO
    !
    pot(:,:,:)=x_d(1:n(1),1:n(2),1:n(3)) ! TODO may be memcp in the future...
    iter = itr
    !
    deallocate(x_d)
    deallocate(r_d)
    deallocate(d0_d)
    deallocate(d1_d)
    !
  contains

    subroutine  set_boundary_condition()
      use system, only : h, q_cen
      use grids, only : v_of_r
      implicit none
      integer :: ig1, ig2, ig3
      logical :: is_halo_1, is_halo_2, is_halo_3
      real(8) :: r_(3), r
      x_d = 0.d0
      do ig3 = nb_d(3), nb_d(6)
        is_halo_3 = .not.(ig3.ge.1.and.ig3.le.n(3))
        do ig2 = nb_d(2), nb_d(5)
          is_halo_2 = .not.(ig2.ge.1.and.ig2.le.n(2))
          do ig1 = nb_d(1), nb_d(4)
            is_halo_1 = .not.(ig1.ge.1.and.ig1.le.n(1))
            if (is_halo_1.or.is_halo_2.or.is_halo_3) then
              r_ = (/dble(ig1)/dble(n(1)),dble(ig2)/dble(n(2)),dble(ig3)/dble(n(3))/)
              r_ = matmul(h, r_)
              r = norm2(r_-q_cen)
              x_d(ig1,ig2,ig3) = v_of_r(r)
            end if
          end do ! ig1
        end do ! ig2
      end do ! ig3
      return
    end subroutine set_boundary_condition

end subroutine stdcg
!============================================================================
!       OP     PCG    FLOPS      MEM         DEP
! ------------------------------------------------
! [01] DOT              2N       2N
!
! [02] MV              37N       3N
! [03] DOT      *       2N       2N
! [04] DOT              2N       2N        02,03
! [05] AXPY             2N       3N        04
! [06] AXPY             2N       3N        04
! [07] CP               --       2N        06
! [08] SCAL     *        N       2N        07
! [09] FW       *      18N       2N        08 ! TODO
! [10] BW       *      18N       2N        09 ! TODO
! [11] CP       *       --       2N        10
! [12] DOT              2N       2N        09
! [13] AXPY             2N       3N        12

!SUBROUTINE PADX(nd,nb,coeke,d,Ad)
!    IMPLICIT NONE
!    REAL(8), PARAMETER    :: eps8 = 1.d-8
!    !------------------------------------------------------------------------
!    ! --- pass in variables ---
!    !------------------------------------------------------------------------
!    INTEGER     :: nd(6)
!    INTEGER     :: nb(6)
!    INTEGER     :: itr, jtr, ktr
!    REAL(8)    :: c00
!    REAL(8)    :: coeke(-3:3,3,3)
!    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
!    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
!    !------------------------------------------------------------------------
!    c00 = coeke(0,1,1)+coeke(0,2,2)+coeke(0,3,3)
!    itr = (blockIdx%x-1)*blockDim%x + threadIdx%x - 1 + nd(1)
!    jtr = (blockIdx%y-1)*blockDim%y + threadIdx%y - 1 + nd(2)
!    ktr = (blockIdx%z-1)*blockDim%z + threadIdx%z - 1 + nd(3)
!    if (itr.gt.nd(4)) RETURN
!    if (jtr.gt.nd(5)) RETURN
!    if (ktr.gt.nd(6)) RETURN
!    Ad(itr,jtr,ktr)=c00*d(itr,jtr,ktr) &
!      &             +coeke(1,1,1)*(d(itr-1,jtr,ktr)+d(itr+1,jtr,ktr)) &
!      &             +coeke(2,1,1)*(d(itr-2,jtr,ktr)+d(itr+2,jtr,ktr)) &
!      &             +coeke(3,1,1)*(d(itr-3,jtr,ktr)+d(itr+3,jtr,ktr)) &
!      &             +coeke(1,2,2)*(d(itr,jtr-1,ktr)+d(itr,jtr+1,ktr)) &
!      &             +coeke(2,2,2)*(d(itr,jtr-2,ktr)+d(itr,jtr+2,ktr)) &
!      &             +coeke(3,2,2)*(d(itr,jtr-3,ktr)+d(itr,jtr+3,ktr)) &
!      &             +coeke(1,3,3)*(d(itr,jtr,ktr-1)+d(itr,jtr,ktr+1)) &
!      &             +coeke(2,3,3)*(d(itr,jtr,ktr-2)+d(itr,jtr,ktr+2)) &
!      &             +coeke(3,3,3)*(d(itr,jtr,ktr-3)+d(itr,jtr,ktr+3))
!!-------------------------------------------------------------------------------
!    IF (ABS(coeke(1,1,2)) .GT. eps8) THEN
!      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr)&
!        & +coeke(1,1,2)*( d(itr+1,jtr+1,ktr)-d(itr+1,jtr-1,ktr)  &
!        &                -d(itr-1,jtr+1,ktr)+d(itr-1,jtr-1,ktr) )&
!        & +coeke(2,1,2)*( d(itr+2,jtr+2,ktr)-d(itr+2,jtr-2,ktr)  &
!        &                -d(itr-2,jtr+2,ktr)+d(itr-2,jtr-2,ktr) )&
!        & +coeke(3,1,2)*( d(itr+3,jtr+3,ktr)-d(itr+3,jtr-3,ktr)  &
!        &                -d(itr-3,jtr+3,ktr)+d(itr-3,jtr-3,ktr) )
!    END IF
!    IF (ABS(coeke(1,1,3)) .GT. eps8) THEN
!      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr) &
!        & +coeke(1,1,3)*( d(itr+1,jtr,ktr+1)-d(itr+1,jtr,ktr-1)  &
!        &                -d(itr-1,jtr,ktr+1)+d(itr-1,jtr,ktr-1) )&
!        & +coeke(2,1,3)*( d(itr+2,jtr,ktr+2)-d(itr+2,jtr,ktr-2)  &
!        &                -d(itr-2,jtr,ktr+2)+d(itr-2,jtr,ktr-2) )&
!        & +coeke(3,1,3)*( d(itr+3,jtr,ktr+3)-d(itr+3,jtr,ktr-3)  &
!        &                -d(itr-3,jtr,ktr+3)+d(itr-3,jtr,ktr-3) )
!    END IF
!    IF (ABS(coeke(1,2,3)) .GT. eps8) THEN
!      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr) &
!        & +coeke(1,2,3)*( d(itr,jtr+1,ktr+1)-d(itr,jtr+1,ktr-1)  &
!        &                -d(itr,jtr-1,ktr+1)+d(itr,jtr-1,ktr-1) )&
!        & +coeke(2,2,3)*( d(itr,jtr+2,ktr+2)-d(itr,jtr+2,ktr-2)  &
!        &                -d(itr,jtr-2,ktr+2)+d(itr,jtr-2,ktr-2) )&
!        & +coeke(3,2,3)*( d(itr,jtr+3,ktr+3)-d(itr,jtr+3,ktr-3)  &
!        &                -d(itr,jtr-3,ktr+3)+d(itr,jtr-3,ktr-3) )
!    END IF
!    return
!END SUBROUTINE PADX

subroutine padx_new(nd,nb,prefac_,stencil_coe,d,Ad)
    use laplacian_natan_kronik, only : drct_aa, drct_bc, nord2
    implicit none
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    real(8), intent(in) :: prefac_(drct_aa:drct_bc)
    real(8), intent(in) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !
    !integer, intent(in) :: g1l,g1u,g2l,g2u,g3l,g3u
    !real(8), intent(in) :: d(:,:,:)
    !real(8) :: Ad(:,:,:)
    real(8) :: v_stencil(-nord2:nord2)
    !real(8) :: v_stencil_2d(-nord2:nord2,drct_aa:drct_cc)
    real(8) :: action_tmp
    integer :: itr, jtr, ktr
    integer :: ii, id
    Ad = 0.d0
    do id = drct_aa, drct_bc
      if (abs(prefac_(id)).gt.1.d-8) then
        do ktr = nd(3), nd(6)
          do jtr = nd(2), nd(5)
            do itr = nd(1), nd(4)
              call fill_v_stencil_1d_stdcg(itr,jtr,ktr,id,nb,d,v_stencil)
              action_tmp = stencil_coe(0,id)*v_stencil(0)
              do ii = 1, nord2
                action_tmp = action_tmp &
                  & + stencil_coe(ii,id)*(v_stencil(ii)+v_stencil(-ii))
              end do ! ii
              Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + action_tmp
            end do ! itr
          end do ! jtr
        end do ! ktr
      end if
    end do ! id
    !Ad(g1l:g1u,g2l:g2u,g3l:g3u) = Ad(g1l:g1u,g2l:g2u,g3l:g3u)/(-4.d0*pi)
    return
end subroutine padx_new

subroutine  fill_v_stencil_1d_stdcg(ig1,ig2,ig3,id,nb,d,v_stencil)
  use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc, eta
  use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc, nord2
  !use grids, only : pot_stdcg
  implicit none
  integer, intent(in)  :: ig1, ig2, ig3, id
  integer     :: nb(6)
  REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
  real(8), intent(out) :: v_stencil(-nord2:nord2)
  integer :: ii
  select case (id)
  case (drct_aa)
    do ii = -nord2,nord2
      v_stencil(ii) = d(ig1+ii, ig2   , ig3   )
    end do ! ii
  case (drct_bb)
    do ii = -nord2,nord2
      v_stencil(ii) = d(ig1   , ig2+ii, ig3   )
    end do ! ii
  case (drct_cc)
    do ii = -nord2,nord2
      v_stencil(ii) = d(ig1   , ig2   , ig3+ii)
    end do ! ii
  case (drct_ab)
    if (eta(drct_ab).gt.0.d0) then
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2+ii, ig3   )
      end do ! ii
    else
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2-ii, ig3   )
      end do ! ii
    end if
  case (drct_ac)
    if (eta(drct_ac).gt.0.d0) then
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2   , ig3+ii)
      end do ! ii
    else
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2   , ig3-ii)
      end do ! ii
    end if
  case (drct_bc)
    if (eta(drct_bc).gt.0.d0) then
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1   , ig2+ii, ig3+ii)
      end do ! ii
    else
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1   , ig2+ii, ig3-ii)
      end do ! ii
    end if
  case default
  end select ! id
  return
end subroutine fill_v_stencil_1d_stdcg
