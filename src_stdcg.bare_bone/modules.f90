module constants
  implicit none
  save
  real(8), parameter :: pi=acos(-1.d0)
  real(8), parameter :: eps8=1.d-8
end module constants

module system
  implicit none
  save
  real(8) :: h(3,3)
  real(8) :: ainv(3,3)
  real(8) :: volume
  real(8) :: q_cen(3)
  real(8) :: q_sig
  real(8), parameter :: q = 1.d0
contains

  subroutine  init_system()
    implicit none
    call read_charge_input
    call read_cell_input
    call inv3x3_mat(h, ainv, volume)
    return
  end subroutine init_system

  subroutine  read_charge_input()
    implicit none
    open(unit=100, file='charge.dat', action='read')
    read(100,*) q_cen(:)
    read(100,*) q_sig
    close(100)
    return
  end subroutine read_charge_input

  subroutine  read_cell_input()
    implicit none
    open(unit=100, file='cel.dat', action='read')
    read(100,*) h(1,:)
    read(100,*) h(2,:)
    read(100,*) h(3,:)
    close(100)
    return
  end subroutine read_cell_input

  subroutine inv3x3_mat(A,T,D)
    implicit none 
    real(8), intent(in)  :: A(3,3)
    real(8), intent(out) :: T(3,3)
    real(8), intent(out) :: D           ! det
    D = A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)- &
      A(1,2)*A(2,1)*A(3,3)+A(1,2)*A(2,3)*A(3,1)+ &
      A(1,3)*A(2,1)*A(3,2)-A(1,3)*A(2,2)*A(3,1)
    T(1,1) = (A(2,2)*A(3,3)-A(2,3)*A(3,2))/D
    T(1,2) = -(A(1,2)*A(3,3)-A(1,3)*A(3,2))/D
    T(1,3) = (A(1,2)*A(2,3)-A(1,3)*A(2,2))/D
    T(2,1) = -(A(2,1)*A(3,3)-A(2,3)*A(3,1))/D
    T(2,2) = (A(1,1)*A(3,3)-A(1,3)*A(3,1))/D
    T(2,3) = -(A(1,1)*A(2,3)-A(1,3)*A(2,1))/D
    T(3,1) = (A(2,1)*A(3,2)-A(2,2)*A(3,1))/D
    T(3,2) = -(A(1,1)*A(3,2)-A(1,2)*A(3,1))/D
    T(3,3) = (A(1,1)*A(2,2)-A(1,2)*A(2,1))/D
    return
  end subroutine inv3x3_mat 
end module system

module grids
  use constants
  use system
  implicit none
  save
  integer :: nr1
  integer :: nr2
  integer :: nr3
  real(8) :: dr1, dr2, dr3
  real(8), allocatable :: pos(:,:,:,:)
  real(8), allocatable :: r  (:,:,:)
  real(8), allocatable :: r2 (:,:,:)
  real(8), allocatable :: rho(:,:,:)
  real(8), allocatable :: pot(:,:,:)
  real(8), allocatable :: pot_stdcg(:,:,:)
  real(8), allocatable :: rho_nk(:,:,:)
  real(8), allocatable :: rho_ours(:,:,:)
contains

  subroutine  init_grids()
    implicit none
    call read_grid_input
    call alloc_grids
    call setup_grids
    return
  end subroutine init_grids

  subroutine  read_grid_input()
    implicit none
    open(unit=100, file='ngrid.dat', action='read')
    read(100,*) nr1, nr2, nr3
    close(100)
    return
  end subroutine read_grid_input

  subroutine  alloc_grids()
    implicit none
    allocate(pos   (3,nr1,nr2,nr3)); pos=0.d0
    allocate(r       (nr1,nr2,nr3)); r=0.d0
    allocate(r2      (nr1,nr2,nr3)); r2=0.d0
    allocate(rho     (nr1,nr2,nr3)); rho=0.d0
    allocate(pot     (nr1,nr2,nr3)); pot=0.d0
    allocate(pot_stdcg(nr1,nr2,nr3)); pot_stdcg=0.d0
    allocate(rho_nk  (nr1,nr2,nr3)); rho_nk=0.d0
    allocate(rho_ours(nr1,nr2,nr3)); rho_ours=0.d0
    return
  end subroutine alloc_grids

  subroutine  setup_grids()
    implicit none
    call compute_grid_spacing
    call compute_position_grid
    call compute_radial_grid
    call compute_density_grid
    call compute_potential_grid
    return
  end subroutine setup_grids

  subroutine  compute_grid_spacing()
    implicit none
    dr1 = sqrt(sum(h(:,1)**2))/dble(nr1)
    dr2 = sqrt(sum(h(:,2)**2))/dble(nr2)
    dr3 = sqrt(sum(h(:,3)**2))/dble(nr3)
    return
  end subroutine compute_grid_spacing

  subroutine  compute_position_grid()
    implicit none
    integer :: ig1, ig2, ig3
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          pos(1,ig1,ig2,ig3) = ig1
          pos(2,ig1,ig2,ig3) = ig2
          pos(3,ig1,ig2,ig3) = ig3
        end do ! ig1
      end do ! ig2
    end do ! ig3
    call transform_to_reduced_space
    call reduced_space_to_real_space
    return
  end subroutine compute_position_grid

  subroutine  transform_to_reduced_space()
    implicit none
    pos(1,:,:,:) = pos(1,:,:,:)/dble(nr1)
    pos(2,:,:,:) = pos(2,:,:,:)/dble(nr2)
    pos(3,:,:,:) = pos(3,:,:,:)/dble(nr3)
    return
  end subroutine transform_to_reduced_space

  subroutine  reduced_space_to_real_space()
    implicit none
    integer :: ig1, ig2, ig3
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          pos(:,ig1,ig2,ig3) = matmul(h, pos(:,ig1,ig2,ig3))
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine reduced_space_to_real_space

  subroutine  compute_radial_grid()
    implicit none
    r2 =(pos(1,:,:,:)-q_cen(1))**2 &
      &+(pos(2,:,:,:)-q_cen(2))**2 &
      &+(pos(3,:,:,:)-q_cen(3))**2
    r = sqrt(r2)
    return
  end subroutine compute_radial_grid

  subroutine  compute_density_grid()
    implicit none
    real(8) :: fac1, fac3
    fac1 = q_sig*sqrt(2.d0*pi)
    fac3 = fac1*fac1*fac1
    rho = q/fac3*exp(-r2/(2.d0*q_sig*q_sig))
    return
  end subroutine compute_density_grid
  
  subroutine  compute_potential_grid()
    implicit none
    pot = v_of_r(r)
    return
  end subroutine compute_potential_grid

  elemental real(8) function v_of_r(ri)
    implicit none
    real(8), intent(in) :: ri
    if (ri.gt.eps8) then
      v_of_r = q/ri*erf(ri/(sqrt(2.d0)*q_sig)) !gaussian unit
    else
      v_of_r = q*sqrt(2.d0/pi)/q_sig
    end if ! r.lt.eps8
  end function v_of_r

  subroutine  dealloc_grids()
    implicit none
    deallocate(pos      )
    deallocate(r        )
    deallocate(r2       )
    deallocate(rho      )
    deallocate(pot      )
    deallocate(pot_stdcg)
    deallocate(rho_nk   )
    deallocate(rho_ours )
    return
  end subroutine dealloc_grids
end module grids
