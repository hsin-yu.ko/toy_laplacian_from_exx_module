!============================================================================
SUBROUTINE CGMIC(iter, n, eps, fbsscale, coemicf, coeke, rho, pot)
    USE kinds,  ONLY : DP
    IMPLICIT NONE
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    INTEGER                    :: iter
    INTEGER                    :: n(3)
    REAL(DP)                   :: eps
    REAL(DP)                   :: fbsscale
    REAL(DP)                   :: coemicf(-3:3,3,3)
    REAL(DP)                   :: coeke(-3:3,3,3)
    REAL(DP)                   :: rho(n(1),n(2),n(3))
    REAL(DP)                   :: pot(n(1),n(2),n(3))
    !------------------------------------------------------------------------


    !------------------------------------------------------------------------
    ! --- local variables ---
    !------------------------------------------------------------------------
    INTEGER                    :: itr,jtr,ktr
    INTEGER                    :: nx,ny,nz
    INTEGER                    :: nord2
    INTEGER                    :: npt
    INTEGER                    :: nd(6)
    INTEGER                    :: nb(6)
    REAL(DP)                   :: nro
    REAL(DP)                   :: nr
    REAL(DP)                   :: mnr
    REAL(DP)                   :: alfa
    REAL(DP)                   :: beta
    REAL(DP), ALLOCATABLE      :: x(:,:,:)
    REAL(DP), ALLOCATABLE      :: r(:,:,:)
    REAL(DP), ALLOCATABLE      :: z(:,:,:)   ! z = M-1 r
    REAL(DP), ALLOCATABLE      :: d0(:,:,:)
    REAL(DP), ALLOCATABLE      :: d1(:,:,:)
    !------------------------------------------------------------------------


    !------------------------------------------------------------------------
    ! --- external function ---
    !------------------------------------------------------------------------
    REAL(DP)                   :: PDDOT
    INTEGER                    :: OMP_GET_NUM_THREADS
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- allocate arrays ---
    !------------------------------------------------------------------------
    nord2=3
    !
    nd(1)=1                                      
    nd(2)=1         
    nd(3)=1         
    nd(4)=n(1)      
    nd(5)=n(2)      
    nd(6)=n(3)      
    !
    nb(1)=1-nord2   
    nb(2)=1-nord2   
    nb(3)=1-nord2   
    nb(4)=n(1)+nord2
    nb(5)=n(2)+nord2
    nb(6)=n(3)+nord2
    !
    npt=(nb(4)-nb(1)+1)*(nb(5)-nb(2)+1)*(nb(6)-nb(3)+1)
    !
    ALLOCATE( x(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    ALLOCATE( r(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    ALLOCATE( z(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    ALLOCATE(d0(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    ALLOCATE(d1(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    !
    x=0.0D0
    r=0.0D0
    z=0.0D0
    d0=0.0D0
    d1=0.0D0
    !
    r(nd(1):nd(4),nd(2):nd(5),nd(3):nd(6))=rho(:,:,:)
    x(nd(1):nd(4),nd(2):nd(5),nd(3):nd(6))=pot(:,:,:)
    !
    CALL PADX(nd,nb,coeke,x,d1)
    CALL PDAXPY(nd,nb,-1.0D0,d1,r)
    CALL DCOPY(npt,r,1,d0,1)
    CALL DSCAL(npt,fbsscale,d0,1)
    CALL FWDSUB(nd,nb,coemicf,d0)
    CALL BKDSUB(nd,nb,coemicf,d0)
    CALL DCOPY(npt,d0,1,z,1)
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- main iteration ---
    !------------------------------------------------------------------------
    iter=0
    nro=PDDOT(nd,nb,r,r)
    !
    DO itr=0,1000
        !                                       !       OP     PCG    FLOPS      MEM         DEP
        iter=iter+1                             ! ------------------------------------------------
        nr  =PDDOT(nd,nb,r,r)                   ! [01] DOT              2N       2N
        IF (nr < eps*eps*(1+nro)) EXIT          !
        CALL PADX(nd,nb,coeke,d0,d1)            ! [02] MV              37N       3N
        mnr=PDDOT(nd,nb,r,z)                    ! [03] DOT      *       2N       2N
        alfa=mnr/PDDOT(nd,nb,d0,d1)             ! [04] DOT              2N       2N        02,03
        CALL PDAXPY(nd,nb,alfa,d0,x)            ! [05] AXPY             2N       3N        04
        CALL PDAXPY(nd,nb,-alfa,d1,r)           ! [06] AXPY             2N       3N        04
        CALL DCOPY(npt,r,1,d1,1)                ! [07] CP               --       2N        06
        CALL DSCAL(npt,fbsscale,d1,1)           ! [08] SCAL     *        N       2N        07
        CALL FWDSUB(nd,nb,coemicf,d1)           ! [09] FW       *      18N       2N        08
        CALL BKDSUB(nd,nb,coemicf,d1)           ! [10] BW       *      18N       2N        09
        CALL DCOPY(npt,d1,1,z,1)                ! [11] CP       *       --       2N        10
        beta=PDDOT(nd,nb,r,d1)/mnr              ! [12] DOT              2N       2N        09
        CALL PDAXPY(nd,nb,beta,d0,d1)           ! [13] AXPY             2N       3N        12
        !
        iter=iter+1
        nr  =PDDOT(nd,nb,r,r)            
        IF (nr < eps*eps*(1+nro)) EXIT
        CALL PADX(nd,nb,coeke,d1,d0) 
        mnr=PDDOT(nd,nb,r,z)
        alfa=mnr/PDDOT(nd,nb,d1,d0)
        CALL PDAXPY(nd,nb,alfa,d1,x)
        CALL PDAXPY(nd,nb,-alfa,d0,r)
        CALL DCOPY(npt,r,1,d0,1)
        CALL DSCAL(npt,fbsscale,d0,1)
        CALL FWDSUB(nd,nb,coemicf,d0)
        CALL BKDSUB(nd,nb,coemicf,d0)
        CALL DCOPY(npt,d0,1,z,1)
        beta=PDDOT(nd,nb,r,d0)/mnr
        CALL PDAXPY(nd,nb,beta,d1,d0)
        !
    END DO
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! print error
    !------------------------------------------------------------------------
    r(nd(1):nd(4),nd(2):nd(5),nd(3):nd(6))=rho(:,:,:)
    CALL PADX(nd,nb,coeke,x,d0)
    CALL PDAXPY(nd,nb,-1.0D0,d0,r)
    !------------------------------------------------------------------------
    ! WRITE(*,"(A, E15.7, A, I4, A)") "error: ", DSQRT(PDDOT(nd,nb,r,r)), "  in", iter, "  steps"
    !------------------------------------------------------------------------

    pot(:,:,:)=x(1:n(1),1:n(2),1:n(3))

    DEALLOCATE(x)
    DEALLOCATE(r)
    DEALLOCATE(z)
    DEALLOCATE(d0)
    DEALLOCATE(d1)
END SUBROUTINE cgmic
!============================================================================
