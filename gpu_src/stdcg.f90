subroutine stdcg(iter, n, eps, rho, pot, dump_A)
    use laplacian_natan_kronik, only : drct_aa, drct_bc, nord2
    use laplacian_natan_kronik, only : output_prefactor
    use laplacian_natan_kronik, only : output_stencil_coefficients
    use nvtx
    implicit none
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    real(8), parameter   :: pi = acos(-1.d0)
    integer, intent(out) :: iter
    integer, intent(in)  :: n(3)
    real(8), intent(in)  :: eps
    real(8), intent(in)  :: rho(n(1),n(2),n(3))
    real(8), intent(out) :: pot(n(1),n(2),n(3))
    logical, intent(in)  :: dump_A
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- local variables ---
    !------------------------------------------------------------------------
    integer              :: itr
    integer              :: npt
    integer              :: nd_d(6)
    integer              :: nb_d(6)
    real(8)              :: nro
    real(8)              :: nr
    real(8)              :: mnr
    real(8)              :: alfa
    real(8), allocatable :: x_d (:,:,:)
    real(8), allocatable :: r_d (:,:,:)
    real(8), allocatable :: d0_d(:,:,:)
    real(8), allocatable :: d1_d(:,:,:)
    !------------------------------------------------------------------------
    real(8), external :: ddot
    !
    real(8) :: prefac_(drct_aa:drct_bc)
    real(8) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    real(8), allocatable :: Amat(:,:)
    !
    ! prepare derivatives
    call output_prefactor(prefac_)
    call output_stencil_coefficients(stencil_coe)
    stencil_coe = stencil_coe/(-4.d0*pi) ! add the -1/4pi to stencil_coe


    !------------------------------------------------------------------------
    ! --- allocate arrays ---
    !------------------------------------------------------------------------
    !
    nd_d(1)=1
    nd_d(2)=1
    nd_d(3)=1
    nd_d(4)=n(1)
    nd_d(5)=n(2)
    nd_d(6)=n(3)
    !
    nb_d(1)=1-nord2
    nb_d(2)=1-nord2
    nb_d(3)=1-nord2
    nb_d(4)=n(1)+nord2
    nb_d(5)=n(2)+nord2
    nb_d(6)=n(3)+nord2
    !
    npt=(n(1)+2*nord2)*(n(2)+2*nord2)*(n(3)+2*nord2)
    !
    ALLOCATE( x_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE( r_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE(d0_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE(d1_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    !
    ! erho
    call set_boundary_condition
    d1_d = 0.d0
    if (dump_A) then
      allocate(Amat(product(n),product(n))); Amat = 0.d0
      call padx_new_get_action(nd_d,nb_d,prefac_,stencil_coe,x_d,Amat,d1_d)
      call output_A
    else
      call padx_new(nd_d,nb_d,prefac_,stencil_coe,x_d,d1_d)
    end if
    !
    r_d = 0.d0; x_d = 0.d0;
    r_d(1:n(1),1:n(2),1:n(3))=rho(:,:,:) ! TODO: memcp in the future...
    x_d(1:n(1),1:n(2),1:n(3))=pot(:,:,:) ! TODO: memcp in the future...
    !
    !erho part 2
    r_d(1:n(1),1:n(2),1:n(3)) = r_d(1:n(1),1:n(2),1:n(3)) - d1_d(1:n(1),1:n(2),1:n(3))  !erho part 2
    x_d = 0.d0
    x_d(1:n(1),1:n(2),1:n(3)) = pot(:,:,:) ! TODO: memcp in the future...
    !
    ! better init those as well, because of thread exit condition it 
    ! could happen that boundaries are not initialized:
    d0_d = 0.d0; d1_d = 0.d0;
    !
    if (dump_A) then
      call padx_new_use_A(nd_d,nb_d,prefac_,stencil_coe,x_d,Amat,d1_d)
    else
      call padx_new(nd_d,nb_d,prefac_,stencil_coe,x_d,d1_d)
    end if
    !CALL PADX(nd_d,nb_d,coeke,x_d,d1_d)
    call Daxpy(npt,-1.0d0,d1_d,1,r_d,1) ! we get the initial residual in r_d ! step 1 : r = b - A * x;
    call Dcopy(npt, r_d, 1, d0_d, 1)    ! we copy the residual in d0_d       ! step 2 : p = r
    nro = Ddot(npt,r_d,1,r_d,1) ! we get the squared norm of the residual    ! step 3 : rsold = r' * r
    DO itr=0,1000 ! loop over the dimension of the problem                   ! step 4 : for i = 1:length(b)
      call nvtxStartRange("Stdcg",itr)
      call nvtxStartRange("padx")
      if (dump_A) then
        call padx_new_use_A(nd_d,nb_d,prefac_,stencil_coe,d0_d,Amat,d1_d)    ! step 5 : Ap = A * p; % p = d0_d; Ap = d1_d
      else
        call padx_new(nd_d,nb_d,prefac_,stencil_coe,d0_d,d1_d)
      end if
        call nvtxEndRange
        alfa=nro/Ddot(npt,d0_d,1,d1_d,1)                                     ! step 6 : alpha = rsold / (p' * Ap);
        call Daxpy(npt,alfa,d0_d,1,x_d,1)                                    ! step 7 : x = x + alpha * p;
        call Daxpy(npt,-alfa,d1_d,1,r_d,1)                                   ! step 8 : r = r - alpha * Ap;
        nr = Ddot(npt,r_d,1,r_d,1)                                           ! step 9 : rsnew = r' * r;
        IF (nr < eps*eps*(1.d0+nro)) EXIT                                    ! step 10-12 : if (converge) : break
        CALL Dscal(npt,nr/nro,d0_d,1)                                        ! step 13 : p = r + (rsnew / rsold) * p;
        call Daxpy(npt,1.d0,r_d,1,d0_d,1)                                    ! step 13 : p = r + (rsnew / rsold) * p;
        nro = nr
        print *,'cg: iter=',itr, 'nr =', nr
        call nvtxEndRange
    END DO
    !
    pot(:,:,:)=x_d(1:n(1),1:n(2),1:n(3)) ! TODO may be memcp in the future...
    iter = itr
    !
    deallocate(x_d)
    deallocate(r_d)
    deallocate(d0_d)
    deallocate(d1_d)
    if (allocated(Amat)) deallocate(Amat)
    !
  contains

    subroutine  set_boundary_condition()
      use system, only : h, q_cen
      use grids, only : v_of_r
      implicit none
      integer :: ig1, ig2, ig3
      logical :: is_halo_1, is_halo_2, is_halo_3
      real(8) :: r_(3), r
      x_d = 0.d0
      do ig3 = nb_d(3), nb_d(6)
        is_halo_3 = .not.(ig3.ge.1.and.ig3.le.n(3))
        do ig2 = nb_d(2), nb_d(5)
          is_halo_2 = .not.(ig2.ge.1.and.ig2.le.n(2))
          do ig1 = nb_d(1), nb_d(4)
            is_halo_1 = .not.(ig1.ge.1.and.ig1.le.n(1))
            if (is_halo_1.or.is_halo_2.or.is_halo_3) then
              r_ = (/dble(ig1)/dble(n(1)),dble(ig2)/dble(n(2)),dble(ig3)/dble(n(3))/)
              r_ = matmul(h, r_)
              !r = norm2(r_-q_cen)
              r=sqrt((r_(1)-q_cen(1))**2 +(r_(2)-q_cen(2))**2 +(r_(3)-q_cen(3))**2 )
              x_d(ig1,ig2,ig3) = v_of_r(r)
            end if
          end do ! ig1
        end do ! ig2
      end do ! ig3
      return
    end subroutine set_boundary_condition

    subroutine  output_A()
      implicit none
      integer :: nnz, idx, odx, ntot
      ntot = product(n)
      nnz = 0
      do idx = 1, ntot
        do odx = 1, ntot
          if (abs(Amat(odx,idx)).gt.1.d-8) then
            !write(1500,*) odx, idx, Amat(odx,idx)
            nnz = nnz+1
          end if
        end do ! odx
      end do ! idx
      open(unit=1500,file='A.dat',action='write')
      write(1500,'("# name: A")')
      write(1500,'("# type: sparse matrix")')
      write(1500,'("# nnz: ", I9)') nnz
      write(1500,'("# rows: ", I9)') ntot
      write(1500,'("# columns: ", I9)') ntot
      do idx = 1, ntot
        do odx = 1, ntot
          if (abs(Amat(odx,idx)).gt.1.d-8) then
            write(1500,*) odx, idx, Amat(odx,idx)
          end if
        end do ! odx
      end do ! idx
      close(1500)
      !
      return
    end subroutine output_A

end subroutine stdcg
!============================================================================
!       OP     PCG    FLOPS      MEM         DEP
! ------------------------------------------------
! [01] DOT              2N       2N
!
! [02] MV              37N       3N
! [03] DOT      *       2N       2N
! [04] DOT              2N       2N        02,03
! [05] AXPY             2N       3N        04
! [06] AXPY             2N       3N        04
! [07] CP               --       2N        06
! [08] SCAL     *        N       2N        07
! [09] FW       *      18N       2N        08 ! TODO
! [10] BW       *      18N       2N        09 ! TODO
! [11] CP       *       --       2N        10
! [12] DOT              2N       2N        09
! [13] AXPY             2N       3N        12

!SUBROUTINE PADX(nd,nb,coeke,d,Ad)
!    IMPLICIT NONE
!    REAL(8), PARAMETER    :: eps8 = 1.d-8
!    !------------------------------------------------------------------------
!    ! --- pass in variables ---
!    !------------------------------------------------------------------------
!    INTEGER     :: nd(6)
!    INTEGER     :: nb(6)
!    INTEGER     :: itr, jtr, ktr
!    REAL(8)    :: c00
!    REAL(8)    :: coeke(-3:3,3,3)
!    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
!    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
!    !------------------------------------------------------------------------
!    c00 = coeke(0,1,1)+coeke(0,2,2)+coeke(0,3,3)
!    itr = (blockIdx%x-1)*blockDim%x + threadIdx%x - 1 + nd(1)
!    jtr = (blockIdx%y-1)*blockDim%y + threadIdx%y - 1 + nd(2)
!    ktr = (blockIdx%z-1)*blockDim%z + threadIdx%z - 1 + nd(3)
!    if (itr.gt.nd(4)) RETURN
!    if (jtr.gt.nd(5)) RETURN
!    if (ktr.gt.nd(6)) RETURN
!    Ad(itr,jtr,ktr)=c00*d(itr,jtr,ktr) &
!      &             +coeke(1,1,1)*(d(itr-1,jtr,ktr)+d(itr+1,jtr,ktr)) &
!      &             +coeke(2,1,1)*(d(itr-2,jtr,ktr)+d(itr+2,jtr,ktr)) &
!      &             +coeke(3,1,1)*(d(itr-3,jtr,ktr)+d(itr+3,jtr,ktr)) &
!      &             +coeke(1,2,2)*(d(itr,jtr-1,ktr)+d(itr,jtr+1,ktr)) &
!      &             +coeke(2,2,2)*(d(itr,jtr-2,ktr)+d(itr,jtr+2,ktr)) &
!      &             +coeke(3,2,2)*(d(itr,jtr-3,ktr)+d(itr,jtr+3,ktr)) &
!      &             +coeke(1,3,3)*(d(itr,jtr,ktr-1)+d(itr,jtr,ktr+1)) &
!      &             +coeke(2,3,3)*(d(itr,jtr,ktr-2)+d(itr,jtr,ktr+2)) &
!      &             +coeke(3,3,3)*(d(itr,jtr,ktr-3)+d(itr,jtr,ktr+3))
!!-------------------------------------------------------------------------------
!    IF (ABS(coeke(1,1,2)) .GT. eps8) THEN
!      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr)&
!        & +coeke(1,1,2)*( d(itr+1,jtr+1,ktr)-d(itr+1,jtr-1,ktr)  &
!        &                -d(itr-1,jtr+1,ktr)+d(itr-1,jtr-1,ktr) )&
!        & +coeke(2,1,2)*( d(itr+2,jtr+2,ktr)-d(itr+2,jtr-2,ktr)  &
!        &                -d(itr-2,jtr+2,ktr)+d(itr-2,jtr-2,ktr) )&
!        & +coeke(3,1,2)*( d(itr+3,jtr+3,ktr)-d(itr+3,jtr-3,ktr)  &
!        &                -d(itr-3,jtr+3,ktr)+d(itr-3,jtr-3,ktr) )
!    END IF
!    IF (ABS(coeke(1,1,3)) .GT. eps8) THEN
!      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr) &
!        & +coeke(1,1,3)*( d(itr+1,jtr,ktr+1)-d(itr+1,jtr,ktr-1)  &
!        &                -d(itr-1,jtr,ktr+1)+d(itr-1,jtr,ktr-1) )&
!        & +coeke(2,1,3)*( d(itr+2,jtr,ktr+2)-d(itr+2,jtr,ktr-2)  &
!        &                -d(itr-2,jtr,ktr+2)+d(itr-2,jtr,ktr-2) )&
!        & +coeke(3,1,3)*( d(itr+3,jtr,ktr+3)-d(itr+3,jtr,ktr-3)  &
!        &                -d(itr-3,jtr,ktr+3)+d(itr-3,jtr,ktr-3) )
!    END IF
!    IF (ABS(coeke(1,2,3)) .GT. eps8) THEN
!      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr) &
!        & +coeke(1,2,3)*( d(itr,jtr+1,ktr+1)-d(itr,jtr+1,ktr-1)  &
!        &                -d(itr,jtr-1,ktr+1)+d(itr,jtr-1,ktr-1) )&
!        & +coeke(2,2,3)*( d(itr,jtr+2,ktr+2)-d(itr,jtr+2,ktr-2)  &
!        &                -d(itr,jtr-2,ktr+2)+d(itr,jtr-2,ktr-2) )&
!        & +coeke(3,2,3)*( d(itr,jtr+3,ktr+3)-d(itr,jtr+3,ktr-3)  &
!        &                -d(itr,jtr-3,ktr+3)+d(itr,jtr-3,ktr-3) )
!    END IF
!    return
!END SUBROUTINE PADX

subroutine padx_new(nd,nb,prefac_,stencil_coe,d,Ad)
    use laplacian_natan_kronik, only : drct_aa, drct_bc, nord2
    implicit none
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    real(8), intent(in) :: prefac_(drct_aa:drct_bc)
    real(8), intent(in) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !
    real(8) :: v_stencil(-nord2:nord2)
    real(8) :: action_tmp
    integer :: itr, jtr, ktr
    integer :: ii, id
    Ad = 0.d0
    do id = drct_aa, drct_bc
      if (abs(prefac_(id)).gt.1.d-8) then
        do ktr = nd(3), nd(6)
          do jtr = nd(2), nd(5)
            do itr = nd(1), nd(4)
              call fill_v_stencil_1d_stdcg(itr,jtr,ktr,id,nb,d,v_stencil)
              action_tmp = stencil_coe(0,id)*v_stencil(0)
              do ii = 1, nord2
                action_tmp = action_tmp &
                  & + stencil_coe(ii,id)*(v_stencil(ii)+v_stencil(-ii))
              end do ! ii
              Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + action_tmp
            end do ! itr
          end do ! jtr
        end do ! ktr
      end if
    end do ! id
    return
end subroutine padx_new

subroutine padx_new_get_action(nd,nb,prefac_,stencil_coe,d,A,Ad)
  use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc, eta
  use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc, nord2
    implicit none
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    real(8), intent(in) :: prefac_(drct_aa:drct_bc)
    real(8), intent(in) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    REAL(8)    :: A((nd(4)-nd(1)+1)*(nd(5)-nd(2)+1)*(nd(6)-nd(3)+1), &
    &               (nd(4)-nd(1)+1)*(nd(5)-nd(2)+1)*(nd(6)-nd(3)+1))
    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !
    !integer, intent(in) :: g1l,g1u,g2l,g2u,g3l,g3u
    !real(8), intent(in) :: d(:,:,:)
    !real(8) :: Ad(:,:,:)
    real(8) :: v_stencil(-nord2:nord2)
    !real(8) :: v_stencil_2d(-nord2:nord2,drct_aa:drct_cc)
    real(8) :: action_tmp
    integer :: itr, jtr, ktr
    integer :: ii, id
    integer :: odx, idx, n1, n2, n3, n12
    logical :: is_in_box
    Ad = 0.d0
    n1 = (nd(4)-nd(1)+1)
    n2 = (nd(5)-nd(2)+1)
    n3 = (nd(6)-nd(3)+1)
    n12 = n1*n2
    do id = drct_aa, drct_bc
      if (abs(prefac_(id)).gt.1.d-8) then
        do ktr = nd(3), nd(6)
          do jtr = nd(2), nd(5)
            do itr = nd(1), nd(4)
              odx = itr+(jtr-1)*n1+(ktr-1)*n12 
              do ii = -nord2, nord2
                select case (id)
                case (drct_aa)
                  Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                    & + stencil_coe(ii,id)*(d(itr+ii, jtr, ktr))
                  idx = itr+ii+(jtr-1)*n1+(ktr-1)*n12 
                  is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1)
                case (drct_bb)
                  Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                    & + stencil_coe(ii,id)*(d(itr, jtr+ii, ktr))
                  idx = itr+(jtr+ii-1)*n1+(ktr-1)*n12 
                  is_in_box = (jtr+ii.ge.1).and.(jtr+ii.le.n2)
                case (drct_cc)
                  Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                    & + stencil_coe(ii,id)*(d(itr, jtr, ktr+ii))
                  idx = itr+(jtr-1)*n1+(ktr+ii-1)*n12 
                  is_in_box = (ktr+ii.ge.1).and.(ktr+ii.le.n3)
                case (drct_ab)
                  if (eta(drct_ab).gt.0.d0) then
                    Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                      & + stencil_coe(ii,id)*(d(itr+ii, jtr+ii, ktr))
                    idx = itr+ii+(jtr+ii-1)*n1+(ktr-1)*n12 
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(jtr+ii.ge.1).and.(jtr+ii.le.n2)
                  else
                    Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                      & + stencil_coe(ii,id)*(d(itr+ii, jtr-ii, ktr))
                    idx = itr+ii+(jtr-ii-1)*n1+(ktr-1)*n12 
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(jtr-ii.ge.1).and.(jtr-ii.le.n2)
                  end if
                case (drct_ac)
                  if (eta(drct_ac).gt.0.d0) then
                    Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                      & + stencil_coe(ii,id)*(d(itr+ii, jtr, ktr+ii))
                    idx = itr+ii+(jtr-1)*n1+(ktr+ii-1)*n12 
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(ktr+ii.ge.1).and.(ktr+ii.le.n3)
                  else
                    Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                      & + stencil_coe(ii,id)*(d(itr+ii, jtr, ktr-ii))
                    idx = itr+ii+(jtr-1)*n1+(ktr-ii-1)*n12 
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(ktr-ii.ge.1).and.(ktr-ii.le.n3)
                  end if
                case (drct_bc)
                  if (eta(drct_bc).gt.0.d0) then
                    Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                      & + stencil_coe(ii,id)*(d(itr, jtr+ii, ktr+ii))
                    idx = itr+(jtr+ii-1)*n1+(ktr+ii-1)*n12 
                    is_in_box = (jtr+ii.ge.1).and.(jtr+ii.le.n2) &
                      &    .and.(ktr+ii.ge.1).and.(ktr+ii.le.n3)
                  else
                    Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                      & + stencil_coe(ii,id)*(d(itr, jtr+ii, ktr-ii))
                    idx = itr+(jtr+ii-1)*n1+(ktr-ii-1)*n12 
                    is_in_box = (jtr+ii.ge.1).and.(jtr+ii.le.n2) &
                      &    .and.(ktr-ii.ge.1).and.(ktr-ii.le.n3)
                  end if
                end select ! id
                if (is_in_box) A(odx,idx) = A(odx,idx) + stencil_coe(ii,id)
              end do ! ii
            end do ! itr
          end do ! jtr
        end do ! ktr
      end if
    end do ! id
    return
end subroutine padx_new_get_action

subroutine  fill_v_stencil_1d_stdcg(ig1,ig2,ig3,id,nb,d,v_stencil)
  use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc, eta
  use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc, nord2
  !use grids, only : pot_stdcg
  implicit none
  integer, intent(in)  :: ig1, ig2, ig3, id
  integer     :: nb(6)
  REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
  real(8), intent(out) :: v_stencil(-nord2:nord2)
  integer :: ii
  select case (id)
  case (drct_aa)
    do ii = -nord2,nord2
      v_stencil(ii) = d(ig1+ii, ig2   , ig3   )
    end do ! ii
  case (drct_bb)
    do ii = -nord2,nord2
      v_stencil(ii) = d(ig1   , ig2+ii, ig3   )
    end do ! ii
  case (drct_cc)
    do ii = -nord2,nord2
      v_stencil(ii) = d(ig1   , ig2   , ig3+ii)
    end do ! ii
  case (drct_ab)
    if (eta(drct_ab).gt.0.d0) then
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2+ii, ig3   )
      end do ! ii
    else
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2-ii, ig3   )
      end do ! ii
    end if
  case (drct_ac)
    if (eta(drct_ac).gt.0.d0) then
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2   , ig3+ii)
      end do ! ii
    else
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1+ii, ig2   , ig3-ii)
      end do ! ii
    end if
  case (drct_bc)
    if (eta(drct_bc).gt.0.d0) then
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1   , ig2+ii, ig3+ii)
      end do ! ii
    else
      do ii = -nord2,nord2
        v_stencil(ii) = d(ig1   , ig2+ii, ig3-ii)
      end do ! ii
    end if
  case default
  end select ! id
  return
end subroutine fill_v_stencil_1d_stdcg

subroutine padx_new_use_A(nd,nb,prefac_,stencil_coe,d,A,Ad)
  use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc, eta
  use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc, nord2
    implicit none
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    real(8), intent(in) :: prefac_(drct_aa:drct_bc)
    real(8), intent(in) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    REAL(8)    :: A((nd(4)-nd(1)+1)*(nd(5)-nd(2)+1)*(nd(6)-nd(3)+1), &
    &               (nd(4)-nd(1)+1)*(nd(5)-nd(2)+1)*(nd(6)-nd(3)+1))
    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !
    !integer, intent(in) :: g1l,g1u,g2l,g2u,g3l,g3u
    !real(8), intent(in) :: d(:,:,:)
    !real(8) :: Ad(:,:,:)
    real(8) :: v_stencil(-nord2:nord2)
    !real(8) :: v_stencil_2d(-nord2:nord2,drct_aa:drct_cc)
    real(8) :: action_tmp
    integer :: itr, jtr, ktr
    integer :: ii, id
    integer :: odx, idx, n1, n2, n3, n12
    logical :: is_in_box
    Ad = 0.d0
    n1 = (nd(4)-nd(1)+1)
    n2 = (nd(5)-nd(2)+1)
    n3 = (nd(6)-nd(3)+1)
    n12 = n1*n2
    do id = drct_aa, drct_bc
      if (abs(prefac_(id)).gt.1.d-8) then
        do ktr = nd(3), nd(6)
          do jtr = nd(2), nd(5)
            do itr = nd(1), nd(4)
              odx = itr+(jtr-1)*n1+(ktr-1)*n12 
              if (id.eq.drct_aa) then
                ! A(odx,odx) contains combined stencil for all directions at (odx,odx)
                Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                  & + A(odx,odx)*(d(itr, jtr, ktr))
              end if
              !print *, 'part1', A(odx,odx), stencil_coe(0,id)
              do ii = -nord2, nord2
                select case (id)
                case (drct_aa)
                  is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1)
                  if (is_in_box) then
                    idx = itr+ii+(jtr-1)*n1+(ktr-1)*n12 
                    if (idx.ne.odx) then
                      Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr+ii, jtr, ktr))
                      !print *, 'part2', A(odx,idx), stencil_coe(abs(ii),id)
                    end if
                  end if
                case (drct_bb)
                  is_in_box = (jtr+ii.ge.1).and.(jtr+ii.le.n2)
                  if (is_in_box) then
                    idx = itr+(jtr+ii-1)*n1+(ktr-1)*n12 
                    if (idx.ne.odx) then
                      Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr, jtr+ii, ktr))
                      !print *, 'part3', A(odx,idx), stencil_coe(abs(ii),id)
                    end if
                  end if
                case (drct_cc)
                  is_in_box = (ktr+ii.ge.1).and.(ktr+ii.le.n3)
                  if (is_in_box) then
                    idx = itr+(jtr-1)*n1+(ktr+ii-1)*n12 
                    if (idx.ne.odx) then
                      Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr, jtr, ktr+ii))
                      !print *, 'part4', A(odx,idx), stencil_coe(abs(ii),id)
                    end if
                  end if
                case (drct_ab)
                  if (eta(drct_ab).gt.0.d0) then
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(jtr+ii.ge.1).and.(jtr+ii.le.n2)
                    if (is_in_box) then
                      idx = itr+ii+(jtr+ii-1)*n1+(ktr-1)*n12 
                      if (idx.ne.odx) Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr+ii, jtr+ii, ktr))
                    end if
                  else
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(jtr-ii.ge.1).and.(jtr-ii.le.n2)
                    if (is_in_box) then
                      idx = itr+ii+(jtr-ii-1)*n1+(ktr-1)*n12 
                      if (idx.ne.odx) Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr+ii, jtr-ii, ktr))
                    end if
                  end if
                case (drct_ac)
                  if (eta(drct_ac).gt.0.d0) then
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(ktr+ii.ge.1).and.(ktr+ii.le.n3)
                    if (is_in_box) then
                      idx = itr+ii+(jtr-1)*n1+(ktr+ii-1)*n12 
                      if (idx.ne.odx) Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr+ii, jtr, ktr+ii))
                    end if
                  else
                    is_in_box = (itr+ii.ge.1).and.(itr+ii.le.n1) &
                      &    .and.(ktr-ii.ge.1).and.(ktr-ii.le.n3)
                    if (is_in_box) then
                      idx = itr+ii+(jtr-1)*n1+(ktr-ii-1)*n12 
                      if (idx.ne.odx) Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr+ii, jtr, ktr-ii))
                    end if
                  end if
                case (drct_bc)
                  if (eta(drct_bc).gt.0.d0) then
                    is_in_box = (jtr+ii.ge.1).and.(jtr+ii.le.n2) &
                      &    .and.(ktr+ii.ge.1).and.(ktr+ii.le.n3)
                    if (is_in_box) then
                      idx = itr+(jtr+ii-1)*n1+(ktr+ii-1)*n12 
                      if (idx.ne.odx) Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr, jtr+ii, ktr+ii))
                    end if
                  else
                    is_in_box = (jtr+ii.ge.1).and.(jtr+ii.le.n2) &
                      &    .and.(ktr-ii.ge.1).and.(ktr-ii.le.n3)
                    if (is_in_box) then
                      idx = itr+(jtr+ii-1)*n1+(ktr-ii-1)*n12 
                      if (idx.ne.odx) Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + &
                        & + A(odx,idx)*(d(itr, jtr+ii, ktr-ii))
                    end if
                  end if
                end select ! id
              end do ! ii
            end do ! itr
          end do ! jtr
        end do ! ktr
      end if
    end do ! id
    return
end subroutine padx_new_use_A

module padx_cuda
#ifdef USE_CUDA
  implicit none
  contains
  subroutine padx_new_cuda(nd, nb, prefac_, stencil_coe, d, Ad)
      use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc, nord2
      use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc, eta
      implicit none
      INTEGER     :: nd(6)
      INTEGER     :: nb(6)
      real(8), device, intent(in) :: prefac_(drct_aa:drct_bc)
      real(8), device, intent(in) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
      REAL(8), device, intent(in)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
      REAL(8), device, intent(inout)   :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))

      real(8) :: action_tmp
      real(8) :: v0, vp, vn, eta_ab, eta_ac, eta_bc
      integer :: itr, jtr, ktr
      integer :: nb1, nb2, nb3, nb4, nb5, nb6
      integer :: ii, id

      Ad = 0.d0

      nb1 = nb(1)
      nb2 = nb(2)
      nb3 = nb(3)
      nb4 = nb(4)
      nb5 = nb(5)
      nb6 = nb(6)

      eta_ab = eta(drct_ab)
      eta_ac = eta(drct_ac)
      eta_bc = eta(drct_bc)

      !$cuf kernel do (3)
      do ktr = nd(3), nd(6)
        do jtr = nd(2), nd(5)
          do itr = nd(1), nd(4)
            do id = drct_aa, drct_bc
              if (abs(prefac_(id)) .gt. 1.d-8) then
                v0 =  get_v_stencil_1d_stdcg_cuda(itr, jtr, ktr, id, &
                                                  nb1, nb2, nb3, nb4, nb5, nb6, &
                                                  eta_ab, eta_ac, eta_bc, d, 0)
                action_tmp = stencil_coe(0,id)*v0
                do ii = 1, nord2
                  vp =  get_v_stencil_1d_stdcg_cuda(itr, jtr, ktr, id, &
                                                    nb1, nb2, nb3, nb4, nb5, nb6, &
                                                    eta_ab, eta_ac, eta_bc, d, ii)
                  vn =  get_v_stencil_1d_stdcg_cuda(itr, jtr, ktr, id, &
                                                    nb1, nb2, nb3, nb4, nb5, nb6, &
                                                    eta_ab, eta_ac, eta_bc, d, -ii)
                  action_tmp = action_tmp &
                    & + stencil_coe(ii,id)*(vp+vn)
                end do ! ii
                Ad(itr,jtr,ktr) = Ad(itr,jtr,ktr) + action_tmp
              end if
            end do ! id
          end do ! itr
        end do ! jtr
      end do ! ktr

      return
  end subroutine padx_new_cuda

  attributes(device) &
  real(8) function get_v_stencil_1d_stdcg_cuda(ig1, ig2, ig3, id,&
                                               nb1, nb2, nb3, nb4, nb5, nb6, &
                                               eta_ab, eta_ac, eta_bc, d, ii) result (v)
    implicit none
    ! Needed to declare these here because module imports not allowed in device functions
    ! and constants needed for case statements
    integer, parameter :: drct_aa = 1
    integer, parameter :: drct_bb = 2
    integer, parameter :: drct_cc = 3
    integer, parameter :: drct_ab = 4
    integer, parameter :: drct_ac = 5
    integer, parameter :: drct_bc = 6

    integer, value, intent(in)  :: ig1, ig2, ig3, id, ii
    integer, value, intent(in)  :: nb1, nb2, nb3, nb4, nb5, nb6
    real(8), value, intent(in)  :: eta_ab, eta_ac, eta_bc
    real(8), intent(out)    :: v

    real(8), device    :: d(nb1:nb4, nb2:nb5, nb3:nb6)

    select case (id)
    case (drct_aa)
      v = d(ig1+ii, ig2   , ig3   )
    case (drct_bb)
      v = d(ig1   , ig2+ii, ig3   )
    case (drct_cc)
      v = d(ig1   , ig2   , ig3+ii)
    case (drct_ab)
      if (eta_ab.gt.0.d0) then
        v = d(ig1+ii, ig2+ii, ig3   )
      else
        v = d(ig1+ii, ig2-ii, ig3   )
      end if
    case (drct_ac)
      if (eta_ac.gt.0.d0) then
        v = d(ig1+ii, ig2   , ig3+ii)
      else
        v = d(ig1+ii, ig2   , ig3-ii)
      end if
    case (drct_bc)
      if (eta_bc.gt.0.d0) then
        v = d(ig1   , ig2+ii, ig3+ii)
      else
        v = d(ig1   , ig2+ii, ig3-ii)
      end if
    case default
    end select ! id
    return
  end function get_v_stencil_1d_stdcg_cuda
#endif
end module padx_cuda
