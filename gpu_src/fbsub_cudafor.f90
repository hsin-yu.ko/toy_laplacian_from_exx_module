#define THREADS (128)
#define DIV_UP(a,b)     (((a)+((b)-1))/(b))

module fbsub_cuda
#ifdef USE_CUDA
  use cudafor
  use laplacian_natan_kronik, only : drct_aa, drct_bc, nord2
  implicit none

  integer, dimension(:), device, allocatable :: zdone_d
  integer :: maxblocks_fwd, maxblocks_bkd
  !real(8), dimension(-nord2:nord2, drct_aa:drct_bc), constant :: stencil_coe_L_c
  real(8), dimension(7, 3), constant :: stencil_coe_L_c

  contains

#define BDX (32)
#define BDY (THREADS/32)
! NOTE: NMX must be greater than expected grid dimension. Currently set to handle
! up to 96^3 grids
!#define NMX (64)
#define NMX (96)

  attributes(device) &
  subroutine rowUpdateFwd_f_d(extX, X, Y, Z, i, j, k, sh, m)
    use cooperative_groups
    implicit none

    integer, value, intent(in) :: extX, X, Y, Z, i, j, k
    real(8), dimension(*), shared, intent(inout) :: sh
    real(8), dimension(X, Y, Z), intent(inout), volatile :: m

    integer :: lid, l
    real(8) :: tmp

    ! NOTE: need to use cooperative groups here because PGI doesn't
    ! expose syncwarp()
    type(coalesced_group) :: wg
    wg = this_warp()

    lid = threadIdx%x

    ! parallel process
    do l = 0, NMX-1, BDX
      if (l+lid <= extX) then
        tmp = stencil_coe_L_c(3,3) * m(i+l+lid, j,   k-1) + &
              stencil_coe_L_c(2,3) * m(i+l+lid, j,   k-2) + &
              stencil_coe_L_c(1,3) * m(i+l+lid, j,   k-3) + &
              stencil_coe_L_c(3,2) * m(i+l+lid, j-1, k  ) + &
              stencil_coe_L_c(2,2) * m(i+l+lid, j-2, k  ) + &
              stencil_coe_L_c(1,2) * m(i+l+lid, j-3, k  )

        sh(l+lid) = m(i+l+lid, j, k) - tmp
      endif
    enddo

    call syncthreads(wg)

    ! serial process
    if (lid == 1) then
      do l = 1, extX-3
        tmp = sh(l)
        sh(l+1) = sh(l+1) - stencil_coe_L_c(3,1) * tmp
        sh(l+2) = sh(l+2) - stencil_coe_L_c(2,1) * tmp
        sh(l+3) = sh(l+3) - stencil_coe_L_c(1,1) * tmp
      enddo

      sh(extX-1) = sh(extX-1) - stencil_coe_L_c(3,1) * sh(extX-2)
      sh(extX) = sh(extX) - stencil_coe_L_c(2,1) * sh(extX-2)
      sh(extX) = sh(extX) - stencil_coe_L_c(3,1) * sh(extX-1)
    endif

    call syncthreads(wg)

    do l = 0, NMX-1, BDX
      if (l+lid <= extX) then
        m(i+l+lid, j, k) = sh(l+lid)
      endif
    enddo

  end subroutine rowUpdateFwd_f_d

  attributes(device) &
  subroutine rowUpdateBkd_f_d(extX, X, Y, Z, i, j, k, sh, m)
    use cooperative_groups
    implicit none

    integer, value, intent(in) :: extX, X, Y, Z, i, j, k
    real(8), dimension(*), shared, intent(inout) :: sh
    real(8), dimension(X, Y, Z), intent(inout), volatile :: m

    integer :: lid, l
    real(8) :: tmp

    ! NOTE: need to use cooperative groups here because PGI doesn't
    ! expose syncwarp()
    type(coalesced_group) :: wg
    wg = this_warp()

    lid = threadIdx%x

    ! parallel process
    do l = 0, NMX-1, BDX
      if (l+lid <= extX) then
        tmp = stencil_coe_L_c(5,3) * m(i+l+lid, j,   k+1) + &
              stencil_coe_L_c(6,3) * m(i+l+lid, j,   k+2) + &
              stencil_coe_L_c(7,3) * m(i+l+lid, j,   k+3) + &
              stencil_coe_L_c(5,2) * m(i+l+lid, j+1, k  ) + &
              stencil_coe_L_c(6,2) * m(i+l+lid, j+2, k  ) + &
              stencil_coe_L_c(7,2) * m(i+l+lid, j+3, k  )

        sh(extX - (l+lid) + 1) = m(i+l+lid, j, k) - tmp
      endif
    enddo

    call syncthreads(wg)

    ! serial process
    if (lid == 1) then
      do l = 1, extX-3
        tmp = sh(l)
        sh(l+1) = sh(l+1) - stencil_coe_L_c(5,1) * tmp
        sh(l+2) = sh(l+2) - stencil_coe_L_c(6,1) * tmp
        sh(l+3) = sh(l+3) - stencil_coe_L_c(7,1) * tmp
      enddo

      sh(extX-1) = sh(extX-1) - stencil_coe_L_c(5,1) * sh(extX-2)
      sh(extX) = sh(extX) - stencil_coe_L_c(6,1) * sh(extX-2)
      sh(extX) = sh(extX) - stencil_coe_L_c(5,1) * sh(extX-1)
    endif

    call syncthreads(wg)

    do l = 0, NMX-1, BDX
      if (l+lid <= extX) then
        m(i+l+lid, j, k) = sh(extX - (l+lid) + 1)
      endif
    enddo

  end subroutine rowUpdateBkd_f_d

  attributes(global) &
  subroutine fwdsub_k_f(X, Y, Z, offX, offY, offZ, extX, extY, extZ, &
                        zdone, m)
    implicit none

    integer, value, intent(in) :: X, Y, Z
    integer, value, intent(in) :: offX, offY, offZ
    integer, value, intent(in) :: extX, extY, extZ
    integer, volatile, dimension(*), intent(inout) :: zdone
    real(8), dimension(X, Y, Z), intent(inout), volatile :: m

    integer :: i, j, k
    integer :: nwp, tidy, wid, nsteps
    real(8), shared :: sh(NMX, BDY)

    nwp = gridDim%x * BDY
    tidy = threadIdx%y
    wid = (blockIdx%x - 1) * BDY + tidy

    j = wid
    k = -wid

    nsteps = DIV_UP(extY, nwp) * extZ + nwp - 1

    do i = 1, nsteps

      if (k >= 1 .and. j <= extY) then
        call rowUpdateFwd_f_d(extX, X, Y, Z, offX, offY+j, offZ+k, sh(:,tidy), m)
      endif

      call threadfence()
      call syncthreads()

      if (j <= extY .and. tidy == BDY) then
        zdone((j-1)/BDY + 1) = k-1
      endif

      k = k + 1

      if (k == extZ+1) then
        if (tidy == BDY) then
          zdone((j-1)/BDY + 1) = k-1
        endif
        k = 1
        j = j + nwp
      endif

      if (j .ne. 1 .and. j <= extY .and. tidy == 1) then
        do while(zdone((j-2) / BDY + 1) < (k-1))
          ! Wait for other block.
        enddo
      endif

      call syncthreads()

    enddo

  end subroutine fwdsub_k_f

  attributes(global) &
  subroutine bkdsub_k_f(X, Y, Z, offX, offY, offZ, extX, extY, extZ, &
                        zdone, m)
    implicit none

    integer, value, intent(in) :: X, Y, Z
    integer, value, intent(in) :: offX, offY, offZ
    integer, value, intent(in) :: extX, extY, extZ
    integer, volatile, dimension(*), intent(inout) :: zdone
    real(8), dimension(X, Y, Z), intent(inout), volatile :: m

    integer :: i, j, k
    integer :: nwp, tidy, wid, nsteps
    real(8), shared :: sh(NMX, BDY)

    nwp = gridDim%x * BDY
    tidy = threadIdx%y
    wid = (blockIdx%x - 1) * BDY + tidy

    j = extY - wid + 1
    k = extZ + wid + 1

    nsteps = DIV_UP(extY, nwp) * extZ + nwp - 1

    do i = 1, nsteps

      if (k <= extZ .and. j >= 1) then
        call rowUpdateBkd_f_d(extX, X, Y, Z, offX, offY+j, offZ+k, sh(:,tidy), m)
      endif

      call threadfence()
      call syncthreads()

      if (j >= 1 .and. tidy == BDY) then
        zdone((j-1)/BDY + 1) = (k-1)
      endif

      k = k - 1

      if (k < 1) then
        if (tidy == BDY) then
          zdone((j-1)/BDY + 1) = k-1
        endif
        k = extZ
        j = j - nwp
      endif

      if (j >= 1 .and. j < extY .and. tidy == 1) then
        do while(zdone((j-2)/BDY + 1) > (k-1))
          ! Wait for other block.
        enddo
      endif

      call syncthreads()

    enddo

  end subroutine bkdsub_k_f

  subroutine fwdsub_init_cudaf(stencil_coe_L)
    implicit none

    !real(8),intent(in) :: stencil_coe_L(-nord2:nord2, drct_aa:drct_bc)
    real(8),intent(in) :: stencil_coe_L(7, 3)
    integer :: istat

    if (size(stencil_coe_L, 1) .ne. 7 .or. size(stencil_coe_L, 2) .ne. 3) then
      print*, "ERROR: Supplied stencil_coe_L shape unsupported! Expected stencil_coe_L(7, 3)!"
      stop
    endif

    stencil_coe_L_c = stencil_coe_L

  end subroutine fwdsub_init_cudaf

  subroutine fwdbkdsub_cudaf(nd, nb, x)
    use cudafor
    implicit none

    integer, dimension(6), intent(in) :: nd, nb
    real(8), device, dimension(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)), intent(inout) :: x

    integer :: dimX, dimY, dimZ
    integer :: offX, offY, offZ
    integer :: extX, extY, extZ
    integer :: istat

    type(dim3) :: tblock

    tblock = dim3(32, THREADS/32, 1)

    dimX = nb(4)-nb(1)+1;
    dimY = nb(5)-nb(2)+1;
    dimZ = nb(6)-nb(3)+1;

    offX = nd(1)-nb(1);
    offY = nd(2)-nb(2);
    offZ = nd(3)-nb(3);

    extX = nd(4)-nd(1)+1;
    extY = nd(5)-nd(2)+1;
    extZ = nd(6)-nd(3)+1;

    if (allocated(zdone_d) .and. size(zdone_d) <  extY / tblock%y) deallocate(zdone_d)
    if (.not. allocated(zdone_d)) then
      allocate(zdone_d(extY / tblock%y))

      ! Get maximum number of active blocks on the GPU for the fwd/bkd kernels
      istat = cudaOccupancyMaxActiveBlocksPerMultiprocessor(maxblocks_fwd, fwdsub_k_f, THREADS, 0)
      istat = cudaOccupancyMaxActiveBlocksPerMultiprocessor(maxblocks_bkd, bkdsub_k_f, THREADS, 0)
    endif

    if (extX > NMX) then
      print*, "ERROR: fwdbkdsub_cudaf only supports grids up to NMX^3, NMX=", NMX
      stop
    endif

    zdone_d = -1

    call fwdsub_k_f<<<min(DIV_UP(extY, tblock%y), maxblocks_fwd), tblock>>>(dimX, dimY, dimZ, &
      offX, offY, offZ, extX, extY, extZ, zdone_d, x)

    call bkdsub_k_f<<<min(DIV_UP(extY, tblock%y), maxblocks_bkd), tblock>>>(dimX, dimY, dimZ, &
      offX, offY, offZ, extX, extY, extZ, zdone_d, x)

  end subroutine fwdbkdsub_cudaf
#endif
end module fbsub_cuda
