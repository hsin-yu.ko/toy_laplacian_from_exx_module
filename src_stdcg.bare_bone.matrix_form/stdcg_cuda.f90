!TODO: use cuda blas/lapack routines...
SUBROUTINE stdcg_cuda(iter, n, eps, fbsscale, coemicf, coeke, rho, pot)
    use cublas
    IMPLICIT NONE
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    integer, intent(out) :: iter
    integer, intent(in)  :: n(3)
    real(8), intent(in)  :: eps
    real(8), intent(in)  :: fbsscale
    real(8), intent(in)  :: coemicf(-3:3,3,3)
    real(8), intent(in)  :: coeke(-3:3,3,3)
    real(8), intent(in)  :: rho(n(1),n(2),n(3))
    real(8), intent(out) :: pot(n(1),n(2),n(3))
    !------------------------------------------------------------------------


    !------------------------------------------------------------------------
    ! --- local variables ---
    !------------------------------------------------------------------------
    integer                      :: itr
    integer                      :: nord2
    integer                      :: npt
    integer, device              :: nd_d(6)
    integer, device              :: nb_d(6)
    real(8)                      :: nro
    real(8)                      :: nr
    real(8)                      :: mnr
    real(8)                      :: alfa
    real(8), device              :: coemicf_d(-3:3,3,3)
    real(8), device              :: coeke_d(-3:3,3,3)
    real(8), device, allocatable :: x_d (:,:,:)
    real(8), device, allocatable :: r_d (:,:,:)
    !real(8), device, allocatable :: z_d(:,:,:)   ! z = M-1 r
    real(8), device, allocatable :: d0_d(:,:,:)
    real(8), device, allocatable :: d1_d(:,:,:)
    type(dim3)                   :: grid, tBlock
    !------------------------------------------------------------------------


    !------------------------------------------------------------------------
    ! --- allocate arrays ---
    !------------------------------------------------------------------------
    nord2=3
    !
    nd_d(1)=1                                      
    nd_d(2)=1         
    nd_d(3)=1         
    nd_d(4)=n(1)      
    nd_d(5)=n(2)      
    nd_d(6)=n(3)      
    !
    nb_d(1)=1-nord2
    nb_d(2)=1-nord2
    nb_d(3)=1-nord2
    nb_d(4)=n(1)+nord2
    nb_d(5)=n(2)+nord2
    nb_d(6)=n(3)+nord2
    !
    !npt=(nb_d(4)-nb_d(1)+1)*(nb_d(5)-nb_d(2)+1)*(nb_d(6)-nb_d(3)+1)
    npt=(n(1)+2*nord2)*(n(2)+2*nord2)*(n(3)+2*nord2)
    !
    ALLOCATE( x_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE( r_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE(d0_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    ALLOCATE(d1_d(1-nord2:n(1)+nord2, 1-nord2:n(2)+nord2, 1-nord2:n(3)+nord2))
    !
    coeke_d = coeke
    coemicf_d = coemicf
    r_d = 0.d0; x_d = 0.d0;
    r_d(1:n(1),1:n(2),1:n(3))=rho(:,:,:) ! TODO: memcp in the future...
    x_d(1:n(1),1:n(2),1:n(3))=pot(:,:,:) ! TODO: memcp in the future...
    ! better init those as well, because of thread exit condition it 
    ! could happen that boundaries are not initialized:
    d0_d = 0.d0; d1_d = 0.d0;
    !
    tBlock = dim3(32,2,2)
    grid = dim3(ceiling(real(n(1))/tBlock%x), &
      &         ceiling(real(n(2))/tBlock%y), &
      &         ceiling(real(n(3))/tBlock%z))
    CALL PADX_CUDA<<<grid, tBlock>>>(nd_d,nb_d,coeke_d,x_d,d1_d)
    call cublasDaxpy(npt,-1.0d0,d1_d,1,r_d,1) ! we get the initial residual in r_d ! std CG_1 : r = b - A * x;
    call cublasDcopy(npt, r_d, 1, d0_d, 1)    ! we copy the residual in d0_d       ! std CG_2 : p = r
    nro = cublasDdot(npt,r_d,1,r_d,1) ! we get the squared norm of the residual  ! std CG_3 : rsold = r' * r
    !
    DO itr=0,1000 ! loop over the dimension of the problem                         ! std CG_4 : for i = 1:length(b)
        CALL PADX_CUDA<<<grid, tBlock>>>(nd_d,nb_d,coeke_d,d0_d,d1_d)                                       ! std CG_5 : Ap = A * p; % p = d0_d; Ap = d1_d
        alfa=nro/cublasDdot(npt,d0_d,1,d1_d,1)                                 ! std CG_6 : alpha = rsold / (p' * Ap);
        call cublasDaxpy(npt,alfa,d0_d,1,x_d,1)                                  ! std CG_7 : x = x + alpha * p;
        call cublasDaxpy(npt,-alfa,d1_d,1,r_d,1)                                 ! std CG_8 : r = r - alpha * Ap;
        nr = cublasDdot(npt,r_d,1,r_d,1)                                         ! std CG_9 : rsnew = r' * r;
        IF (nr < eps*eps*(1.d0+nro)) EXIT                                        ! std CG_10-12 : if (converge) : break
        CALL cublasDscal(npt,nr/nro,d0_d,1)                                    ! std CG_13 : p = r + (rsnew / rsold) * p;
        call cublasDaxpy(npt,1.d0,r_d,1,d0_d,1)                                    ! std CG_13 : p = r + (rsnew / rsold) * p;
        nro = nr
    END DO
    !------------------------------------------------------------------------

    pot(:,:,:)=x_d(1:n(1),1:n(2),1:n(3)) ! TODO may be memcp in the future...
    iter = itr

    DEALLOCATE(x_d)
    DEALLOCATE(r_d)
    DEALLOCATE(d0_d)
    DEALLOCATE(d1_d)
    !
END SUBROUTINE stdcg_cuda
!============================================================================
!       OP     PCG    FLOPS      MEM         DEP
! ------------------------------------------------
! [01] DOT              2N       2N
!
! [02] MV              37N       3N
! [03] DOT      *       2N       2N
! [04] DOT              2N       2N        02,03
! [05] AXPY             2N       3N        04
! [06] AXPY             2N       3N        04
! [07] CP               --       2N        06
! [08] SCAL     *        N       2N        07
! [09] FW       *      18N       2N        08 ! TODO
! [10] BW       *      18N       2N        09 ! TODO
! [11] CP       *       --       2N        10
! [12] DOT              2N       2N        09
! [13] AXPY             2N       3N        12

attributes(global) SUBROUTINE PADX_CUDA(nd,nb,coeke,d,Ad)
    IMPLICIT NONE
    REAL(8), PARAMETER    :: eps8 = 1.d-8
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    INTEGER     :: itr, jtr, ktr
    REAL(8)    :: c00
    REAL(8)    :: coeke(-3:3,3,3)
    REAL(8)    :: d(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    REAL(8)    :: Ad(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !------------------------------------------------------------------------
    c00 = coeke(0,1,1)+coeke(0,2,2)+coeke(0,3,3)

    itr = (blockIdx%x-1)*blockDim%x + threadIdx%x - 1 + nd(1)
    jtr = (blockIdx%y-1)*blockDim%y + threadIdx%y - 1 + nd(2)
    ktr = (blockIdx%z-1)*blockDim%z + threadIdx%z - 1 + nd(3)
    if (itr.gt.nd(4)) RETURN
    if (jtr.gt.nd(5)) RETURN
    if (ktr.gt.nd(6)) RETURN

    Ad(itr,jtr,ktr)=c00*d(itr,jtr,ktr) &
      &             +coeke(1,1,1)*(d(itr-1,jtr,ktr)+d(itr+1,jtr,ktr)) &
      &             +coeke(2,1,1)*(d(itr-2,jtr,ktr)+d(itr+2,jtr,ktr)) &
      &             +coeke(3,1,1)*(d(itr-3,jtr,ktr)+d(itr+3,jtr,ktr)) &
      &             +coeke(1,2,2)*(d(itr,jtr-1,ktr)+d(itr,jtr+1,ktr)) &
      &             +coeke(2,2,2)*(d(itr,jtr-2,ktr)+d(itr,jtr+2,ktr)) &
      &             +coeke(3,2,2)*(d(itr,jtr-3,ktr)+d(itr,jtr+3,ktr)) &
      &             +coeke(1,3,3)*(d(itr,jtr,ktr-1)+d(itr,jtr,ktr+1)) &
      &             +coeke(2,3,3)*(d(itr,jtr,ktr-2)+d(itr,jtr,ktr+2)) &
      &             +coeke(3,3,3)*(d(itr,jtr,ktr-3)+d(itr,jtr,ktr+3))

!-------------------------------------------------------------------------------
    IF (ABS(coeke(1,1,2)) .GT. eps8) THEN
      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr)&
        & +coeke(1,1,2)*( d(itr+1,jtr+1,ktr)-d(itr+1,jtr-1,ktr)  &
        &                -d(itr-1,jtr+1,ktr)+d(itr-1,jtr-1,ktr) )&
        & +coeke(2,1,2)*( d(itr+2,jtr+2,ktr)-d(itr+2,jtr-2,ktr)  &
        &                -d(itr-2,jtr+2,ktr)+d(itr-2,jtr-2,ktr) )&
        & +coeke(3,1,2)*( d(itr+3,jtr+3,ktr)-d(itr+3,jtr-3,ktr)  &
        &                -d(itr-3,jtr+3,ktr)+d(itr-3,jtr-3,ktr) )
    END IF

    IF (ABS(coeke(1,1,3)) .GT. eps8) THEN
      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr) &
        & +coeke(1,1,3)*( d(itr+1,jtr,ktr+1)-d(itr+1,jtr,ktr-1)  &
        &                -d(itr-1,jtr,ktr+1)+d(itr-1,jtr,ktr-1) )&
        & +coeke(2,1,3)*( d(itr+2,jtr,ktr+2)-d(itr+2,jtr,ktr-2)  &
        &                -d(itr-2,jtr,ktr+2)+d(itr-2,jtr,ktr-2) )&
        & +coeke(3,1,3)*( d(itr+3,jtr,ktr+3)-d(itr+3,jtr,ktr-3)  &
        &                -d(itr-3,jtr,ktr+3)+d(itr-3,jtr,ktr-3) )
    END IF

    IF (ABS(coeke(1,2,3)) .GT. eps8) THEN
      Ad(itr,jtr,ktr)=Ad(itr,jtr,ktr) &
        & +coeke(1,2,3)*( d(itr,jtr+1,ktr+1)-d(itr,jtr+1,ktr-1)  &
        &                -d(itr,jtr-1,ktr+1)+d(itr,jtr-1,ktr-1) )&
        & +coeke(2,2,3)*( d(itr,jtr+2,ktr+2)-d(itr,jtr+2,ktr-2)  &
        &                -d(itr,jtr-2,ktr+2)+d(itr,jtr-2,ktr-2) )&
        & +coeke(3,2,3)*( d(itr,jtr+3,ktr+3)-d(itr,jtr+3,ktr-3)  &
        &                -d(itr,jtr-3,ktr+3)+d(itr,jtr-3,ktr-3) )
    END IF
    return
END SUBROUTINE PADX_CUDA
