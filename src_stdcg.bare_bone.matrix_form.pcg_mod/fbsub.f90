!============================================================================
SUBROUTINE FWDSUB(nd,nb,stencil_coe_L,x)               ! solving the equation Lx = b by forward substitution, where L is lower triangular, given by coemicf
    IMPLICIT NONE
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    REAL(8)    :: stencil_coe_L(-3:3,3)
    REAL(8)    :: x(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- local variable ---
    !------------------------------------------------------------------------
    INTEGER     :: nthreads
    INTEGER     :: tidpo                      ! thread id plus one
    INTEGER     :: itr, jtr, ktr
    LOGICAL     :: finished(nb(2):nb(5),nb(3):nb(6))
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- external function ---
    !------------------------------------------------------------------------
    INTEGER     :: OMP_GET_NUM_THREADS
    INTEGER     :: OMP_GET_THREAD_NUM
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- initialize indicator ---
    !------------------------------------------------------------------------
    finished=.TRUE.
    DO ktr=nd(3),nd(6)
        DO jtr=nd(2),nd(5)
            finished(jtr,ktr)=.FALSE.
        END DO
    END DO
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- parallel work ---
    !------------------------------------------------------------------------
    !$omp parallel default(shared) private(tidpo,ktr,jtr,itr)
        !
        nthreads = OMP_GET_NUM_THREADS()
        tidpo    = OMP_GET_THREAD_NUM()+nd(3)
        !
        DO ktr = tidpo,nd(6),nthreads
            DO jtr = nd(2),nd(5)
                !--------------------------------------------------------------------
                DO WHILE(.NOT.finished(jtr,ktr-1))
                    !$OMP FLUSH
                END DO
                !$OMP FLUSH
                !--------------------------------------------------------------------
                 
                !--------------------------------------------------------------------
                !DIR$ SIMD
                DO itr = nd(1),nd(4)
                    x(itr,jtr,ktr)=x(itr,jtr,ktr)-stencil_coe_L(-1,3)*x(itr,jtr,ktr-1) & 
                                                 -stencil_coe_L(-2,3)*x(itr,jtr,ktr-2) &
                                                 -stencil_coe_L(-3,3)*x(itr,jtr,ktr-3) &
                                                 -stencil_coe_L(-1,2)*x(itr,jtr-1,ktr) &
                                                 -stencil_coe_L(-2,2)*x(itr,jtr-2,ktr) &
                                                 -stencil_coe_L(-3,2)*x(itr,jtr-3,ktr)
                END DO
                !--------------------------------------------------------------------
                
                !--------------------------------------------------------------------
                DO itr = nd(1),nd(4)-3
                    x(itr+1,jtr,ktr)=x(itr+1,jtr,ktr)-stencil_coe_L(-1,1)*x(itr,jtr,ktr)
                    x(itr+2,jtr,ktr)=x(itr+2,jtr,ktr)-stencil_coe_L(-2,1)*x(itr,jtr,ktr)
                    x(itr+3,jtr,ktr)=x(itr+3,jtr,ktr)-stencil_coe_L(-3,1)*x(itr,jtr,ktr)
                END DO
                x(nd(4)-1,jtr,ktr)=x(nd(4)-1,jtr,ktr)-stencil_coe_L(-1,1)*x(nd(4)-2,jtr,ktr)
                x(nd(4),  jtr,ktr)=x(nd(4),  jtr,ktr)-stencil_coe_L(-2,1)*x(nd(4)-2,jtr,ktr)
                x(nd(4),  jtr,ktr)=x(nd(4),  jtr,ktr)-stencil_coe_L(-1,1)*x(nd(4)-1,jtr,ktr)
                !--------------------------------------------------------------------
                  
                !--------------------------------------------------------------------
                !$OMP FLUSH
                finished(jtr,ktr)=.TRUE.
                !$OMP FLUSH
                !--------------------------------------------------------------------
            END DO
        END DO
        !
    !$omp end parallel
    !------------------------------------------------------------------------
END SUBROUTINE FWDSUB
!============================================================================

!============================================================================
SUBROUTINE BKDSUB(nd,nb,stencil_coe_L,x)               ! solving the equation Lx = b by backward substitution, where L is upper triangular, given by coemicf
    IMPLICIT NONE
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    INTEGER     :: nd(6)
    INTEGER     :: nb(6)
    REAL(8)    :: stencil_coe_L(-3:3,3)
    REAL(8)    :: x(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6))
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- local variable ---
    !------------------------------------------------------------------------
    INTEGER     :: nthreads
    INTEGER     :: nzmtid                      ! n(3) minus threads number
    INTEGER     :: itr, jtr, ktr
    LOGICAL     :: finished(nb(2):nb(5),nb(3):nb(6))
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- external function ---
    !------------------------------------------------------------------------
    INTEGER     :: OMP_GET_NUM_THREADS
    INTEGER     :: OMP_GET_THREAD_NUM
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- initialize indicator ---
    !------------------------------------------------------------------------
    finished=.TRUE.
    DO ktr=nd(3),nd(6)
        DO jtr=nd(2),nd(5)
            finished(jtr,ktr)=.FALSE.
        END DO
    END DO
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- parallel work ---
    !------------------------------------------------------------------------
    !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nzmtid,ktr,jtr,itr)
        !
        nthreads = OMP_GET_NUM_THREADS()
        nzmtid   = nd(6)-OMP_GET_THREAD_NUM()
        !
        DO ktr = nzmtid,nd(3),-nthreads
            DO jtr = nd(5),nd(2),-1
                !--------------------------------------------------------------------
                DO WHILE(.NOT.finished(jtr,ktr+1))
                    !$OMP FLUSH
                END DO
                !$OMP FLUSH
                !--------------------------------------------------------------------
                
                !--------------------------------------------------------------------
                !DIR$ SIMD
                DO itr = nd(1),nd(4)
                    x(itr,jtr,ktr)=x(itr,jtr,ktr)-stencil_coe_L(1,3)*x(itr,jtr,ktr+1) & 
                                                 -stencil_coe_L(2,3)*x(itr,jtr,ktr+2) &
                                                 -stencil_coe_L(3,3)*x(itr,jtr,ktr+3) &
                                                 -stencil_coe_L(1,2)*x(itr,jtr+1,ktr) &
                                                 -stencil_coe_L(2,2)*x(itr,jtr+2,ktr) &
                                                 -stencil_coe_L(3,2)*x(itr,jtr+3,ktr)
                END DO
                !--------------------------------------------------------------------
                
                !--------------------------------------------------------------------
                DO itr = nd(4),nd(1)+3,-1
                    x(itr-1,jtr,ktr)=x(itr-1,jtr,ktr)-stencil_coe_L(1,1)*x(itr,jtr,ktr)
                    x(itr-2,jtr,ktr)=x(itr-2,jtr,ktr)-stencil_coe_L(2,1)*x(itr,jtr,ktr)
                    x(itr-3,jtr,ktr)=x(itr-3,jtr,ktr)-stencil_coe_L(3,1)*x(itr,jtr,ktr)
                END DO
                x(nd(1)+1,jtr,ktr)=x(nd(1)+1,jtr,ktr)-stencil_coe_L(1,1)*x(nd(1)+2,jtr,ktr)
                x(nd(1),  jtr,ktr)=x(nd(1)  ,jtr,ktr)-stencil_coe_L(2,1)*x(nd(1)+2,jtr,ktr)
                x(nd(1),  jtr,ktr)=x(nd(1)  ,jtr,ktr)-stencil_coe_L(1,1)*x(nd(1)+1,jtr,ktr)
                !--------------------------------------------------------------------
                
                !--------------------------------------------------------------------
                !$OMP FLUSH
                finished(jtr,ktr)=.TRUE.
                !$OMP FLUSH
                !--------------------------------------------------------------------
            END DO
        END DO
        !
    !$OMP END PARALLEL
    !------------------------------------------------------------------------
END SUBROUTINE BKDSUB
!============================================================================

