module lapl_gpu_test
  implicit none
  save
  real(8), parameter :: pi=acos(-1.d0)
  integer, parameter :: drct_aa = 1
  integer, parameter :: drct_bb = 2
  integer, parameter :: drct_cc = 3
  integer, parameter :: drct_ab = 4
  integer, parameter :: drct_ac = 5
  integer, parameter :: drct_bc = 6
  contains

    attributes(global) subroutine  laplacian_sweep_cuda(&
      & nr1, nr2, nr3, pot, prefac, nord2, stencil_coe, rho)
      implicit none
      integer, intent(in), value :: nr1, nr2, nr3
      real(8), intent(in ) :: pot(nr1,nr2,nr3)
      real(8), intent(in ) :: prefac(drct_aa:drct_bc)
      integer, intent(in), value :: nord2
      real(8), intent(in ) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
      real(8), intent(inout) :: rho(nr1,nr2,nr3)
      !
      integer :: ii
      integer :: ig1,ig2,ig3
      !
      ig1 = (blockIdx%x-1)*blockDim%x + threadIdx%x
      ig2 = (blockIdx%y-1)*blockDim%y + threadIdx%y
      ig3 = (blockIdx%z-1)*blockDim%z + threadIdx%z
      if (ig1.lt.1+nord2.or.ig1.gt.nr1-nord2) RETURN
      if (ig2.lt.1+nord2.or.ig2.gt.nr2-nord2) RETURN
      if (ig3.lt.1+nord2.or.ig3.gt.nr3-nord2) RETURN
      !
      rho(ig1,ig2,ig3) =   stencil_coe(0,1) * pot(ig1,ig2,ig3) & 
                         + stencil_coe(0,2) * pot(ig1,ig2,ig3) &
                         + stencil_coe(0,3) * pot(ig1,ig2,ig3) 
      do ii = 1, nord2
        rho(ig1,ig2,ig3) = rho(ig1,ig2,ig3) + &
          stencil_coe(ii,1) * ( pot(ig1-ii,ig2,ig3) + pot(ig1+ii,ig2,ig3) ) + &
          stencil_coe(ii,2) * ( pot(ig1,ig2-ii,ig3) + pot(ig1,ig2+ii,ig3) ) + &
          stencil_coe(ii,3) * ( pot(ig1,ig2,ig3-ii) + pot(ig1,ig2,ig3+ii) ) 
      end do ! ii
      rho(ig1,ig2,ig3) = rho(ig1,ig2,ig3)/(-4.d0*pi)
      return
    end subroutine laplacian_sweep_cuda

end module lapl_gpu_test

!
! Author    : Hsin-Yu Ko
! Started at: 15.01.18
!
program main
  use constants
  use system
  use grids
  use cudafor
  !
  use laplacian_natan_kronik, only : eps10
  use laplacian_natan_kronik, only : init_derivatives_nk  => init_derivatives
  use laplacian_natan_kronik, only : compute_laplacian_nk => compute_laplacian
  use laplacian_natan_kronik, only : output_prefactor
  use laplacian_natan_kronik, only : output_stencil_coefficients
  use laplacian_natan_kronik, only : nord2
  use laplacian_natan_kronik, only : eta
  use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc
  use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc
  !
  implicit none
  real(8) :: tic, toc
  real(8) :: t_cpu,t_gpu
  call initialize
  !
  !CALL CPU_TIME(tic)
  !call cudaProfilerStart()
  call compute_numerical_laplacian_for_density_external
  !call cudaProfilerStop()
  !CALL CPU_TIME(toc)
  !t_cpu = toc - tic
  !print *, 't_cpu:', t_cpu
  !
  !CALL CPU_TIME(tic)
  call compute_numerical_laplacian_for_density_external_gpu
  !CALL CPU_TIME(toc)
  !t_gpu = toc - tic
  !print *, 't_gpu:', t_gpu
  !
  call check_laplacian
  call finalize
  call exit
contains

  subroutine  initialize()
    implicit none
    call init_system
    call init_grids
    call init_derivatives_nk(h, ainv, dr1, dr2, dr3)
    return
  end subroutine initialize

  subroutine  compute_numerical_laplacian_for_density_external()
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: v_stencil(-nord2:nord2)
    real(8) :: prefac_(drct_aa:drct_bc)
    real(8) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    real(8) :: action_tmp
    !
    integer :: ii, id
    real(8) :: toc, tic
    rho_nk = 0.d0
    call output_prefactor(prefac_)
    call output_stencil_coefficients(stencil_coe)

    call CPU_TIME(tic)

    do id = drct_aa, drct_bc
      if (abs(prefac_(id)).gt.1.d-8) then
        do ig3 = 1+nord2, nr3-nord2
          do ig2 = 1+nord2, nr2-nord2
            do ig1 = 1+nord2, nr1-nord2
              call fill_v_stencil_1d(ig1,ig2,ig3,id,v_stencil)
              action_tmp = stencil_coe(0,id)*v_stencil(0)
              do ii = 1, nord2
                action_tmp = action_tmp &
                  & + stencil_coe(ii,id)*(v_stencil(ii)+v_stencil(-ii))
              end do ! ii
              rho_nk(ig1,ig2,ig3) = rho_nk(ig1,ig2,ig3) + action_tmp
            end do ! ig1
          end do ! ig2
        end do ! ig3
      end if
    end do ! id
    rho_nk = rho_nk/(-4.d0*pi)

    call CPU_TIME(toc)

    print *, 'Time CPU: ', (toc-tic)

    return
  end subroutine compute_numerical_laplacian_for_density_external

  subroutine  fill_v_stencil_1d(ig1,ig2,ig3,id,v_stencil)
    implicit none
    integer, intent(in)  :: ig1, ig2, ig3, id
    real(8), intent(out) :: v_stencil(-nord2:nord2)
    integer :: ii
    select case (id)
    case (drct_aa)
      do ii = -nord2,nord2
        v_stencil(ii) = pot(ig1+ii, ig2   , ig3   )
      end do ! ii
    case (drct_bb)
      do ii = -nord2,nord2
        v_stencil(ii) = pot(ig1   , ig2+ii, ig3   )
      end do ! ii
    case (drct_cc)
      do ii = -nord2,nord2
        v_stencil(ii) = pot(ig1   , ig2   , ig3+ii)
      end do ! ii
    case default
    end select ! id
    return
  end subroutine fill_v_stencil_1d

  subroutine  compute_numerical_laplacian_for_density_external_gpu()
    use cudafor
    use lapl_gpu_test
    implicit none
    real(8) :: v_stencil(-nord2:nord2)
    real(8) :: prefac_(drct_aa:drct_bc)
    real(8) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    real(8), device, allocatable :: rho_d(:,:,:)
    real(8), device, allocatable :: pot_d(:,:,:)
    real(8), device :: stencil_coe_d(-nord2:nord2,drct_aa:drct_bc)
    real(8), device :: prefac_d(drct_aa:drct_bc)
    type(dim3) :: grid, tBlock
    !
    integer :: istat
    real(8) :: toc, tic
    ! CPU
    call output_prefactor(prefac_)
    call output_stencil_coefficients(stencil_coe)
    ! GPU
    allocate(rho_d(nr1,nr2,nr3),pot_d(nr1,nr2,nr3))
    call CPU_TIME(tic)
    istat = cudaMemcpy(pot_d,pot,nr1*nr2*nr3)
    prefac_d = prefac_
    stencil_coe_d = stencil_coe
    !
    tBlock = dim3(10,10,10) ! FIXME: arch specific settins...
    grid = dim3(ceiling(real(nr1)/tBlock%x), &
      &         ceiling(real(nr2)/tBlock%y), &
      &         ceiling(real(nr3)/tBlock%z))
    call laplacian_sweep_cuda<<<grid, tBlock>>>(nr1,nr2,nr3, pot_d, &
      & prefac_d, nord2, stencil_coe_d, rho_d)
    istat = cudaGetLastError() 
    if (istat .ne. 0) print*, cudaGetErrorString(istat)
    istat = cudaMemcpy(rho_ours,rho_d,nr1*nr2*nr3)
    call CPU_TIME(toc)
    print *, 'GPU TIME: ', (toc-tic)
    deallocate(rho_d,pot_d)
    return
  end subroutine compute_numerical_laplacian_for_density_external_gpu

  subroutine  check_laplacian()
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: abs_dev
    do ig3 = 1+nord2, nr3-nord2
      do ig2 = 1+nord2, nr2-nord2
        do ig1 = 1+nord2, nr1-nord2
          abs_dev = abs(rho_nk(ig1,ig2,ig3)-rho_ours(ig1,ig2,ig3))
          if (abs_dev.gt.1.d-8) then
            print *, ig1,ig2,ig3, abs_dev
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine check_laplacian

  !subroutine  test_numerical_vs_analytical(ig1,ig2,ig3)
  !  implicit none
  !  integer :: ig1, ig2, ig3
  !  real(8) :: abs_err_nk
  !  real(8) :: abs_err_ours
  !  abs_err_nk   = abs(rho_nk(ig1,ig2,ig3)  - rho(ig1,ig2,ig3))
  !  abs_err_ours = abs(rho_ours(ig1,ig2,ig3)- rho(ig1,ig2,ig3))
  !  !if (abs_err_nk.gt.eps10) then
  !  !  print *, 'nk  ', ig1,ig2,ig3,rho_nk(ig1,ig2,ig3),rho(ig1,ig2,ig3)
  !  !end if
  !  !print *, ig1,ig2,ig3,abs_err_nk/rho(ig1,ig2,ig3), abs_err_ours/rho(ig1,ig2,ig3)
  !  !if (abs_err_ours.gt.eps10) then
  !  !  print *, 'ours', ig1,ig2,ig3,rho_ours(ig1,ig2,ig3),rho(ig1,ig2,ig3)
  !  !end if
  !  return
  !end subroutine test_numerical_vs_analytical

  subroutine  finalize()
    implicit none
    call dealloc_grids
    return
  end subroutine finalize
end program main
