module laplacian_natan_kronik
  implicit none
  save
  integer, parameter :: nord1 = 3
  integer, parameter :: nord2 = 3
  real(8) :: coe_1d_1st(-nord1:nord1)
  real(8) :: coe_1d_2nd(-nord2:nord2)
  real(8) :: Jim(3,3)  ! [d/d x]        [d/d a]
  !          jacobian    |d/d y| = [J]  |d/d b|
  !                      [d/d z]        [d/d c]
  real(8) :: JtJ(3,3)  ! F in Natan-Kronik notation
  !enumerators
  integer, parameter :: drct_aa = 1
  integer, parameter :: drct_bb = 2
  integer, parameter :: drct_cc = 3
  integer, parameter :: drct_ab = 4
  integer, parameter :: drct_ac = 5
  integer, parameter :: drct_bc = 6
  !
  real(8) :: eta(drct_ab:drct_bc)    ! the index is for directions numerated above
  real(8) :: dd(drct_ab:drct_bc)     ! the index is for directions numerated above
  real(8) :: prefac(drct_aa:drct_bc) ! the index is for directions numerated above
  ! local wrappers
  real(8) :: h_(3,3)
  real(8) :: ainv_(3,3)
  real(8) :: dr_a
  real(8) :: dr_b
  real(8) :: dr_c
  ! local vars
  real(8), parameter :: pi=acos(-1.d0)
  real(8), parameter :: eps8=1.d-8
  real(8), parameter :: eps10=1.d-10
  real(8) :: lenA,lenB,lenC !length of lattice vectors
  real(8) :: e_a(3), e_b(3), e_c(3) ! unit lattice vectors
contains

  subroutine  init_derivatives(h, ainv, dr1, dr2, dr3)
    implicit none
    real(8), intent(in) :: h(3,3)
    real(8), intent(in) :: ainv(3,3)
    real(8), intent(in) :: dr1
    real(8), intent(in) :: dr2
    real(8), intent(in) :: dr3
    call take_input(h,ainv,dr1,dr2,dr3)
    call get_1d_central_difference_coefficeints
    call get_quadratic_form_of_jacobian
    call compute_prefactors_for_laplacian
    return
  end subroutine init_derivatives

  !
  subroutine  take_input(h, ainv, dr1, dr2, dr3)
    implicit none
    real(8), intent(in) :: h(3,3)
    real(8), intent(in) :: ainv(3,3)
    real(8), intent(in) :: dr1
    real(8), intent(in) :: dr2
    real(8), intent(in) :: dr3
    h_ = h
    ainv_ = ainv
    dr_a = dr1
    dr_b = dr2
    dr_c = dr3
    return
  end subroutine take_input

  !
  subroutine  get_1d_central_difference_coefficeints()
    implicit none
    integer :: ierr
    call fornberg(nord1,nord2,coe_1d_1st,coe_1d_2nd,ierr)
    return
  end subroutine get_1d_central_difference_coefficeints

  !
  subroutine  get_quadratic_form_of_jacobian()
    implicit none
    call compute_jacobian_abc_to_xyz
    JtJ = matmul(transpose(Jim),Jim)
    return
  end subroutine get_quadratic_form_of_jacobian

  !!
  subroutine  compute_jacobian_abc_to_xyz
    implicit none
    call compute_lattice_dimensions
    ! J = transpose(ainv_).(diag(a))
    Jim(:,1) = ainv_(1,:)*lenA
    Jim(:,2) = ainv_(2,:)*lenB ! i={xyz}, m={abc}
    Jim(:,3) = ainv_(3,:)*lenC
    return
  end subroutine compute_jacobian_abc_to_xyz

  !!!
  subroutine  compute_lattice_dimensions()
    implicit none
    lenA=sqrt(h_(1,1)*h_(1,1)+h_(2,1)*h_(2,1)+h_(3,1)*h_(3,1))
    lenB=sqrt(h_(1,2)*h_(1,2)+h_(2,2)*h_(2,2)+h_(3,2)*h_(3,2))
    lenC=sqrt(h_(1,3)*h_(1,3)+h_(2,3)*h_(2,3)+h_(3,3)*h_(3,3))
    return
  end subroutine compute_lattice_dimensions

  !
  subroutine  compute_prefactors_for_laplacian
    implicit none
    call get_effective_directions_spacings_for_mixed_derivatives
    call combine_mixed_derivatives_to_second_derivatives_in_prefactor
    call include_grid_spacings_to_prefactor
    return
  end subroutine compute_prefactors_for_laplacian

  !!
  subroutine  get_effective_directions_spacings_for_mixed_derivatives()
    implicit none
    call get_eta_function
    call get_dd_function
    return
  end subroutine get_effective_directions_spacings_for_mixed_derivatives

  !!!
  subroutine  get_eta_function()
    implicit none
    call compute_unit_lattice_vectors
    eta(drct_ab) = -dr_b/dr_a*sign(1.d0,dot_product(e_a,e_b))
    eta(drct_ac) = -dr_c/dr_a*sign(1.d0,dot_product(e_a,e_c))
    eta(drct_bc) = -dr_c/dr_b*sign(1.d0,dot_product(e_b,e_c))
    return
  end subroutine get_eta_function

  !!!!
  subroutine  compute_unit_lattice_vectors()
    implicit none
    e_a = h_(:,1)/lenA
    e_b = h_(:,2)/lenB
    e_c = h_(:,3)/lenC
    return
  end subroutine compute_unit_lattice_vectors

  !!!
  subroutine  get_dd_function()
    implicit none
    dd(drct_ab) = norm_sq(e_a+eta(drct_ab)*e_b)
    dd(drct_ac) = norm_sq(e_a+eta(drct_ac)*e_c)
    dd(drct_bc) = norm_sq(e_b+eta(drct_bc)*e_c)
    return
  end subroutine get_dd_function

  !!!!
  real(8) function norm_sq(v)
    implicit none
    real(8), intent(in) :: v(3)
    norm_sq = dot_product(v,v)
  end function norm_sq

  !!
  subroutine  combine_mixed_derivatives_to_second_derivatives_in_prefactor()
    implicit none
    prefac(drct_aa) = JtJ(1,1)-JtJ(1,2)/eta(drct_ab)-JtJ(1,3)/eta(drct_ac)
    prefac(drct_bb) = JtJ(2,2)-JtJ(1,2)*eta(drct_ab)-JtJ(2,3)/eta(drct_bc)
    prefac(drct_cc) = JtJ(3,3)-JtJ(1,3)*eta(drct_ac)-JtJ(2,3)*eta(drct_bc)
    prefac(drct_ab) = JtJ(1,2)*dd(drct_ab)/eta(drct_ab)
    prefac(drct_ac) = JtJ(1,3)*dd(drct_ac)/eta(drct_ac)
    prefac(drct_bc) = JtJ(2,3)*dd(drct_bc)/eta(drct_bc)
    return
  end subroutine combine_mixed_derivatives_to_second_derivatives_in_prefactor

  !!
  subroutine  include_grid_spacings_to_prefactor()
    implicit none
    real(8) :: dr_a2, dr_b2, dr_c2
    dr_a2 = dr_a*dr_a
    dr_b2 = dr_b*dr_b
    dr_c2 = dr_c*dr_c
    prefac(drct_aa) = prefac(drct_aa) / dr_a2
    prefac(drct_bb) = prefac(drct_bb) / dr_b2
    prefac(drct_cc) = prefac(drct_cc) / dr_c2
    prefac(drct_ab) = prefac(drct_ab) / (dr_a2*dd(drct_ab))
    prefac(drct_ac) = prefac(drct_ac) / (dr_a2*dd(drct_ac))
    prefac(drct_bc) = prefac(drct_bc) / (dr_b2*dd(drct_bc))
    return
  end subroutine include_grid_spacings_to_prefactor

  subroutine  compute_laplacian(v_stencil,lap)
    implicit none
    real(8), intent(in)  :: v_stencil(-nord2:nord2,drct_aa:drct_bc)
    real(8), intent(out) :: lap
    !call compute_laplacian_simple(v_stencil,lap)
    call compute_laplacian_fast(v_stencil,lap)
    return
  end subroutine compute_laplacian

  ! deprecated
  subroutine  compute_laplacian_simple(v_stencil,lap)
    implicit none
    real(8), intent(in)  :: v_stencil(-nord2:nord2,drct_aa:drct_bc)
    real(8), intent(out) :: lap
    real(8) :: lap_id
    integer :: ii, id
    lap = 0.d0
    do id = drct_aa, drct_bc
      lap_id = 0.d0
      do ii = -nord2,nord2
        !speed up by using symmetry of coe_1d_2nd
        lap_id = lap_id + coe_1d_2nd(ii)*v_stencil(ii,id)
      end do ! ii
      lap = lap + prefac(id)*lap_id 
    end do ! id
    return
  end subroutine compute_laplacian_simple

  ! deprecated
  subroutine  compute_laplacian_symmetrize_coe(v_stencil,lap)
    implicit none
    real(8), intent(in)  :: v_stencil(-nord2:nord2,drct_aa:drct_bc)
    real(8), intent(out) :: lap
    real(8) :: lap_id
    integer :: ii, id
    lap = sum(prefac)*coe_1d_2nd(0)*v_stencil(0,1)
    do id = drct_aa, drct_bc
      lap_id = 0.d0
      do ii = 1,nord2
        lap_id = lap_id + coe_1d_2nd(ii)*(v_stencil(-ii,id)+v_stencil(ii,id))
      end do ! ii
      lap = lap + prefac(id)*lap_id 
    end do ! id
    return
  end subroutine compute_laplacian_symmetrize_coe

  !
  subroutine  compute_laplacian_fast(v_stencil,lap)
    implicit none
    real(8), intent(in)  :: v_stencil(-nord2:nord2,drct_aa:drct_bc)
    real(8), intent(out) :: lap
    real(8) :: lap_id
    integer :: ii, id
    lap = sum(prefac)*coe_1d_2nd(0)*v_stencil(0,drct_aa)
    do id = drct_aa, drct_bc
      if (abs(prefac(id)).lt.eps8) cycle
      lap_id = 0.d0
      do ii = 1,nord2
        lap_id = lap_id + coe_1d_2nd(ii)*(v_stencil(-ii,id)+v_stencil(ii,id))
      end do ! ii
      !if (prefac(id).lt.eps8) print *, id,prefac(id),lap_id
      lap = lap + prefac(id)*lap_id 
    end do ! id
    return
  end subroutine compute_laplacian_fast

  subroutine  output_prefactor(prefac_)
    implicit none
    real(8), intent(out)  :: prefac_(drct_aa:drct_bc)
    prefac_ = prefac
    return
  end subroutine output_prefactor

  subroutine  output_stencil_coefficients(stencil_coe)
    ! this routine is useful is one wants to compute the sweep externally
    ! basically: lap = sum(stencil_coe*v_stencil)
    implicit none
    real(8), intent(out)  :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    integer :: ii, id
    do id = drct_aa, drct_bc
      stencil_coe(-nord2:nord2,id) = prefac(id)*coe_1d_2nd(-nord2:nord2)
    end do ! id
    return
  end subroutine output_stencil_coefficients

  !! external library:
  !===============================================================
  !
  !Coefficients for the first & second order numerical derivative
  !under the centered finite difference scheme.
  !Bengt Fornberg,  Exxon Res. & Eng. Co., NJ 08801
  !'bfornbe@erenj.com'
  !David M. Sloan,  Dept. of Mathematics, U. of Strathclyde,
  !Glasgow G1 1HX, Scotland,  'd.sloan@strath.ac.uk'
  !Acta Numerica 94,  Cambridge Univ. Press (1994)
  !
  !===============================================================
  subroutine fornberg(norder1,norder2,coe1,coe,ierr)
    !
    !     add functionality for the general cell
    !     we follow B. Fornberg in  Math. Comp. 51 (1988), 699-706
    !
    implicit none
    !
    !     Input/Output variables:
    !
    !     order of expansion of derivative. 
    !     it is the number of neighbors used ON ONE SIDE.
    !     the maximum order implemented is 20.
    INTEGER, INTENT(IN) :: norder1, norder2
    !
    !  coe1 - coefficients for the first derivative
    REAL(8), INTENT(OUT) :: coe1(-norder1:norder1)
    !
    !  coe  - coefficients for the axial derivative (2 nd)
    REAL(8), INTENT(OUT) :: coe(-norder2:norder2)
    !
    !     ierr - error flag
    INTEGER, INTENT(OUT) :: ierr
    !     
    !     Work variables:
    !
    !     counters 
    INTEGER i
    !     ---------------------------------------------------------------
    !
    !     First order derivative
    !
    ierr = 0
    !
    SELECT CASE (norder1)
    CASE (1)
      coe1(1) =  0.50000000000000D+00
    CASE (2)
      coe1(1) =  2.d0/3.d0
      coe1(2) = -1.d0/12.d0
    CASE (3)
      coe1(1) =  3.d0/4.d0
      coe1(2) = -3.d0/20.d0
      coe1(3) =  1.d0/60.d0
    CASE (4)
      coe1(1) =  4.d0/5.d0
      coe1(2) = -1.d0/5.d0
      coe1(3) =  4.d0/105.d0
      coe1(4) = -1.d0/280.d0
    CASE (5)
      coe1(1) =  0.8333333333D+00
      coe1(2) = -0.2380952381D+00
      coe1(3) =  0.5952380952D-01
      coe1(4) = -0.9920634921D-02
      coe1(5) =  0.7936507937D-03
    CASE (6)
      coe1(1) =  0.8571428571D+00
      coe1(2) = -0.2678571429D+00
      coe1(3) =  0.7936507937D-01
      coe1(4) = -0.1785714286D-01
      coe1(5) =  0.2597402597D-02
      coe1(6) = -0.1803751804D-03
    CASE (7)
      coe1(1) =  0.8750000000D+00
      coe1(2) = -0.2916666667D+00
      coe1(3) =  0.9722222222D-01
      coe1(4) = -0.2651515152D-01
      coe1(5) =  0.5303030303D-02
      coe1(6) = -0.6798756799D-03
      coe1(7) =  0.4162504163D-04
    CASE (8)
      coe1(1) =  0.8888888889D+00
      coe1(2) = -0.3111111111D+00
      coe1(3) =  0.1131313131D+00
      coe1(4) = -0.3535353535D-01
      coe1(5) =  0.8702408702D-02
      coe1(6) = -0.1554001554D-02
      coe1(7) =  0.1776001776D-03
      coe1(8) = -0.9712509713D-05
    CASE (9)
      coe1(1) =  0.9000000000D+00
      coe1(2) = -0.3272727273D+00
      coe1(3) =  0.1272727273D+00
      coe1(4) = -0.4405594406D-01
      coe1(5) =  0.1258741259D-01
      coe1(6) = -0.2797202797D-02
      coe1(7) =  0.4495504496D-03
      coe1(8) = -0.4627725216D-04
      coe1(9) =  0.2285296403D-05
    CASE (10)
      coe1(1) =  0.9090909091D+00
      coe1(2) = -0.3409090909D+00
      coe1(3) =  0.1398601399D+00
      coe1(4) = -0.5244755245D-01
      coe1(5) =  0.1678321678D-01
      coe1(6) = -0.4370629371D-02
      coe1(7) =  0.8814714697D-03
      coe1(8) = -0.1285479227D-03
      coe1(9) =  0.1202787580D-04
      coe1(10)= -0.5412544112D-06
    END SELECT
    !
    coe1(0) = 0.0d0
    DO i = 1,norder1
      coe1(-i) = -coe1(i)
    END DO
    !
    !     Second order derivative
    !
    SELECT CASE (norder2)
    CASE (1)
      coe(0) = -0.20000000000000D+01
      coe(1) =  0.10000000000000D+01
    CASE (2)
      coe(0) = -0.25000000000000D+01
      coe(1) =  0.13333333333333D+01
      coe(2) = -0.83333333333333D-01
    CASE (3)
      coe(0) = -0.27222222222222D+01
      coe(1) =  0.15000000000000D+01
      coe(2) = -0.15000000000000D+00
      coe(3) =  0.11111111111111D-01
    CASE (4)
      coe(0) = -0.28472222222222D+01
      coe(1) =  0.16000000000000D+01
      coe(2) = -0.20000000000000D+00
      coe(3) =  0.25396825396825D-01
      coe(4) = -0.17857142857143D-02
    CASE (5)
      coe(0) = -0.29272222222222D+01
      coe(1) =  0.16666666666667D+01
      coe(2) = -0.23809523809524D+00
      coe(3) =  0.39682539682540D-01
      coe(4) = -0.49603174603175D-02
      coe(5) =  0.31746031746032D-03
    CASE (6)
      coe(0) = -0.29827777777778D+01
      coe(1) =  0.17142857142857D+01
      coe(2) = -0.26785714285714D+00
      coe(3) =  0.52910052910053D-01
      coe(4) = -0.89285714285714D-02
      coe(5) =  0.10389610389610D-02
      coe(6) = -0.60125060125060D-04
    CASE (7)
      coe(0) = -0.30235941043084D+01
      coe(1) =  0.17500000000000D+01
      coe(2) = -0.29166666666667D+00
      coe(3) =  0.64814814814815D-01
      coe(4) = -0.13257575757576D-01
      coe(5) =  0.21212121212121D-02
      coe(6) = -0.22662522662523D-03
      coe(7) =  0.11892869035726D-04
    CASE (8)
      coe(0) = -0.30548441043084D+01
      coe(1) =  0.17777777777778D+01
      coe(2) = -0.31111111111111D+00
      coe(3) =  0.75420875420875D-01
      coe(4) = -0.17676767676768D-01
      coe(5) =  0.34809634809635D-02
      coe(6) = -0.51800051800052D-03
      coe(7) =  0.50742907885765D-04
      coe(8) = -0.24281274281274D-05
    CASE (9)
      coe(0) = -0.30795354623331D+01
      coe(1) =  0.18000000000000D+01
      coe(2) = -0.32727272727273D+00
      coe(3) =  0.84848484848485D-01
      coe(4) = -0.22027972027972D-01
      coe(5) =  0.50349650349650D-02
      coe(6) = -0.93240093240093D-03
      coe(7) =  0.12844298558584D-03
      coe(8) = -0.11569313039901D-04
      coe(9) =  0.50784364509855D-06
    CASE (10)
      coe(0) = -0.30995354623331D+01
      coe(1) =  0.18181818181818D+01
      coe(2) = -0.34090909090909D+00
      coe(3) =  0.93240093240093D-01
      coe(4) = -0.26223776223776D-01
      coe(5) =  0.67132867132867D-02
      coe(6) = -0.14568764568765D-02
      coe(7) =  0.25184899134479D-03
      coe(8) = -0.32136980666392D-04
      coe(9) =  0.26728612899924D-05
      coe(10)= -0.10825088224469D-06
    END SELECT
    !
    DO i = 1,norder2
      coe(-i) = coe(i)
    END DO
    !
    RETURN
  end subroutine fornberg
end module laplacian_natan_kronik
