!
! Author    : Hsin-Yu Ko
! Started at: 26.07.19
!
!program read_sparse
!  implicit none
!  call read_sparse_octave
!  stop
!end program read_sparse

  subroutine  read_sparse_octave()
    implicit none
    real(8), allocatable :: L(:,:)
    character(len=10) :: dum1, dum2
    integer :: u_l
    integer :: nnz, nr, nc, il, ir, ic
    real(8) :: val
    open (newunit=u_l, file='L.dat', action='read')
    read(u_l,*)                 ! # Created by Octave ...
    read(u_l,*)                 ! # name: L
    read(u_l,*)                 ! # type: sparse matrix
    read(u_l,*) dum1, dum2, nnz ! # nnz: ...
    read(u_l,*) dum1, dum2, nr  ! # rows: 8000
    read(u_l,*) dum1, dum2, nc  ! # columns: 8000
    allocate(L(nr,nc)); L = 0.d0
    do il = 1, nnz
      read(u_l,*) ir, ic, val
      L(ir,ic) = val
    end do ! il
    print *, L(1205, 2)
    close(u_l)
    deallocate(L)
    return
  end subroutine read_sparse_octave
