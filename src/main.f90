!
! Author    : Hsin-Yu Ko
! Started at: 15.01.18
!
program main
  use constants
  use system
  use grids
  !
  use laplacian_natan_kronik, only : eps10
  use laplacian_natan_kronik, only : init_derivatives_nk  => init_derivatives
  use laplacian_natan_kronik, only : compute_laplacian_nk => compute_laplacian
  use laplacian_natan_kronik, only : output_prefactor
  use laplacian_natan_kronik, only : output_stencil_coefficients
  use laplacian_natan_kronik, only : nord2
  use laplacian_natan_kronik, only : eta
  use laplacian_natan_kronik, only : drct_aa, drct_bb, drct_cc
  use laplacian_natan_kronik, only : drct_ab, drct_ac, drct_bc
  !
  implicit none
  real(8) :: tic, toc
  real(8) :: t_internal,t_external
  call initialize
  !
  CALL CPU_TIME(tic)
  call compute_numerical_laplacian_for_density
  CALL CPU_TIME(toc)
  t_internal = toc - tic
  print *, 't_internal:', t_internal
  !
  CALL CPU_TIME(tic)
  call compute_numerical_laplacian_for_density_external
  CALL CPU_TIME(toc)
  t_external = toc - tic
  print *, 't_external:', t_external
  !
  call check_laplacian
  call finalize
  stop
contains

  subroutine  initialize()
    implicit none
    call init_system
    call init_grids
    call init_derivatives_nk(h, ainv, dr1, dr2, dr3)
    return
  end subroutine initialize

  subroutine  compute_numerical_laplacian_for_density()
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: v_stencil(-nord2:nord2,drct_aa:drct_bc)
    real(8) :: v_stencil_2d(-nord2:nord2,drct_aa:drct_cc)
    real(8) :: v_stencil_cross(-nord2:nord2,2,drct_ab:drct_bc)
    real(8) :: lapl_v
    real(8) :: energy_nk
    rho_nk = 0.d0
    ! NOTE for openacc:
    !   pot (potential) should be copyin
    do ig3 = 1+nord2, nr3-nord2
      do ig2 = 1+nord2, nr2-nord2
        do ig1 = 1+nord2, nr1-nord2
          call fill_v_stencils_nk(ig1,ig2,ig3,v_stencil)
          call compute_laplacian_nk(v_stencil, lapl_v)
          rho_nk(ig1,ig2,ig3) = lapl_v/(-4.d0*pi)
          energy_nk = energy_nk + v_stencil(0,1) * rho_nk(ig1,ig2,ig3)
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine compute_numerical_laplacian_for_density

  subroutine  fill_v_stencils_nk(ig1,ig2,ig3,v_stencil)
    implicit none
    integer, intent(in)  :: ig1, ig2, ig3
    real(8), intent(out) :: v_stencil(-nord2:nord2,drct_aa:drct_bc)
    integer :: ii
    do ii = -nord2,nord2
      v_stencil(ii,drct_aa) = pot(ig1+ii, ig2   , ig3   )
      v_stencil(ii,drct_bb) = pot(ig1   , ig2+ii, ig3   )
      v_stencil(ii,drct_cc) = pot(ig1   , ig2   , ig3+ii)
      if (eta(drct_ab).gt.0.d0) then
        !print *,'drct_ab pos'
        v_stencil(ii,drct_ab) = pot(ig1+ii, ig2+ii, ig3   )
      else
        !print *,'drct_ab neg'
        v_stencil(ii,drct_ab) = pot(ig1+ii, ig2-ii, ig3   )
      end if
      if (eta(drct_ac).gt.0.d0) then
        !print *,'drct_ac pos'
        v_stencil(ii,drct_ac) = pot(ig1+ii, ig2   , ig3+ii)
      else
        !print *,'drct_ac neg'
        v_stencil(ii,drct_ac) = pot(ig1+ii, ig2   , ig3-ii)
      end if
      if (eta(drct_bc).gt.0.d0) then
        !print *,'drct_bc pos'
        v_stencil(ii,drct_bc) = pot(ig1   , ig2+ii, ig3+ii)
      else
        !print *,'drct_bc neg'
        v_stencil(ii,drct_bc) = pot(ig1   , ig2+ii, ig3-ii)
      end if
    end do ! ii
    return
  end subroutine fill_v_stencils_nk

  subroutine  compute_numerical_laplacian_for_density_external()
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: v_stencil(-nord2:nord2)
    real(8) :: v_stencil_2d(-nord2:nord2,drct_aa:drct_cc)
    real(8) :: prefac_(drct_aa:drct_bc)
    real(8) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    real(8) :: action_tmp
    !
    integer :: ii, id
    rho_ours = 0.d0
    call output_prefactor(prefac_)
    call output_stencil_coefficients(stencil_coe)
    do id = drct_aa, drct_bc
      if (abs(prefac_(id)).gt.1.d-8) then
        do ig3 = 1+nord2, nr3-nord2
          do ig2 = 1+nord2, nr2-nord2
            do ig1 = 1+nord2, nr1-nord2
              call fill_v_stencil_1d(ig1,ig2,ig3,id,v_stencil)
              action_tmp = stencil_coe(0,id)*v_stencil(0)
              do ii = 1, nord2
                action_tmp = action_tmp &
                  & + stencil_coe(ii,id)*(v_stencil(ii)+v_stencil(-ii))
              end do ! ii
              rho_ours(ig1,ig2,ig3) = rho_ours(ig1,ig2,ig3) + action_tmp
            end do ! ig1
          end do ! ig2
        end do ! ig3
      end if
    end do ! id
    rho_ours = rho_ours/(-4.d0*pi)
    return
  end subroutine compute_numerical_laplacian_for_density_external

  subroutine  fill_v_stencil_1d(ig1,ig2,ig3,id,v_stencil)
    implicit none
    integer, intent(in)  :: ig1, ig2, ig3, id
    real(8), intent(out) :: v_stencil(-nord2:nord2)
    integer :: ii
    select case (id)
    case (drct_aa)
      do ii = -nord2,nord2
        v_stencil(ii) = pot(ig1+ii, ig2   , ig3   )
      end do ! ii
    case (drct_bb)
      do ii = -nord2,nord2
        v_stencil(ii) = pot(ig1   , ig2+ii, ig3   )
      end do ! ii
    case (drct_cc)
      do ii = -nord2,nord2
        v_stencil(ii) = pot(ig1   , ig2   , ig3+ii)
      end do ! ii
    case (drct_ab)
      if (eta(drct_ab).gt.0.d0) then
        do ii = -nord2,nord2
          v_stencil(ii) = pot(ig1+ii, ig2+ii, ig3   )
        end do ! ii
      else
        do ii = -nord2,nord2
          v_stencil(ii) = pot(ig1+ii, ig2-ii, ig3   )
        end do ! ii
      end if
    case (drct_ac)
      if (eta(drct_ac).gt.0.d0) then
        do ii = -nord2,nord2
          v_stencil(ii) = pot(ig1+ii, ig2   , ig3+ii)
        end do ! ii
      else
        do ii = -nord2,nord2
          v_stencil(ii) = pot(ig1+ii, ig2   , ig3-ii)
        end do ! ii
      end if
    case (drct_bc)
      if (eta(drct_bc).gt.0.d0) then
        do ii = -nord2,nord2
          v_stencil(ii) = pot(ig1   , ig2+ii, ig3+ii)
        end do ! ii
      else
        do ii = -nord2,nord2
          v_stencil(ii) = pot(ig1   , ig2+ii, ig3-ii)
        end do ! ii
      end if
    case default
    end select ! id
    return
  end subroutine fill_v_stencil_1d

  subroutine  check_laplacian()
    implicit none
    write(*,*) 'check_laplacian_1'
    call check_laplacian_1
    write(*,*) 'check_laplacian_2'
    call check_laplacian_2
    return
  end subroutine check_laplacian

  subroutine  check_laplacian_1()
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: abs_dev
    do ig3 = 1+nord2, nr3-nord2
      do ig2 = 1+nord2, nr2-nord2
        do ig1 = 1+nord2, nr1-nord2
          abs_dev = abs(rho_nk(ig1,ig2,ig3)-rho(ig1,ig2,ig3))
          if (abs_dev.gt.1.d-8) then
            print *, ig1,ig2,ig3, abs_dev
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine check_laplacian_1

  subroutine  check_laplacian_2()
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: abs_dev
    do ig3 = 1+nord2, nr3-nord2
      do ig2 = 1+nord2, nr2-nord2
        do ig1 = 1+nord2, nr1-nord2
          abs_dev = abs(rho_ours(ig1,ig2,ig3)-rho(ig1,ig2,ig3))
          if (abs_dev.gt.1.d-8) then
            print *, ig1,ig2,ig3, abs_dev
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine check_laplacian_2

  !subroutine  test_numerical_vs_analytical(ig1,ig2,ig3)
  !  implicit none
  !  integer :: ig1, ig2, ig3
  !  real(8) :: abs_err_nk
  !  real(8) :: abs_err_ours
  !  abs_err_nk   = abs(rho_nk(ig1,ig2,ig3)  - rho(ig1,ig2,ig3))
  !  abs_err_ours = abs(rho_ours(ig1,ig2,ig3)- rho(ig1,ig2,ig3))
  !  !if (abs_err_nk.gt.eps10) then
  !  !  print *, 'nk  ', ig1,ig2,ig3,rho_nk(ig1,ig2,ig3),rho(ig1,ig2,ig3)
  !  !end if
  !  !print *, ig1,ig2,ig3,abs_err_nk/rho(ig1,ig2,ig3), abs_err_ours/rho(ig1,ig2,ig3)
  !  !if (abs_err_ours.gt.eps10) then
  !  !  print *, 'ours', ig1,ig2,ig3,rho_ours(ig1,ig2,ig3),rho(ig1,ig2,ig3)
  !  !end if
  !  return
  !end subroutine test_numerical_vs_analytical

  subroutine  finalize()
    implicit none
    call dealloc_grids
    return
  end subroutine finalize
end program main
