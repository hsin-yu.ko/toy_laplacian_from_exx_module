!
! Author    : Hsin-Yu Ko
! Started at: 15.01.18
!
program main
  !use constants
  !
  implicit none
  call initialize ! get analytical potential at this point...
  call initial_guess
  call check_error_norm_with_exact_result_init_guess
  call solve_for_potential
  call check_error_norm_with_exact_result
  call finalize
  stop
contains

  subroutine  initialize()
    use system, only : init_system, h, ainv
    use grids,  only : init_grids, dr1, dr2, dr3
    use laplacian_natan_kronik, only : init_derivatives
    use transfinite_interpolation, only : init_tfi
    implicit none
    call init_system
    call init_grids
    call init_derivatives(h, ainv, dr1, dr2, dr3)
    call init_tfi
    return
  end subroutine initialize

  subroutine  solve_for_potential()
    use grids, only : rho, pot_stdcg, pot, nr1, nr2, nr3
    implicit none
    integer :: iter, n(3)
    real(8), parameter :: eps=1.0d-6
    ! TODO apply boundary condition
    n(1) = nr1; n(2) = nr2; n(3) = nr3
    call stdcg(iter, n, eps, rho, pot_stdcg)
    return
  end subroutine solve_for_potential

  subroutine  initial_guess()
    implicit none
    integer :: u_guess
    integer :: guess_mode
    open(newunit=u_guess, file='guess.dat', action = 'read')
    read(u_guess,*) guess_mode
    close(u_guess)
    select case (guess_mode)
    case (1)
      call initial_guess_zero
    case (2)
      call initial_guess_lda(1.05827d0)
    case (3)
      call initial_guess_rand
    case (4)
      call initial_guess_neg_rho
    case (5)
      call initial_guess_interpolation
    case (6)
      call initial_guess_lda(1.31299d0)
    case (7)
      call initial_guess_interpolation_add_lda(1.05827d0)
    case (8)
      call initial_guess_interpolation_add_lda(1.31299d0)
    case (9)
      call initial_guess_hermite_cubic_tfi
    case default
      write(*,*) 'error: guess_mode selected not implemented. stop...'
      stop
    end select ! guess_mode
    return
  end subroutine initial_guess

  subroutine  initial_guess_zero()
    use grids, only : pot_stdcg
    implicit none
    pot_stdcg = 0.d0
    return
  end subroutine initial_guess_zero

  subroutine  initial_guess_lda(prefac)
    use grids, only : rho, pot_stdcg
    implicit none
    real(8), intent(in) :: prefac
    real(8), parameter :: one_third = 1.d0/3.d0
    real(8), allocatable :: arho(:,:,:)
    !pot_stdcg = prefac*(rho)**(one_third)
    allocate(arho, mold=rho)
    arho = abs(rho)
    pot_stdcg = prefac*(arho)**(one_third)
    pot_stdcg = sign(pot_stdcg,rho)
    deallocate(arho)
    return
  end subroutine initial_guess_lda

  subroutine  initial_guess_rand()
    use grids, only : pot_stdcg
    implicit none
    integer, allocatable :: seed(:)
    integer :: n, i
    call random_seed(size = n)
    allocate(seed(n))
    do i = 1,n
      seed = i
    end do ! i
    call random_seed(put=seed)
    deallocate(seed)
    !
    call random_number(pot_stdcg)
    return
  end subroutine initial_guess_rand

  subroutine  initial_guess_neg_rho()
    use grids, only : rho, pot_stdcg
    implicit none
    pot_stdcg = -rho
    return
  end subroutine initial_guess_neg_rho

  subroutine  initial_guess_interpolation()
    implicit none
    call initial_guess_linear_tfi
    return
  end subroutine initial_guess_interpolation

  subroutine  initial_guess_linear_tfi()
    use transfinite_interpolation, only : get_pot_on_boundary_and_halo
    use transfinite_interpolation, only : perform_linear_tfi_for_near_field_pot
    !use transfinite_interpolation, only : perform_linear_tfi_for_near_field_pot_old
    implicit none
    !
    call get_pot_on_boundary_and_halo
    call perform_linear_tfi_for_near_field_pot
    !call perform_linear_tfi_for_near_field_pot_old
    !
    return
  end subroutine initial_guess_linear_tfi

  subroutine  initial_guess_hermite_cubic_tfi()
    use transfinite_interpolation, only : get_pot_on_boundary_and_halo
    use transfinite_interpolation, only : get_dpot_on_boundary_and_halo
    use transfinite_interpolation, only : perform_hermite_cubic_tfi_for_near_field_pot
    !use transfinite_interpolation, only : perform_hermite_cubic_tfi_for_near_field_pot_old
    !
    use grids, only : pot_stdcg
    use grids, only : nr1, nr2, nr3
    use transfinite_interpolation, only : pot_w_halo
    implicit none
    !
    call get_pot_on_boundary_and_halo
    call get_dpot_on_boundary_and_halo
    !call perform_hermite_cubic_tfi_for_near_field_pot
    !call perform_hermite_cubic_tfi_for_near_field_pot_old
    call  linear_transfinite_interpolation_serial((/nr1,nr2,nr3/),&
      & pot_w_halo(0:nr1+1,0:nr2+1,0:nr3+1))
    pot_stdcg(1:nr1,1:nr2,1:nr3) = pot_w_halo(1:nr1,1:nr2,1:nr3)
    !
    return
  end subroutine initial_guess_hermite_cubic_tfi

  subroutine  initial_guess_interpolation_old()
    use laplacian_natan_kronik, only : nord2
    use system, only : h, q_cen, n_q
    use grids, only : v_of_r, nr1, nr2, nr3
    use grids, only : rho, pot_stdcg
    implicit none
    integer :: ig1, ig2, ig3
    logical :: is_halo_1, is_halo_2, is_halo_3
    real(8) :: r_(3), r
    real(8) :: x,y,z
    real(8), allocatable :: pot_w_halo(:,:,:)
    integer :: iq
    !print *, 'warning: this method of initial guess is only tested for cubic case...'
    pot_stdcg = 0.d0
    !
    ! eval halo
    allocate(pot_w_halo(1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    pot_w_halo = 0.d0
    do ig3 = 1-nord2, nr3+nord2
      is_halo_3 = .not.(ig3.gt.1.and.ig3.lt.nr3)
      do ig2 = 1-nord2, nr2+nord2
        is_halo_2 = .not.(ig2.gt.1.and.ig2.lt.nr2)
        do ig1 = 1-nord2, nr1+nord2
          is_halo_1 = .not.(ig1.gt.1.and.ig1.lt.nr1)
          if (is_halo_1.or.is_halo_2.or.is_halo_3) then
            do iq = 1, n_q
              r_ = (/dble(ig1)/dble(nr1),dble(ig2)/dble(nr2),dble(ig3)/dble(nr3)/)
              r_ = matmul(h, r_)
              r = norm2(r_-q_cen(:,iq))
              pot_w_halo (ig1,ig2,ig3) = pot_w_halo (ig1,ig2,ig3) + v_of_r(iq,r)
            end do ! iq
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    !pot_stdcg(:,:,:) = pot_w_halo (1:nr1,1:nr2,1:nr3)
    !
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          x = (dble(ig1)/dble(nr1))
          y = (dble(ig2)/dble(nr2))
          z = (dble(ig3)/dble(nr3))
          pot_stdcg(ig1,ig2,ig3) = &
            & (1 - x)*pot_w_halo(1,ig2,ig3)                       & ! (1 - x)*f[0, y, z] 
            & + x*pot_w_halo(nr1,ig2,ig3)                            & ! + x*f[1, y, z] 
            & + (1 - y)*pot_w_halo(ig1,1,ig3)                     & ! + (1 - y)*f[x, 0, z] 
            & + y*pot_w_halo(ig1, nr2, ig3)                          & ! + y*f[x, 1, z] 
            & + (1 - z)*pot_w_halo(ig1,ig2,1)                     & ! + (1 - z)*f[x, y, 0]
            & + z*pot_w_halo(ig1,ig2,nr3)                            & ! + z*f[x, y, 1]
            & + (1 - x)*((1 - y)*((1 - z)*pot_w_halo(1,1,1) & ! + (1 - x)*((1 - y)*((1 - z)*f[0, 0, 0]
            & + z*pot_w_halo(1,1,nr3))                               & ! + z*f[0, 0, 1])
            & + y*((1 - z)*pot_w_halo(1,nr2,1)                    & ! + y*((1 - z)*f[0, 1, 0]
            & + z*pot_w_halo(1, nr2, nr3)))                          & ! + z*f[0, 1, 1]))
            & + x*((1 - y)*((1 - z)*pot_w_halo(nr1, 1, 1)      & ! + x*((1 - y)*((1 - z)*f[1, 0, 0]
            & + z*pot_w_halo(nr1, 1, nr3))                           & ! + z*f[1, 0, 1])
            & + y*((1 - z)*pot_w_halo(nr1, nr2, 1)                & ! + y*((1 - z)*f[1, 1, 0]
            & + z*pot_w_halo(nr1, nr2, nr3)))                        & ! + z*f[1, 1, 1]))
            & - (1 - x)*((1 - y)*pot_w_halo(1, 1, ig3)         & ! - (1 - x)*((1 - y)*f[0, 0, z] 
            & + y*pot_w_halo(1, nr2, ig3))                           & ! + y*f[0, 1, z])
            & - x*((1 - y)*pot_w_halo(nr1, 1, ig3)                & ! - x*((1 - y)*f[1, 0, z] 
            & + y*pot_w_halo(nr1, nr2, ig3))                         & ! + y*f[1, 1, z]) 
            & - (1 - y)*((1 - z)*pot_w_halo(ig1, 1, 1)         & ! - (1 - y)*((1 - z)*f[x, 0, 0] 
            & + z*pot_w_halo(ig1, 1, nr3))                           & ! + z*f[x, 0, 1]) 
            & - y*((1 - z)*pot_w_halo(ig1, nr2, 1)                & ! - y*((1 - z)*f[x, 1, 0] 
            & + z*pot_w_halo(ig1, nr2, nr3))                         & ! + z*f[x, 1, 1]) 
            & - (1 - z)*((1 - x)*pot_w_halo(1, ig2, 1)         & ! - (1 - z)*((1 - x)*f[0, y, 0] 
            & + x*pot_w_halo(nr1, ig2, 1))                           & ! + x*f[1, y, 0]) 
            & - z*((1 - x)*pot_w_halo(1, ig2, nr3)                & ! - z*((1 - x)*f[0, y, 1] 
            & + x*pot_w_halo(nr1, ig2, nr3))                           ! + x*f[1, y, 1])
        end do ! ig1
      end do ! ig2
    end do ! ig3
    !
    deallocate(pot_w_halo)
    return
  end subroutine initial_guess_interpolation_old

  subroutine  initial_guess_interpolation_hermite_cubic()
    use laplacian_natan_kronik, only : nord2
    use system, only : h, q_cen, n_q
    use grids, only : v_of_r, dvdr_of_r, nr1, nr2, nr3
    use grids, only : rho, pot_stdcg
    implicit none
    integer :: ig1, ig2, ig3
    logical :: is_halo_1, is_halo_2, is_halo_3
    real(8) :: r_(3), r
    real(8) :: x,y,z
    real(8), allocatable :: pot_w_halo(:,:,:)
    real(8), allocatable :: dpot_w_halo(:,:,:,:)
    real(8) :: dvdr
    real(8) :: U, a10, a11, a20, a21
    integer :: iq
    !print *, 'warning: this method of initial guess is only tested for cubic case...'
    pot_stdcg = 0.d0
    !
    ! eval halo
    allocate(pot_w_halo(1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    allocate(dpot_w_halo(3,1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    pot_w_halo = 0.d0
    dpot_w_halo = 0.d0
    do ig3 = 1-nord2, nr3+nord2
      is_halo_3 = .not.(ig3.gt.1.and.ig3.lt.nr3)
      do ig2 = 1-nord2, nr2+nord2
        is_halo_2 = .not.(ig2.gt.1.and.ig2.lt.nr2)
        do ig1 = 1-nord2, nr1+nord2
          is_halo_1 = .not.(ig1.gt.1.and.ig1.lt.nr1)
          if (is_halo_1.or.is_halo_2.or.is_halo_3) then
            do iq = 1, n_q
              r_ = (/dble(ig1)/dble(nr1),dble(ig2)/dble(nr2),dble(ig3)/dble(nr3)/)
              r_ = matmul(h, r_)
              r = norm2(r_-q_cen(:,iq))
              pot_w_halo (ig1,ig2,ig3) = pot_w_halo (ig1,ig2,ig3) + v_of_r(iq,r)
              dvdr = dvdr_of_r(iq,r)
              if (r.gt.1.d-8) then ! dvdr -> 0 if otherwise
                dpot_w_halo (:,ig1,ig2,ig3) = dpot_w_halo (:,ig1,ig2,ig3) + dvdr*r_/r
              end if
            end do ! iq
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    !
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          x = (dble(ig1)/dble(nr1))
          y = (dble(ig2)/dble(nr2))
          z = (dble(ig3)/dble(nr3))
          !
          a10 = 2.d0*x*x*x - 3.d0*x*x + 1
          a11 = 2.d0*x*x*x - 3.d0*x*x + 1

          pot_stdcg(ig1,ig2,ig3) = &
            & (1 - x)*pot_w_halo(1,ig2,ig3)                       & ! (1 - x)*f[0, y, z] 
            & + x*pot_w_halo(nr1,ig2,ig3)                            & ! + x*f[1, y, z] 
            & + (1 - y)*pot_w_halo(ig1,1,ig3)                     & ! + (1 - y)*f[x, 0, z] 
            & + y*pot_w_halo(ig1, nr2, ig3)                          & ! + y*f[x, 1, z] 
            & + (1 - z)*pot_w_halo(ig1,ig2,1)                     & ! + (1 - z)*f[x, y, 0]
            & + z*pot_w_halo(ig1,ig2,nr3)                            & ! + z*f[x, y, 1]
            & + (1 - x)*((1 - y)*((1 - z)*pot_w_halo(1,1,1) & ! + (1 - x)*((1 - y)*((1 - z)*f[0, 0, 0]
            & + z*pot_w_halo(1,1,nr3))                               & ! + z*f[0, 0, 1])
            & + y*((1 - z)*pot_w_halo(1,nr2,1)                    & ! + y*((1 - z)*f[0, 1, 0]
            & + z*pot_w_halo(1, nr2, nr3)))                          & ! + z*f[0, 1, 1]))
            & + x*((1 - y)*((1 - z)*pot_w_halo(nr1, 1, 1)      & ! + x*((1 - y)*((1 - z)*f[1, 0, 0]
            & + z*pot_w_halo(nr1, 1, nr3))                           & ! + z*f[1, 0, 1])
            & + y*((1 - z)*pot_w_halo(nr1, nr2, 1)                & ! + y*((1 - z)*f[1, 1, 0]
            & + z*pot_w_halo(nr1, nr2, nr3)))                        & ! + z*f[1, 1, 1]))
            & - (1 - x)*((1 - y)*pot_w_halo(1, 1, ig3)         & ! - (1 - x)*((1 - y)*f[0, 0, z] 
            & + y*pot_w_halo(1, nr2, ig3))                           & ! + y*f[0, 1, z])
            & - x*((1 - y)*pot_w_halo(nr1, 1, ig3)                & ! - x*((1 - y)*f[1, 0, z] 
            & + y*pot_w_halo(nr1, nr2, ig3))                         & ! + y*f[1, 1, z]) 
            & - (1 - y)*((1 - z)*pot_w_halo(ig1, 1, 1)         & ! - (1 - y)*((1 - z)*f[x, 0, 0] 
            & + z*pot_w_halo(ig1, 1, nr3))                           & ! + z*f[x, 0, 1]) 
            & - y*((1 - z)*pot_w_halo(ig1, nr2, 1)                & ! - y*((1 - z)*f[x, 1, 0] 
            & + z*pot_w_halo(ig1, nr2, nr3))                         & ! + z*f[x, 1, 1]) 
            & - (1 - z)*((1 - x)*pot_w_halo(1, ig2, 1)         & ! - (1 - z)*((1 - x)*f[0, y, 0] 
            & + x*pot_w_halo(nr1, ig2, 1))                           & ! + x*f[1, y, 0]) 
            & - z*((1 - x)*pot_w_halo(1, ig2, nr3)                & ! - z*((1 - x)*f[0, y, 1] 
            & + x*pot_w_halo(nr1, ig2, nr3))                           ! + x*f[1, y, 1])
        end do ! ig1
      end do ! ig2
    end do ! ig3
    !
    deallocate(pot_w_halo)
    deallocate(dpot_w_halo)
    return
  end subroutine initial_guess_interpolation_hermite_cubic

  subroutine  initial_guess_interpolation_add_lda(prefac)
    use grids, only : rho, pot_stdcg
    implicit none
    real(8), intent(in) :: prefac
    real(8), parameter :: one_third = 1.d0/3.d0
    call initial_guess_interpolation
    pot_stdcg = pot_stdcg + prefac*(rho)**(one_third)
    return
  end subroutine initial_guess_interpolation_add_lda


  subroutine  check_error_norm_with_exact_result_init_guess()
    use grids, only : pot, pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: i,j,k
    print *, '|| v_exact ||_2 = ', norm2(pot)
    print *, '|| v_solved - v_exact ||_2 = ', norm2(pot_stdcg - pot)
    do k = 1, nr3
      do j = 1, nr2
        do i = 1, nr1
          write(99,*) pot_stdcg(i,j,k) , pot(i,j,k)
        end do ! i
      end do ! j
    end do ! k
    return
  end subroutine check_error_norm_with_exact_result_init_guess


  subroutine  check_error_norm_with_exact_result()
    use grids, only : pot, pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: i,j,k
    print *, '|| v_exact ||_2 = ', norm2(pot)
    print *, '|| v_solved - v_exact ||_2 = ', norm2(pot_stdcg - pot)
    do k = 1, nr3
      do j = 1, nr2
        do i = 1, nr1
          write(100,*) pot_stdcg(i,j,k) , pot(i,j,k)
        end do ! i
      end do ! j
    end do ! k
    return
  end subroutine check_error_norm_with_exact_result

  subroutine  finalize()
    use grids, only : dealloc_grids
    use system, only : dealloc_system
    use transfinite_interpolation, only : finalize_tfi
    implicit none
    call dealloc_grids
    call dealloc_system
    return
  end subroutine finalize
end program main
