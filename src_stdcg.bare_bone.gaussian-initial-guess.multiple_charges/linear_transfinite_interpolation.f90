! Synopsis:
!   This routine uses values of v in the first-shell of the halo to perform
!   linear TFI in the interior region...
!
! Authors:
!   Hsin-Yu Ko and Zachary Sparrow (2019-08-01)
!
subroutine  linear_transfinite_interpolation_serial(n,h,v)
  implicit none
  integer, intent(in   ) :: n(3)
  integer, intent(in   ) :: h ! size of halo on one side (must be >=1)
  real(8), intent(inout) :: v(1-h:n(1)+h, 1-h:n(2)+h, 1-h:n(3)+h)
  ! v(1:n(1),1:n(2),1:n(3)) is the interior and rest of the halo
  !
  integer :: ig1, ig2, ig3
  integer :: np(3) ! wrapper for n+1 to save flops
  real(8) :: x,y,z ! scale (crystal) coordinates mapping 0 and 1 upto the boundary of the halo)
  np(:) = n(:) + 1
  do ig3 = 1, n(3)
    z = dble(ig3)/dble(np(3))
    do ig2 = 1, n(2)
      y = dble(ig2)/dble(np(2))
      do ig1 = 1, n(1)
        x = dble(ig1)/dble(np(1))
        v(ig1,ig2,ig3) = &
          &   (1.d0 - x)*v(0,ig2,ig3)                     &
          & + x*v(np(1),ig2,ig3)                          &
          & + (1.d0 - y)*v(ig1,0,ig3)                     &
          & + y*v(ig1, np(2), ig3)                        &
          & + (1.d0 - z)*v(ig1,ig2,0)                     &
          & + z*v(ig1,ig2,np(3))                          &
          & + (1.d0 - x)*((1.d0 - y)*((1.d0 - z)*v(0,0,0) &
          & + z*v(0,0,np(3)))                             &
          & + y*((1.d0 - z)*v(0,np(2),0)                  &
          & + z*v(0, np(2), np(3))))                      &
          & + x*((1.d0 - y)*((1.d0 - z)*v(np(1), 0, 0)    &
          & + z*v(np(1), 0, np(3)))                       &
          & + y*((1.d0 - z)*v(np(1), np(2), 0)            &
          & + z*v(np(1), np(2), np(3))))                  &
          & - (1.d0 - x)*((1.d0 - y)*v(0, 0, ig3)         &
          & + y*v(0, np(2), ig3))                         &
          & - x*((1.d0 - y)*v(np(1), 0, ig3)              &
          & + y*v(np(1), np(2), ig3))                     &
          & - (1.d0 - y)*((1.d0 - z)*v(ig1, 0, 0)         &
          & + z*v(ig1, 0, np(3)))                         &
          & - y*((1.d0 - z)*v(ig1, np(2), 0)              &
          & + z*v(ig1, np(2), np(3)))                     &
          & - (1.d0 - z)*((1.d0 - x)*v(0, ig2, 0)         &
          & + x*v(np(1), ig2, 0))                         &
          & - z*((1.d0 - x)*v(0, ig2, np(3))              &
          & + x*v(np(1), ig2, np(3)))
      end do ! ig1
    end do ! ig2
  end do ! ig3
  return
end subroutine linear_transfinite_interpolation_serial

subroutine  linear_transfinite_interpolation_openmp(n,h,v)
  !TODO: need to be tested
  implicit none
  integer, intent(in   ) :: n(3)
  integer, intent(in   ) :: h ! size of halo on one side (must be >=1)
  real(8), intent(inout) :: v(1-h:n(1)+h, 1-h:n(2)+h, 1-h:n(3)+h)
  ! v(1:n(1),1:n(2),1:n(3)) is the interior and rest of the halo
  !
  integer :: ig1, ig2, ig3
  integer :: np(3) ! wrapper for n+1 to save flops
  real(8) :: x,y,z ! scale (crystal) coordinates mapping 0 and 1 upto the boundary of the halo)
  np(:) = n(:) + 1
  !$omp parallel do collapse(3), private(x,y,z)
  do ig3 = 1, n(3)
    do ig2 = 1, n(2)
      do ig1 = 1, n(1)
        x = dble(ig1)/dble(np(1))
        y = dble(ig2)/dble(np(2))
        z = dble(ig3)/dble(np(3))
        v(ig1,ig2,ig3) = &
          &   (1.d0 - x)*v(0,ig2,ig3)                     &
          & + x*v(np(1),ig2,ig3)                          &
          & + (1.d0 - y)*v(ig1,0,ig3)                     &
          & + y*v(ig1, np(2), ig3)                        &
          & + (1.d0 - z)*v(ig1,ig2,0)                     &
          & + z*v(ig1,ig2,np(3))                          &
          & + (1.d0 - x)*((1.d0 - y)*((1.d0 - z)*v(0,0,0) &
          & + z*v(0,0,np(3)))                             &
          & + y*((1.d0 - z)*v(0,np(2),0)                  &
          & + z*v(0, np(2), np(3))))                      &
          & + x*((1.d0 - y)*((1.d0 - z)*v(np(1), 0, 0)    &
          & + z*v(np(1), 0, np(3)))                       &
          & + y*((1.d0 - z)*v(np(1), np(2), 0)            &
          & + z*v(np(1), np(2), np(3))))                  &
          & - (1.d0 - x)*((1.d0 - y)*v(0, 0, ig3)         &
          & + y*v(0, np(2), ig3))                         &
          & - x*((1.d0 - y)*v(np(1), 0, ig3)              &
          & + y*v(np(1), np(2), ig3))                     &
          & - (1.d0 - y)*((1.d0 - z)*v(ig1, 0, 0)         &
          & + z*v(ig1, 0, np(3)))                         &
          & - y*((1.d0 - z)*v(ig1, np(2), 0)              &
          & + z*v(ig1, np(2), np(3)))                     &
          & - (1.d0 - z)*((1.d0 - x)*v(0, ig2, 0)         &
          & + x*v(np(1), ig2, 0))                         &
          & - z*((1.d0 - x)*v(0, ig2, np(3))              &
          & + x*v(np(1), ig2, np(3)))
      end do ! ig1
    end do ! ig2
  end do ! ig3
  !$omp end parallel do
  return
end subroutine linear_transfinite_interpolation_openmp

subroutine  linear_transfinite_interpolation_cuda(n,h,v)
  !TODO: need to be tested
  implicit none
  integer, intent(in   ) :: n(3)
  integer, intent(in   ) :: h ! size of halo on one side (must be >=1)
  real(8), intent(inout) :: v(1-h:n(1)+h, 1-h:n(2)+h, 1-h:n(3)+h)
  ! v(1:n(1),1:n(2),1:n(3)) is the interior and rest of the halo
  !
  integer :: ig1, ig2, ig3
  integer :: np(3) ! wrapper for n+1 to save flops
  real(8) :: x,y,z ! scale (crystal) coordinates mapping 0 and 1 upto the boundary of the halo)
  np(:) = n(:) + 1
  !$cuf kernel do (3)
  do ig3 = 1, n(3)
    do ig2 = 1, n(2)
      do ig1 = 1, n(1)
        x = dble(ig1)/dble(np(1))
        y = dble(ig2)/dble(np(2))
        z = dble(ig3)/dble(np(3))
        v(ig1,ig2,ig3) = &
          &   (1.d0 - x)*v(0,ig2,ig3)                     &
          & + x*v(np(1),ig2,ig3)                          &
          & + (1.d0 - y)*v(ig1,0,ig3)                     &
          & + y*v(ig1, np(2), ig3)                        &
          & + (1.d0 - z)*v(ig1,ig2,0)                     &
          & + z*v(ig1,ig2,np(3))                          &
          & + (1.d0 - x)*((1.d0 - y)*((1.d0 - z)*v(0,0,0) &
          & + z*v(0,0,np(3)))                             &
          & + y*((1.d0 - z)*v(0,np(2),0)                  &
          & + z*v(0, np(2), np(3))))                      &
          & + x*((1.d0 - y)*((1.d0 - z)*v(np(1), 0, 0)    &
          & + z*v(np(1), 0, np(3)))                       &
          & + y*((1.d0 - z)*v(np(1), np(2), 0)            &
          & + z*v(np(1), np(2), np(3))))                  &
          & - (1.d0 - x)*((1.d0 - y)*v(0, 0, ig3)         &
          & + y*v(0, np(2), ig3))                         &
          & - x*((1.d0 - y)*v(np(1), 0, ig3)              &
          & + y*v(np(1), np(2), ig3))                     &
          & - (1.d0 - y)*((1.d0 - z)*v(ig1, 0, 0)         &
          & + z*v(ig1, 0, np(3)))                         &
          & - y*((1.d0 - z)*v(ig1, np(2), 0)              &
          & + z*v(ig1, np(2), np(3)))                     &
          & - (1.d0 - z)*((1.d0 - x)*v(0, ig2, 0)         &
          & + x*v(np(1), ig2, 0))                         &
          & - z*((1.d0 - x)*v(0, ig2, np(3))              &
          & + x*v(np(1), ig2, np(3)))
      end do ! ig1
    end do ! ig2
  end do ! ig3
  return
end subroutine linear_transfinite_interpolation_cuda
