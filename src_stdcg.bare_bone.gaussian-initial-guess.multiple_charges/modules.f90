module constants
  implicit none
  save
  real(8), parameter :: pi=acos(-1.d0)
  real(8), parameter :: eps8=1.d-8
end module constants

module system
  implicit none
  real(8) :: h(3,3)
  real(8) :: ainv(3,3)
  real(8) :: volume
  integer :: n_q
  real(8), allocatable :: q_cen(:,:)
  real(8), allocatable :: q_sig(:)
  real(8), allocatable :: q(:)
contains

  subroutine  init_system()
    implicit none
    call read_charge_input
    call read_cell_input
    call inv3x3_mat(h, ainv, volume)
    return
  end subroutine init_system

  subroutine  read_charge_input()
    implicit none
    integer :: iq
    open(unit=100, file='charges.dat', action='read')
    read(100,*) n_q
    allocate(q_cen(3,n_q)); q_cen = 0.d0
    allocate(q_sig(n_q)); q_sig = 0.d0
    allocate(q(n_q)); q = 0.d0
    do iq = 1, n_q
      read(100,*) q_cen(:,iq)
      read(100,*) q_sig(iq)
      read(100,*) q(iq)
    end do ! iq
    close(100)
    return
  end subroutine read_charge_input

  subroutine  read_cell_input()
    implicit none
    open(unit=100, file='cel.dat', action='read')
    read(100,*) h(1,:)
    read(100,*) h(2,:)
    read(100,*) h(3,:)
    close(100)
    return
  end subroutine read_cell_input

  subroutine inv3x3_mat(A,T,D)
    implicit none 
    real(8), intent(in)  :: A(3,3)
    real(8), intent(out) :: T(3,3)
    real(8), intent(out) :: D           ! det
    D = A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)- &
      A(1,2)*A(2,1)*A(3,3)+A(1,2)*A(2,3)*A(3,1)+ &
      A(1,3)*A(2,1)*A(3,2)-A(1,3)*A(2,2)*A(3,1)
    T(1,1) = (A(2,2)*A(3,3)-A(2,3)*A(3,2))/D
    T(1,2) = -(A(1,2)*A(3,3)-A(1,3)*A(3,2))/D
    T(1,3) = (A(1,2)*A(2,3)-A(1,3)*A(2,2))/D
    T(2,1) = -(A(2,1)*A(3,3)-A(2,3)*A(3,1))/D
    T(2,2) = (A(1,1)*A(3,3)-A(1,3)*A(3,1))/D
    T(2,3) = -(A(1,1)*A(2,3)-A(1,3)*A(2,1))/D
    T(3,1) = (A(2,1)*A(3,2)-A(2,2)*A(3,1))/D
    T(3,2) = -(A(1,1)*A(3,2)-A(1,2)*A(3,1))/D
    T(3,3) = (A(1,1)*A(2,2)-A(1,2)*A(2,1))/D
    return
  end subroutine inv3x3_mat 

  subroutine  dealloc_system()
    implicit none
    deallocate(q_cen)
    deallocate(q_sig)
    deallocate(q)
    return
  end subroutine dealloc_system
end module system

module grids
  use constants
  use system
  implicit none
  save
  integer :: nr1
  integer :: nr2
  integer :: nr3
  real(8) :: dr1, dr2, dr3
  real(8), allocatable :: pos(:,:,:,:)
  real(8), allocatable :: r  (:,:,:)
  real(8), allocatable :: r2 (:,:,:)
  real(8), allocatable :: rho(:,:,:)
  real(8), allocatable :: pot(:,:,:)
  real(8), allocatable :: pot_stdcg(:,:,:)
  real(8), allocatable :: rho_nk(:,:,:)
  real(8), allocatable :: rho_ours(:,:,:)
contains

  subroutine  init_grids()
    implicit none
    call read_grid_input
    call alloc_grids
    call setup_grids
    return
  end subroutine init_grids

  subroutine  read_grid_input()
    implicit none
    open(unit=100, file='ngrid.dat', action='read')
    read(100,*) nr1, nr2, nr3
    close(100)
    return
  end subroutine read_grid_input

  subroutine  alloc_grids()
    implicit none
    allocate(pos   (3,nr1,nr2,nr3)); pos=0.d0
    allocate(r       (nr1,nr2,nr3)); r=0.d0
    allocate(r2      (nr1,nr2,nr3)); r2=0.d0
    allocate(rho     (nr1,nr2,nr3)); rho=0.d0
    allocate(pot     (nr1,nr2,nr3)); pot=0.d0
    allocate(pot_stdcg(nr1,nr2,nr3)); pot_stdcg=0.d0
    allocate(rho_nk  (nr1,nr2,nr3)); rho_nk=0.d0
    allocate(rho_ours(nr1,nr2,nr3)); rho_ours=0.d0
    return
  end subroutine alloc_grids

  subroutine  setup_grids()
    implicit none
    integer :: iq
    call compute_grid_spacing
    call compute_position_grid
    do iq = 1, n_q
      call compute_radial_grid(iq)
      call compute_density_grid(iq)
      call compute_potential_grid(iq)
    end do ! iq
    return
  end subroutine setup_grids

  subroutine  compute_grid_spacing()
    implicit none
    dr1 = sqrt(sum(h(:,1)**2))/dble(nr1)
    dr2 = sqrt(sum(h(:,2)**2))/dble(nr2)
    dr3 = sqrt(sum(h(:,3)**2))/dble(nr3)
    return
  end subroutine compute_grid_spacing

  subroutine  compute_position_grid()
    implicit none
    integer :: ig1, ig2, ig3
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          pos(1,ig1,ig2,ig3) = ig1
          pos(2,ig1,ig2,ig3) = ig2
          pos(3,ig1,ig2,ig3) = ig3
        end do ! ig1
      end do ! ig2
    end do ! ig3
    call transform_to_reduced_space
    call reduced_space_to_real_space
    return
  end subroutine compute_position_grid

  subroutine  transform_to_reduced_space()
    implicit none
    pos(1,:,:,:) = pos(1,:,:,:)/dble(nr1)
    pos(2,:,:,:) = pos(2,:,:,:)/dble(nr2)
    pos(3,:,:,:) = pos(3,:,:,:)/dble(nr3)
    return
  end subroutine transform_to_reduced_space

  subroutine  reduced_space_to_real_space()
    implicit none
    integer :: ig1, ig2, ig3
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          pos(:,ig1,ig2,ig3) = matmul(h, pos(:,ig1,ig2,ig3))
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine reduced_space_to_real_space

  subroutine  compute_radial_grid(iq)
    implicit none
    integer, intent(in) :: iq
    r2 =(pos(1,:,:,:)-q_cen(1,iq))**2 &
      &+(pos(2,:,:,:)-q_cen(2,iq))**2 &
      &+(pos(3,:,:,:)-q_cen(3,iq))**2
    r = sqrt(r2)
    return
  end subroutine compute_radial_grid

  subroutine  compute_density_grid(iq)
    implicit none
    integer, intent(in) :: iq
    real(8) :: fac1, fac3
    fac1 = q_sig(iq)*sqrt(2.d0*pi)
    fac3 = fac1*fac1*fac1
    rho = rho + q(iq)/fac3*exp(-r2/(2.d0*q_sig(iq)*q_sig(iq)))
    return
  end subroutine compute_density_grid
  
  subroutine  compute_potential_grid(iq)
    implicit none
    integer, intent(in) :: iq
    pot = pot + v_of_r(iq,r)
    return
  end subroutine compute_potential_grid

  elemental real(8) function v_of_r(iq,ri)
    implicit none
    integer, intent(in) :: iq
    real(8), intent(in) :: ri
    if (ri.gt.eps8) then
      v_of_r = q(iq)/ri*erf(ri/(sqrt(2.d0)*q_sig(iq))) !gaussian unit
    else
      v_of_r = q(iq)*sqrt(2.d0/pi)/q_sig(iq)
    end if ! r.lt.eps8
  end function v_of_r

  elemental real(8) function dvdr_of_r(iq,ri)
    implicit none
    integer, intent(in) :: iq
    real(8), intent(in) :: ri
    real(8) :: fac
    if (ri.gt.eps8) then
      fac = 1.d0/(sqrt(2.d0)*q_sig(iq))
      dvdr_of_r = q(iq)*(2.d0*fac*exp(-fac*fac*ri*ri)/(sqrt(pi)*ri) - erf(fac*ri)/(ri*ri)) !gaussian unit
    else
      dvdr_of_r = 0.d0
    end if ! r.lt.eps8
  end function dvdr_of_r

  elemental real(8) function d2vdr2_of_r(iq,ri)
    implicit none
    integer, intent(in) :: iq
    real(8), intent(in) :: ri
    real(8) :: fac
    if (ri.gt.eps8) then
      fac = 1.d0/(sqrt(2.d0)*q_sig(iq))
      !TODO: check
      d2vdr2_of_r = q(iq)*(-2.d0*fac* &
        & (2.d0*fac*fac*ri*ri+1)*exp(-fac*fac*ri*ri)/(sqrt(pi)*ri*ri) &
        & - (2.d0*fac*exp(-fac*fac*ri*ri)/(sqrt(pi)*ri*ri)- 2.d0*erf(fac*ri)/(ri*ri*ri))) !gaussian unit
    else
      d2vdr2_of_r = 0.d0
    end if ! r.lt.eps8
  end function d2vdr2_of_r

  elemental real(8) function d3vdr3_of_r(iq,ri)
    implicit none
    integer, intent(in) :: iq
    real(8), intent(in) :: ri
    real(8) :: fac
    if (ri.gt.eps8) then
      fac = 1.d0/(sqrt(2.d0)*q_sig(iq))
      !TODO: check
      d3vdr3_of_r = q(iq)*(4.d0*fac* &
        & (2.d0*(fac*ri)**4+(fac*ri)**2+1.d0)*exp(-fac*fac*ri*ri)/(sqrt(pi)*ri*ri*ri) &
        & - (-8.d0*fac*exp(-fac*fac*ri*ri)/(sqrt(pi)*ri*ri*ri) &
        &    -4.d0*fac**3*exp(-fac*fac*ri*ri)/(sqrt(pi)*ri) + 6.d0*erf(fac*ri)/(ri**4)))
    else
      d3vdr3_of_r = 0.d0
    end if ! r.lt.eps8
  end function d3vdr3_of_r

  subroutine  dealloc_grids()
    implicit none
    deallocate(pos      )
    deallocate(r        )
    deallocate(r2       )
    deallocate(rho      )
    deallocate(pot      )
    deallocate(pot_stdcg)
    deallocate(rho_nk   )
    deallocate(rho_ours )
    return
  end subroutine dealloc_grids
end module grids

module transfinite_interpolation
  implicit none
  real(8), allocatable :: pot_w_halo(:,:,:)
  real(8), allocatable :: dpot_w_halo(:,:,:,:)
  real(8), allocatable :: ddpot_w_halo(:,:,:,:)
  real(8), allocatable :: dddpot_w_halo(:,:,:,:)
  !iterators
  integer, parameter :: d_a = 1
  integer, parameter :: d_b = 2
  integer, parameter :: d_c = 3
  integer, parameter :: d_ab = 1
  integer, parameter :: d_ac = 2
  integer, parameter :: d_bc = 3
  integer, parameter :: d_abc = 1
contains

  subroutine  init_tfi()
    use grids,  only : nr1, nr2, nr3
    use laplacian_natan_kronik, only : nord2
    implicit none
    allocate(pot_w_halo(1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    allocate(dpot_w_halo(d_a:d_c,1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    allocate(ddpot_w_halo(d_ab:d_bc,1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    allocate(dddpot_w_halo(d_abc:d_abc,1-nord2:nr1+nord2, 1-nord2:nr2+nord2, 1-nord2:nr3+nord2))
    return
  end subroutine init_tfi

  subroutine  get_pot_on_boundary_and_halo()
    use grids, only : v_of_r, nr1, nr2, nr3
    use laplacian_natan_kronik, only : nord2
    use system, only : h, q_cen, n_q
    implicit none
    logical :: is_halo_1, is_halo_2, is_halo_3
    integer :: ig1, ig2, ig3
    integer :: iq
    real(8) :: r_(3), r
    pot_w_halo = 0.d0
    do ig3 = 1-nord2, nr3+nord2
      is_halo_3 = .not.(ig3.gt.1.and.ig3.lt.nr3)
      do ig2 = 1-nord2, nr2+nord2
        is_halo_2 = .not.(ig2.gt.1.and.ig2.lt.nr2)
        do ig1 = 1-nord2, nr1+nord2
          is_halo_1 = .not.(ig1.gt.1.and.ig1.lt.nr1)
          if (is_halo_1.or.is_halo_2.or.is_halo_3) then
            do iq = 1, n_q
              r_ = (/dble(ig1)/dble(nr1),dble(ig2)/dble(nr2),dble(ig3)/dble(nr3)/)
              r_ = matmul(h, r_)
              r = norm2(r_-q_cen(:,iq))
              pot_w_halo (ig1,ig2,ig3) = pot_w_halo (ig1,ig2,ig3) + v_of_r(iq,r)
            end do ! iq
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine get_pot_on_boundary_and_halo

  subroutine  get_dpot_on_boundary_and_halo()
    use grids, only : nr1, nr2, nr3
    use grids, only : dvdr_of_r, d2vdr2_of_r, d3vdr3_of_r
    use laplacian_natan_kronik, only : nord2
    use system, only : h, q_cen, n_q
    implicit none
    logical :: is_halo_1, is_halo_2, is_halo_3
    integer :: ig1, ig2, ig3
    integer :: iq
    real(8) :: r_(3), r, dvdr, d2vdr2, d3vdr3
    !print *, 'dvdr_of_r(5*sqrt(3)) = ', dvdr_of_r(1,5*sqrt(3.d0))
    ! checked with wolfram alpha on N[D[1/r*erf(r/(sqrt(2)*2)),r]/.r->5*1.7320508075689]
    !print *, 'd2vdr2_of_r(5*sqrt(3)) = ', d2vdr2_of_r(1,5*sqrt(3.d0))
    ! checked with wolfram alpha on N[D[D[1/r*erf(r/(sqrt(2)*2)),r],r]/.r->5*1.7320508075689]
    !print *, 'd3vdr3_of_r(5*sqrt(3)) = ', d3vdr3_of_r(1,5*sqrt(3.d0))
    ! checked with wolfram alpha on N[D[D[D[1/r*erf(r/(sqrt(2)*2)),r],r],r]/.r->5*1.7320508075689]
    !!
    !print *, 'dvdr_(5*sqrt(3)) = ', dvdr_of_r(1,5*sqrt(3.d0))/(5*sqrt(3.d0))*(/-5,-5,-5/) ! correct
    !!
    !print *, 'd2vdr_2(ab,5*sqrt(3)) = ', (d2vdr2_of_r(1,5*sqrt(3.d0))-dvdr_of_r(1,5*sqrt(3.d0))/(5*sqrt(3.d0)))/(3) ! correct
    !print *, 'd2vdr_2(bc,5*sqrt(3)) = ', (d2vdr2_of_r(1,5*sqrt(3.d0))-dvdr_of_r(1,5*sqrt(3.d0))/(5*sqrt(3.d0)))/(3)
    !print *, 'd2vdr_2(ac,5*sqrt(3)) = ', (d2vdr2_of_r(1,5*sqrt(3.d0))-dvdr_of_r(1,5*sqrt(3.d0))/(5*sqrt(3.d0)))/(3)
    !!
    !print *, 'd3vdr_3(abc,5*sqrt(3)) = ', (d3vdr3_of_r(1,5*sqrt(3.d0))-3.d0*d2vdr2_of_r(1,5*sqrt(3.d0))/(5*sqrt(3.d0))&
    !  &                                  +3.d0*dvdr_of_r(1,5*sqrt(3.d0))/(-75.d0))/(3.d0*sqrt(3.d0)) ! correct
    dpot_w_halo = 0.d0
    ddpot_w_halo = 0.d0
    dddpot_w_halo = 0.d0
    do ig3 = 1-nord2, nr3+nord2
      is_halo_3 = .not.(ig3.gt.1.and.ig3.lt.nr3)
      do ig2 = 1-nord2, nr2+nord2
        is_halo_2 = .not.(ig2.gt.1.and.ig2.lt.nr2)
        do ig1 = 1-nord2, nr1+nord2
          is_halo_1 = .not.(ig1.gt.1.and.ig1.lt.nr1)
          if (is_halo_1.or.is_halo_2.or.is_halo_3) then
            do iq = 1, n_q
              r_ = (/dble(ig1)/dble(nr1),dble(ig2)/dble(nr2),dble(ig3)/dble(nr3)/)
              r_ = matmul(h, r_)
              r_ = r_-q_cen(:,iq)
              r = norm2(r_)
              dvdr = dvdr_of_r(iq,r)
              d2vdr2 = d2vdr2_of_r(iq,r)
              d3vdr3 = d3vdr3_of_r(iq,r)
              if (r.gt.1.d-8) then ! dvdr -> 0 if otherwise
                dpot_w_halo (:,ig1,ig2,ig3) = dpot_w_halo (:,ig1,ig2,ig3) + dvdr*r_(:)/r
                !
                ddpot_w_halo (d_ab,ig1,ig2,ig3) = ddpot_w_halo (d_ab,ig1,ig2,ig3) &
                  & + (d2vdr2-dvdr/r)*r_(d_a)*r_(d_b)/(r*r)
                ddpot_w_halo (d_ac,ig1,ig2,ig3) = ddpot_w_halo (d_ac,ig1,ig2,ig3) &
                  & + (d2vdr2-dvdr/r)*r_(d_a)*r_(d_c)/(r*r)
                ddpot_w_halo (d_bc,ig1,ig2,ig3) = ddpot_w_halo (d_bc,ig1,ig2,ig3) &
                  & + (d2vdr2-dvdr/r)*r_(d_b)*r_(d_c)/(r*r)
                !
                dddpot_w_halo (d_abc,ig1,ig2,ig3) = dddpot_w_halo (d_abc,ig1,ig2,ig3) &
                  & + (d3vdr3-3.d0*d2vdr2/r+3.d0*dvdr/(r*r))*r_(d_a)*r_(d_b)*r_(d_c)/(r*r*r)
              end if
              ! transform to crystal coordinates (for cubic cell only)
              dpot_w_halo (:,ig1,ig2,ig3) = dpot_w_halo (:,ig1,ig2,ig3)/h(1,1)
              ddpot_w_halo (:,ig1,ig2,ig3) = ddpot_w_halo (:,ig1,ig2,ig3)/(h(1,1)*h(1,1))
              dddpot_w_halo (d_abc,ig1,ig2,ig3) = dddpot_w_halo (d_abc,ig1,ig2,ig3)/(h(1,1)**3)
            end do ! iq
          end if
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine get_dpot_on_boundary_and_halo

  subroutine  perform_linear_tfi_for_near_field_pot()
    use grids, only : pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: x,y,z
    pot_stdcg = 0.d0
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          x = (dble(ig1)/dble(nr1+1))
          y = (dble(ig2)/dble(nr2+1))
          z = (dble(ig3)/dble(nr3+1))
          pot_stdcg(ig1,ig2,ig3) = &
            &   (1.d0 - x)*pot_w_halo(0,ig2,ig3)                     & ! (1 - x)*f[0, y, z] 
            & + x*pot_w_halo(nr1+1,ig2,ig3)                          & ! + x*f[1, y, z] 
            & + (1.d0 - y)*pot_w_halo(ig1,0,ig3)                     & ! + (1 - y)*f[x, 0, z] 
            & + y*pot_w_halo(ig1, nr2+1, ig3)                        & ! + y*f[x, 1, z] 
            & + (1.d0 - z)*pot_w_halo(ig1,ig2,0)                     & ! + (1 - z)*f[x, y, 0]
            & + z*pot_w_halo(ig1,ig2,nr3+1)                          & ! + z*f[x, y, 1]
            & + (1.d0 - x)*((1.d0 - y)*((1.d0 - z)*pot_w_halo(0,0,0) & ! + (1 - x)*((1 - y)*((1 - z)*f[0, 0, 0]
            & + z*pot_w_halo(0,0,nr3+1))                             & ! + z*f[0, 0, 1])
            & + y*((1.d0 - z)*pot_w_halo(0,nr2+1,0)                  & ! + y*((1 - z)*f[0, 1, 0]
            & + z*pot_w_halo(0, nr2+1, nr3+1)))                      & ! + z*f[0, 1, 1]))
            & + x*((1.d0 - y)*((1.d0 - z)*pot_w_halo(nr1+1, 0, 0)    & ! + x*((1 - y)*((1 - z)*f[1, 0, 0]
            & + z*pot_w_halo(nr1+1, 0, nr3+1))                       & ! + z*f[1, 0, 1])
            & + y*((1.d0 - z)*pot_w_halo(nr1+1, nr2+1, 0)            & ! + y*((1 - z)*f[1, 1, 0]
            & + z*pot_w_halo(nr1+1, nr2+1, nr3+1)))                  & ! + z*f[1, 1, 1]))
            & - (1.d0 - x)*((1.d0 - y)*pot_w_halo(0, 0, ig3)         & ! - (1 - x)*((1 - y)*f[0, 0, z] 
            & + y*pot_w_halo(0, nr2+1, ig3))                         & ! + y*f[0, 1, z])
            & - x*((1.d0 - y)*pot_w_halo(nr1+1, 0, ig3)              & ! - x*((1 - y)*f[1, 0, z] 
            & + y*pot_w_halo(nr1+1, nr2+1, ig3))                     & ! + y*f[1, 1, z]) 
            & - (1.d0 - y)*((1.d0 - z)*pot_w_halo(ig1, 0, 0)         & ! - (1 - y)*((1 - z)*f[x, 0, 0] 
            & + z*pot_w_halo(ig1, 0, nr3+1))                         & ! + z*f[x, 0, 1]) 
            & - y*((1.d0 - z)*pot_w_halo(ig1, nr2+1, 0)              & ! - y*((1 - z)*f[x, 1, 0] 
            & + z*pot_w_halo(ig1, nr2+1, nr3+1))                     & ! + z*f[x, 1, 1]) 
            & - (1.d0 - z)*((1.d0 - x)*pot_w_halo(0, ig2, 0)         & ! - (1 - z)*((1 - x)*f[0, y, 0] 
            & + x*pot_w_halo(nr1+1, ig2, 0))                         & ! + x*f[1, y, 0]) 
            & - z*((1.d0 - x)*pot_w_halo(0, ig2, nr3+1)              & ! - z*((1 - x)*f[0, y, 1] 
            & + x*pot_w_halo(nr1+1, ig2, nr3+1))                       ! + x*f[1, y, 1])
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine perform_linear_tfi_for_near_field_pot

  subroutine  perform_linear_tfi_for_near_field_pot_old()
    use grids, only : pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: x,y,z
    pot_stdcg = 0.d0
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          x = (dble(ig1)/dble(nr1))
          y = (dble(ig2)/dble(nr2))
          z = (dble(ig3)/dble(nr3))
          pot_stdcg(ig1,ig2,ig3) = &
            & (1 - x)*pot_w_halo(1,ig2,ig3)                 & ! (1 - x)*f[0, y, z] 
            & + x*pot_w_halo(nr1,ig2,ig3)                   & ! + x*f[1, y, z] 
            & + (1 - y)*pot_w_halo(ig1,1,ig3)               & ! + (1 - y)*f[x, 0, z] 
            & + y*pot_w_halo(ig1, nr2, ig3)                 & ! + y*f[x, 1, z] 
            & + (1 - z)*pot_w_halo(ig1,ig2,1)               & ! + (1 - z)*f[x, y, 0]
            & + z*pot_w_halo(ig1,ig2,nr3)                   & ! + z*f[x, y, 1]
            & + (1 - x)*((1 - y)*((1 - z)*pot_w_halo(1,1,1) & ! + (1 - x)*((1 - y)*((1 - z)*f[0, 0, 0]
            & + z*pot_w_halo(1,1,nr3))                      & ! + z*f[0, 0, 1])
            & + y*((1 - z)*pot_w_halo(1,nr2,1)              & ! + y*((1 - z)*f[0, 1, 0]
            & + z*pot_w_halo(1, nr2, nr3)))                 & ! + z*f[0, 1, 1]))
            & + x*((1 - y)*((1 - z)*pot_w_halo(nr1, 1, 1)   & ! + x*((1 - y)*((1 - z)*f[1, 0, 0]
            & + z*pot_w_halo(nr1, 1, nr3))                  & ! + z*f[1, 0, 1])
            & + y*((1 - z)*pot_w_halo(nr1, nr2, 1)          & ! + y*((1 - z)*f[1, 1, 0]
            & + z*pot_w_halo(nr1, nr2, nr3)))               & ! + z*f[1, 1, 1]))
            & - (1 - x)*((1 - y)*pot_w_halo(1, 1, ig3)      & ! - (1 - x)*((1 - y)*f[0, 0, z] 
            & + y*pot_w_halo(1, nr2, ig3))                  & ! + y*f[0, 1, z])
            & - x*((1 - y)*pot_w_halo(nr1, 1, ig3)          & ! - x*((1 - y)*f[1, 0, z] 
            & + y*pot_w_halo(nr1, nr2, ig3))                & ! + y*f[1, 1, z]) 
            & - (1 - y)*((1 - z)*pot_w_halo(ig1, 1, 1)      & ! - (1 - y)*((1 - z)*f[x, 0, 0] 
            & + z*pot_w_halo(ig1, 1, nr3))                  & ! + z*f[x, 0, 1]) 
            & - y*((1 - z)*pot_w_halo(ig1, nr2, 1)          & ! - y*((1 - z)*f[x, 1, 0] 
            & + z*pot_w_halo(ig1, nr2, nr3))                & ! + z*f[x, 1, 1]) 
            & - (1 - z)*((1 - x)*pot_w_halo(1, ig2, 1)      & ! - (1 - z)*((1 - x)*f[0, y, 0] 
            & + x*pot_w_halo(nr1, ig2, 1))                  & ! + x*f[1, y, 0]) 
            & - z*((1 - x)*pot_w_halo(1, ig2, nr3)          & ! - z*((1 - x)*f[0, y, 1] 
            & + x*pot_w_halo(nr1, ig2, nr3))                  ! + x*f[1, y, 1])
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine perform_linear_tfi_for_near_field_pot_old


  real(8) function a(idr,ibd,var)
    ! hermite cubic blending function
    implicit none
    integer, intent(in) :: idr
    integer, intent(in) :: ibd
    real(8), intent(in) :: var
    real(8) :: var2, var3
    var2 = var*var
    var3 = var2*var
    if (idr.eq.0) then
      if (ibd.eq.1) then
        a = 2.d0*var3 - 3.d0*var2 + 1.d0
      else if (ibd.eq.2) then
        a = -2.d0*var3 + 3.d0*var2
      end if ! ibd.eq.1
    else if (idr.eq.1) then
      if (ibd.eq.1) then
        a = var3 - 2.d0*var2 + var
      else if (ibd.eq.2) then
        a = var3 - var2
      end if ! ibd.eq.1
    end if ! idr.eq.0
  end function a

  subroutine  perform_hermite_cubic_tfi_for_near_field_pot()
    use grids, only : pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: x,y,z
    real(8) :: u, v, w, uv, vw, uw, uvw
    pot_stdcg = 0.d0
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          x = (dble(ig1)/dble(nr1+1))
          y = (dble(ig2)/dble(nr2+1))
          z = (dble(ig3)/dble(nr3+1))
          !
          !u = a(0,1,x)*func[0,y,z]+a(0,2,x)*func[1,y,z]+a(1,1,x)*(func^(1,0,0))[0,y,z]+a(1,2,x)*(func^(1,0,0))[1,y,z]
          u =  a(0,1,x)*pot_w_halo(    0, ig2, ig3)      &
            & +a(0,2,x)*pot_w_halo(nr1+1, ig2, ig3)      &
            & +a(1,1,x)*dpot_w_halo(d_a,    0, ig2, ig3) &
            & +a(1,2,x)*dpot_w_halo(d_a,nr1+1, ig2, ig3)
          !
          !  v = a[0,1,y] func[x,0,z]+a[0,2,y] func[x,1,z]+a[1,1,y] (func^(0,1,0))[x,0,z]+a[1,2,y] (func^(0,1,0))[x,1,z]
          v =  a(0,1,y)*pot_w_halo( ig1,    0, ig3)      &
            & +a(0,2,y)*pot_w_halo( ig1,nr2+1, ig3)    &
            & +a(1,1,y)*dpot_w_halo(d_b, ig1,    0, ig3) &
            & +a(1,2,y)*dpot_w_halo(d_b, ig1,nr2+1, ig3)
          !
          !  w: a[0,1,z] func[x,y,0]+a[0,2,z] func[x,y,1]+a[1,1,z] (func^(0,0,1))[x,y,0]+a[1,2,z] (func^(0,0,1))[x,y,1]
          w =  a(0,1,z)*pot_w_halo( ig1, ig2,    0)      &
            & +a(0,2,z)*pot_w_halo( ig1, ig2,nr3+1)      &
            & +a(1,1,z)*dpot_w_halo(d_c, ig1, ig2,    0) &
            & +a(1,2,z)*dpot_w_halo(d_c, ig1, ig2,nr3+1)
          !
          !  uv: a[0,1,x] a[0,1,y] func[0,0,z]+a[0,1,x] a[0,2,y] func[0,1,z]+a[0,1,y] a[0,2,x] func[1,0,z]+a[0,2,x] a[0,2,y] func[1,1,z]+a[0,1,x] a[1,1,y] (func^(0,1,0))[0,0,z]+a[0,1,x] a[1,2,y] (func^(0,1,0))[0,1,z]+a[0,2,x] a[1,1,y] (func^(0,1,0))[1,0,z]+a[0,2,x] a[1,2,y] (func^(0,1,0))[1,1,z]+a[0,1,y] a[1,1,x] (func^(1,0,0))[0,0,z]+a[0,2,y] a[1,1,x] (func^(1,0,0))[0,1,z]+a[0,1,y] a[1,2,x] (func^(1,0,0))[1,0,z]+a[0,2,y] a[1,2,x] (func^(1,0,0))[1,1,z]+a[1,1,x] a[1,1,y] (func^(1,1,0))[0,0,z]+a[1,1,x] a[1,2,y] (func^(1,1,0))[0,1,z]+a[1,1,y] a[1,2,x] (func^(1,1,0))[1,0,z]+a[1,2,x] a[1,2,y] (func^(1,1,0))[1,1,z]
          !
          uv =  a(0,1,x)*a(0,1,y)*pot_w_halo(    0,    0, ig3)        &
            &  +a(0,1,x)*a(0,2,y)*pot_w_halo(    0,nr2+1, ig3)        &
            &  +a(0,1,y)*a(0,2,x)*pot_w_halo(nr1+1,    0, ig3)        &
            &  +a(0,2,x)*a(0,2,y)*pot_w_halo(nr1+1,nr2+1, ig3)        &
            &  +a(0,1,x)*a(1,1,y)*dpot_w_halo(d_b,    0,    0, ig3)   &
            &  +a(0,1,x)*a(1,2,y)*dpot_w_halo(d_b,    0,nr2+1, ig3)   &
            &  +a(0,2,x)*a(1,1,y)*dpot_w_halo(d_b,nr1+1,    0, ig3)   &
            &  +a(0,2,x)*a(1,2,y)*dpot_w_halo(d_b,nr1+1,nr2+1, ig3)   &
            &  +a(0,1,y)*a(1,1,x)*dpot_w_halo(d_a,    0,    0, ig3)   &
            &  +a(0,2,y)*a(1,1,x)*dpot_w_halo(d_a,    0,nr2+1, ig3)   &
            &  +a(0,1,y)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,    0, ig3)   &
            &  +a(0,2,y)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,nr2+1, ig3)   &
            &  +a(1,1,x)*a(1,1,y)*ddpot_w_halo(d_ab,    0,    0, ig3) &
            &  +a(1,1,x)*a(1,2,y)*ddpot_w_halo(d_ab,    0,nr2+1, ig3) &
            &  +a(1,1,y)*a(1,2,x)*ddpot_w_halo(d_ab,nr1+1,    0, ig3) &
            &  +a(1,2,x)*a(1,2,y)*ddpot_w_halo(d_ab,nr1+1,nr2+1, ig3)
          !
          !  vw: a[0,1,y] a[0,1,z] func[x,0,0]+a[0,1,y] a[0,2,z] func[x,0,1]+a[0,1,z] a[0,2,y] func[x,1,0]+a[0,2,y] a[0,2,z] func[x,1,1]+a[0,1,y] a[1,1,z] (func^(0,0,1))[x,0,0]+a[0,1,y] a[1,2,z] (func^(0,0,1))[x,0,1]+a[0,2,y] a[1,1,z] (func^(0,0,1))[x,1,0]+a[0,2,y] a[1,2,z] (func^(0,0,1))[x,1,1]+a[0,1,z] a[1,1,y] (func^(0,1,0))[x,0,0]+a[0,2,z] a[1,1,y] (func^(0,1,0))[x,0,1]+a[0,1,z] a[1,2,y] (func^(0,1,0))[x,1,0]+a[0,2,z] a[1,2,y] (func^(0,1,0))[x,1,1]+a[1,1,y] a[1,1,z] (func^(0,1,1))[x,0,0]+a[1,1,y] a[1,2,z] (func^(0,1,1))[x,0,1]+a[1,1,z] a[1,2,y] (func^(0,1,1))[x,1,0]+a[1,2,y] a[1,2,z] (func^(0,1,1))[x,1,1]
          vw =  a(0,1,y)*a(0,1,z)*pot_w_halo( ig1,    0,    0)        &
            &  +a(0,1,y)*a(0,2,z)*pot_w_halo( ig1,    0,nr3+1)        &
            &  +a(0,1,z)*a(0,2,y)*pot_w_halo( ig1,nr2+1,    0)        &
            &  +a(0,2,y)*a(0,2,z)*pot_w_halo( ig1,nr2+1,nr3+1)        &
            &  +a(0,1,y)*a(1,1,z)*dpot_w_halo(d_c, ig1,    0,    0)   &
            &  +a(0,1,y)*a(1,2,z)*dpot_w_halo(d_c, ig1,    0,nr3+1)   &
            &  +a(0,2,y)*a(1,1,z)*dpot_w_halo(d_c, ig1,nr2+1,    0)   &
            &  +a(0,2,y)*a(1,2,z)*dpot_w_halo(d_c, ig1,nr2+1,nr3+1)   &
            &  +a(0,1,z)*a(1,1,y)*dpot_w_halo(d_b, ig1,    0,    0)   &
            &  +a(0,2,z)*a(1,1,y)*dpot_w_halo(d_b, ig1,    0,nr3+1)   &
            &  +a(0,1,z)*a(1,2,y)*dpot_w_halo(d_b, ig1,nr2+1,    0)   &
            &  +a(0,2,z)*a(1,2,y)*dpot_w_halo(d_b, ig1,nr2+1,nr3+1)   &
            &  +a(1,1,y)*a(1,1,z)*ddpot_w_halo(d_bc, ig1,    0,    0) &
            &  +a(1,1,y)*a(1,2,z)*ddpot_w_halo(d_bc, ig1,    0,nr3+1) &
            &  +a(1,1,z)*a(1,2,y)*ddpot_w_halo(d_bc, ig1,nr2+1,    0) &
            &  +a(1,2,y)*a(1,2,z)*ddpot_w_halo(d_bc, ig1,nr2+1,nr3+1)
          !
          !  uw = a[0,1,x] a[0,1,z] func[0,y,0]+a[0,1,x] a[0,2,z] func[0,y,1]+a[0,1,z] a[0,2,x] func[1,y,0]+a[0,2,x] a[0,2,z] func[1,y,1]+a[0,1,x] a[1,1,z] (func^(0,0,1))[0,y,0]+a[0,1,x] a[1,2,z] (func^(0,0,1))[0,y,1]+a[0,2,x] a[1,1,z] (func^(0,0,1))[1,y,0]+a[0,2,x] a[1,2,z] (func^(0,0,1))[1,y,1]+a[0,1,z] a[1,1,x] (func^(1,0,0))[0,y,0]+a[0,2,z] a[1,1,x] (func^(1,0,0))[0,y,1]+a[0,1,z] a[1,2,x] (func^(1,0,0))[1,y,0]+a[0,2,z] a[1,2,x] (func^(1,0,0))[1,y,1]+a[1,1,x] a[1,1,z] (func^(1,0,1))[0,y,0]+a[1,1,x] a[1,2,z] (func^(1,0,1))[0,y,1]+a[1,1,z] a[1,2,x] (func^(1,0,1))[1,y,0]+a[1,2,x] a[1,2,z] (func^(1,0,1))[1,y,1]
          uw =  a(0,1,x)*a(0,1,z)*pot_w_halo(    0,ig2,    0)        &
            &  +a(0,1,x)*a(0,2,z)*pot_w_halo(    0,ig2,nr3+1)        &
            &  +a(0,1,z)*a(0,2,x)*pot_w_halo(nr1+1,ig2,    0)        &
            &  +a(0,2,x)*a(0,2,z)*pot_w_halo(nr1+1,ig2,nr3+1)        &
            &  +a(0,1,x)*a(1,1,z)*dpot_w_halo(d_c,    0,ig2,    0)   &
            &  +a(0,1,x)*a(1,2,z)*dpot_w_halo(d_c,    0,ig2,nr3+1)   &
            &  +a(0,2,x)*a(1,1,z)*dpot_w_halo(d_c,nr1+1,ig2,    0)   &
            &  +a(0,2,x)*a(1,2,z)*dpot_w_halo(d_c,nr1+1,ig2,nr3+1)   &
            &  +a(0,1,z)*a(1,1,x)*dpot_w_halo(d_a,    0,ig2,    0)   &
            &  +a(0,2,z)*a(1,1,x)*dpot_w_halo(d_a,    0,ig2,nr3+1)   &
            &  +a(0,1,z)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,ig2,    0)   &
            &  +a(0,2,z)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,ig2,nr3+1)   &
            &  +a(1,1,x)*a(1,1,z)*ddpot_w_halo(d_ac,    0,ig2,    0) &
            &  +a(1,1,x)*a(1,2,z)*ddpot_w_halo(d_ac,    0,ig2,nr3+1) &
            &  +a(1,1,z)*a(1,2,x)*ddpot_w_halo(d_ac,nr1+1,ig2,    0) &
            &  +a(1,2,x)*a(1,2,z)*ddpot_w_halo(d_ac,nr1+1,ig2,nr3+1)
          !
          !  uvw: a[0,1,x] a[0,1,y] a[0,1,z] func[0,0,0]+a[0,1,x] a[0,1,y] a[0,2,z] func[0,0,1]+a[0,1,x] a[0,1,z] a[0,2,y] func[0,1,0]+a[0,1,x] a[0,2,y] a[0,2,z] func[0,1,1]+a[0,1,y] a[0,1,z] a[0,2,x] func[1,0,0]+a[0,1,y] a[0,2,x] a[0,2,z] func[1,0,1]+a[0,1,z] a[0,2,x] a[0,2,y] func[1,1,0]+a[0,2,x] a[0,2,y] a[0,2,z] func[1,1,1]+a[0,1,x] a[0,1,y] a[1,1,z] (func^(0,0,1))[0,0,0]+a[0,1,x] a[0,1,y] a[1,2,z] (func^(0,0,1))[0,0,1]+a[0,1,x] a[0,2,y] a[1,1,z] (func^(0,0,1))[0,1,0]+a[0,1,x] a[0,2,y] a[1,2,z] (func^(0,0,1))[0,1,1]+a[0,1,y] a[0,2,x] a[1,1,z] (func^(0,0,1))[1,0,0]+a[0,1,y] a[0,2,x] a[1,2,z] (func^(0,0,1))[1,0,1]+a[0,2,x] a[0,2,y] a[1,1,z] (func^(0,0,1))[1,1,0]+a[0,2,x] a[0,2,y] a[1,2,z] (func^(0,0,1))[1,1,1]+a[0,1,x] a[0,1,z] a[1,1,y] (func^(0,1,0))[0,0,0]+a[0,1,x] a[0,2,z] a[1,1,y] (func^(0,1,0))[0,0,1]+a[0,1,x] a[0,1,z] a[1,2,y] (func^(0,1,0))[0,1,0]+a[0,1,x] a[0,2,z] a[1,2,y] (func^(0,1,0))[0,1,1]+a[0,1,z] a[0,2,x] a[1,1,y] (func^(0,1,0))[1,0,0]+a[0,2,x] a[0,2,z] a[1,1,y] (func^(0,1,0))[1,0,1]+a[0,1,z] a[0,2,x] a[1,2,y] (func^(0,1,0))[1,1,0]+a[0,2,x] a[0,2,z] a[1,2,y] (func^(0,1,0))[1,1,1]+a[0,1,x] a[1,1,y] a[1,1,z] (func^(0,1,1))[0,0,0]+a[0,1,x] a[1,1,y] a[1,2,z] (func^(0,1,1))[0,0,1]+a[0,1,x] a[1,1,z] a[1,2,y] (func^(0,1,1))[0,1,0]+a[0,1,x] a[1,2,y] a[1,2,z] (func^(0,1,1))[0,1,1]+a[0,2,x] a[1,1,y] a[1,1,z] (func^(0,1,1))[1,0,0]+a[0,2,x] a[1,1,y] a[1,2,z] (func^(0,1,1))[1,0,1]+a[0,2,x] a[1,1,z] a[1,2,y] (func^(0,1,1))[1,1,0]+a[0,2,x] a[1,2,y] a[1,2,z] (func^(0,1,1))[1,1,1]+a[0,1,y] a[0,1,z] a[1,1,x] (func^(1,0,0))[0,0,0]+a[0,1,y] a[0,2,z] a[1,1,x] (func^(1,0,0))[0,0,1]+a[0,1,z] a[0,2,y] a[1,1,x] (func^(1,0,0))[0,1,0]+a[0,2,y] a[0,2,z] a[1,1,x] (func^(1,0,0))[0,1,1]+a[0,1,y] a[0,1,z] a[1,2,x] (func^(1,0,0))[1,0,0]+a[0,1,y] a[0,2,z] a[1,2,x] (func^(1,0,0))[1,0,1]+a[0,1,z] a[0,2,y] a[1,2,x] (func^(1,0,0))[1,1,0]+a[0,2,y] a[0,2,z] a[1,2,x] (func^(1,0,0))[1,1,1]+a[0,1,y] a[1,1,x] a[1,1,z] (func^(1,0,1))[0,0,0]+a[0,1,y] a[1,1,x] a[1,2,z] (func^(1,0,1))[0,0,1]+a[0,2,y] a[1,1,x] a[1,1,z] (func^(1,0,1))[0,1,0]+a[0,2,y] a[1,1,x] a[1,2,z] (func^(1,0,1))[0,1,1]+a[0,1,y] a[1,1,z] a[1,2,x] (func^(1,0,1))[1,0,0]+a[0,1,y] a[1,2,x] a[1,2,z] (func^(1,0,1))[1,0,1]+a[0,2,y] a[1,1,z] a[1,2,x] (func^(1,0,1))[1,1,0]+a[0,2,y] a[1,2,x] a[1,2,z] (func^(1,0,1))[1,1,1]+a[0,1,z] a[1,1,x] a[1,1,y] (func^(1,1,0))[0,0,0]+a[0,2,z] a[1,1,x] a[1,1,y] (func^(1,1,0))[0,0,1]+a[0,1,z] a[1,1,x] a[1,2,y] (func^(1,1,0))[0,1,0]+a[0,2,z] a[1,1,x] a[1,2,y] (func^(1,1,0))[0,1,1]+a[0,1,z] a[1,1,y] a[1,2,x] (func^(1,1,0))[1,0,0]+a[0,2,z] a[1,1,y] a[1,2,x] (func^(1,1,0))[1,0,1]+a[0,1,z] a[1,2,x] a[1,2,y] (func^(1,1,0))[1,1,0]+a[0,2,z] a[1,2,x] a[1,2,y] (func^(1,1,0))[1,1,1]+a[1,1,x] a[1,1,y] a[1,1,z] (func^(1,1,1))[0,0,0]+a[1,1,x] a[1,1,y] a[1,2,z] (func^(1,1,1))[0,0,1]+a[1,1,x] a[1,1,z] a[1,2,y] (func^(1,1,1))[0,1,0]+a[1,1,x] a[1,2,y] a[1,2,z] (func^(1,1,1))[0,1,1]+a[1,1,y] a[1,1,z] a[1,2,x] (func^(1,1,1))[1,0,0]+a[1,1,y] a[1,2,x] a[1,2,z] (func^(1,1,1))[1,0,1]+a[1,1,z] a[1,2,x] a[1,2,y] (func^(1,1,1))[1,1,0]+a[1,2,x] a[1,2,y] a[1,2,z] (func^(1,1,1))[1,1,1]
          !
          uvw =  a(0,1,x)*a(0,1,y)*a(0,1,z)*pot_w_halo(    0,    0,    0)          &
          &     +a(0,1,x)*a(0,1,y)*a(0,2,z)*pot_w_halo(    0,    0,nr3+1)          &
          &     +a(0,1,x)*a(0,1,z)*a(0,2,y)*pot_w_halo(    0,nr2+1,    0)          &
          &     +a(0,1,x)*a(0,2,y)*a(0,2,z)*pot_w_halo(    0,nr2+1,nr3+1)          &
          &     +a(0,1,y)*a(0,1,z)*a(0,2,x)*pot_w_halo(nr1+1,    0,    0)          &
          &     +a(0,1,y)*a(0,2,x)*a(0,2,z)*pot_w_halo(nr1+1,    0,nr3+1)          &
          &     +a(0,1,z)*a(0,2,x)*a(0,2,y)*pot_w_halo(nr1+1,nr2+1,    0)          &
          &     +a(0,2,x)*a(0,2,y)*a(0,2,z)*pot_w_halo(nr1+1,nr2+1,nr3+1)          &
          !
          &     +a(0,1,x)*a(0,1,y)*a(1,1,z)*dpot_w_halo(d_c,    0,    0,    0)     &
          &     +a(0,1,x)*a(0,1,y)*a(1,2,z)*dpot_w_halo(d_c,    0,    0,nr3+1)     &
          &     +a(0,1,x)*a(0,2,y)*a(1,1,z)*dpot_w_halo(d_c,    0,nr2+1,    0)     &
          &     +a(0,1,x)*a(0,2,y)*a(1,2,z)*dpot_w_halo(d_c,    0,nr2+1,nr3+1)     &
          &     +a(0,1,y)*a(0,2,x)*a(1,1,z)*dpot_w_halo(d_c,nr1+1,    0,    0)     &
          &     +a(0,1,y)*a(0,2,x)*a(1,2,z)*dpot_w_halo(d_c,nr1+1,    0,nr3+1)     &
          &     +a(0,2,x)*a(0,2,y)*a(1,1,z)*dpot_w_halo(d_c,nr1+1,nr2+1,    0)     &
          &     +a(0,2,x)*a(0,2,y)*a(1,2,z)*dpot_w_halo(d_c,nr1+1,nr2+1,nr3+1)     &
          &     +a(0,1,x)*a(0,1,z)*a(1,1,y)*dpot_w_halo(d_b,    0,    0,    0)     &
          &     +a(0,1,x)*a(0,2,z)*a(1,1,y)*dpot_w_halo(d_b,    0,    0,nr3+1)     &
          &     +a(0,1,x)*a(0,1,z)*a(1,2,y)*dpot_w_halo(d_b,    0,nr2+1,    0)     &
          &     +a(0,1,x)*a(0,2,z)*a(1,2,y)*dpot_w_halo(d_b,    0,nr2+1,nr3+1)     &
          &     +a(0,1,z)*a(0,2,x)*a(1,1,y)*dpot_w_halo(d_b,nr1+1,    0,    0)     &
          &     +a(0,2,x)*a(0,2,z)*a(1,1,y)*dpot_w_halo(d_b,nr1+1,    0,nr3+1)     &
          &     +a(0,1,z)*a(0,2,x)*a(1,2,y)*dpot_w_halo(d_b,nr1+1,nr2+1,    0)     &
          &     +a(0,2,x)*a(0,2,z)*a(1,2,y)*dpot_w_halo(d_b,nr1+1,nr2+1,nr3+1)     &
          &     +a(0,1,y)*a(0,1,z)*a(1,1,x)*dpot_w_halo(d_a,    0,    0,    0)     &
          &     +a(0,1,y)*a(0,2,z)*a(1,1,x)*dpot_w_halo(d_a,    0,    0,nr3+1)     &
          &     +a(0,1,z)*a(0,2,y)*a(1,1,x)*dpot_w_halo(d_a,    0,nr2+1,    0)     &
          &     +a(0,2,y)*a(0,2,z)*a(1,1,x)*dpot_w_halo(d_a,    0,nr2+1,nr3+1)     &
          &     +a(0,1,y)*a(0,1,z)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,    0,    0)     &
          &     +a(0,1,y)*a(0,2,z)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,    0,nr3+1)     &
          &     +a(0,1,z)*a(0,2,y)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,nr2+1,    0)     &
          &     +a(0,2,y)*a(0,2,z)*a(1,2,x)*dpot_w_halo(d_a,nr1+1,nr2+1,nr3+1)     &
          !
          &     +a(0,1,x)*a(1,1,y)*a(1,1,z)*ddpot_w_halo(d_bc,    0,    0,    0)   &
          &     +a(0,1,x)*a(1,1,y)*a(1,2,z)*ddpot_w_halo(d_bc,    0,    0,nr3+1)   &
          &     +a(0,1,x)*a(1,1,z)*a(1,2,y)*ddpot_w_halo(d_bc,    0,nr2+1,    0)   &
          &     +a(0,1,x)*a(1,2,y)*a(1,2,z)*ddpot_w_halo(d_bc,    0,nr2+1,nr3+1)   &
          &     +a(0,2,x)*a(1,1,y)*a(1,1,z)*ddpot_w_halo(d_bc,nr1+1,    0,    0)   &
          &     +a(0,2,x)*a(1,1,y)*a(1,2,z)*ddpot_w_halo(d_bc,nr1+1,    0,nr3+1)   &
          &     +a(0,2,x)*a(1,1,z)*a(1,2,y)*ddpot_w_halo(d_bc,nr1+1,nr2+1,    0)   &
          &     +a(0,2,x)*a(1,2,y)*a(1,2,z)*ddpot_w_halo(d_bc,nr1+1,nr2+1,nr3+1)   &
          &     +a(0,1,y)*a(1,1,x)*a(1,1,z)*ddpot_w_halo(d_ac,    0,    0,    0)   &
          &     +a(0,1,y)*a(1,1,x)*a(1,2,z)*ddpot_w_halo(d_ac,    0,    0,nr3+1)   &
          &     +a(0,2,y)*a(1,1,x)*a(1,1,z)*ddpot_w_halo(d_ac,    0,nr2+1,    0)   &
          &     +a(0,2,y)*a(1,1,x)*a(1,2,z)*ddpot_w_halo(d_ac,    0,nr2+1,nr3+1)   &
          &     +a(0,1,y)*a(1,1,z)*a(1,2,x)*ddpot_w_halo(d_ac,nr1+1,    0,    0)   &
          &     +a(0,1,y)*a(1,2,x)*a(1,2,z)*ddpot_w_halo(d_ac,nr1+1,    0,nr3+1)   &
          &     +a(0,2,y)*a(1,1,z)*a(1,2,x)*ddpot_w_halo(d_ac,nr1+1,nr2+1,    0)   &
          &     +a(0,2,y)*a(1,2,x)*a(1,2,z)*ddpot_w_halo(d_ac,nr1+1,nr2+1,nr3+1)   &
          &     +a(0,1,z)*a(1,1,x)*a(1,1,y)*ddpot_w_halo(d_ab,    0,    0,    0)   &
          &     +a(0,2,z)*a(1,1,x)*a(1,1,y)*ddpot_w_halo(d_ab,    0,    0,nr3+1)   &
          &     +a(0,1,z)*a(1,1,x)*a(1,2,y)*ddpot_w_halo(d_ab,    0,nr2+1,    0)   &
          &     +a(0,2,z)*a(1,1,x)*a(1,2,y)*ddpot_w_halo(d_ab,    0,nr2+1,nr3+1)   &
          &     +a(0,1,z)*a(1,1,y)*a(1,2,x)*ddpot_w_halo(d_ab,nr1+1,    0,    0)   &
          &     +a(0,2,z)*a(1,1,y)*a(1,2,x)*ddpot_w_halo(d_ab,nr1+1,    0,nr3+1)   &
          &     +a(0,1,z)*a(1,2,x)*a(1,2,y)*ddpot_w_halo(d_ab,nr1+1,nr2+1,    0)   &
          &     +a(0,2,z)*a(1,2,x)*a(1,2,y)*ddpot_w_halo(d_ab,nr1+1,nr2+1,nr3+1)   &
          !
          &     +a(1,1,x)*a(1,1,y)*a(1,1,z)*dddpot_w_halo(d_abc,    0,    0,    0) &
          &     +a(1,1,x)*a(1,1,y)*a(1,2,z)*dddpot_w_halo(d_abc,    0,    0,nr3+1) &
          &     +a(1,1,x)*a(1,1,z)*a(1,2,y)*dddpot_w_halo(d_abc,    0,nr2+1,    0) &
          &     +a(1,1,x)*a(1,2,y)*a(1,2,z)*dddpot_w_halo(d_abc,    0,nr2+1,nr3+1) &
          &     +a(1,1,y)*a(1,1,z)*a(1,2,x)*dddpot_w_halo(d_abc,nr1+1,    0,    0) &
          &     +a(1,1,y)*a(1,2,x)*a(1,2,z)*dddpot_w_halo(d_abc,nr1+1,    0,nr3+1) &
          &     +a(1,1,z)*a(1,2,x)*a(1,2,y)*dddpot_w_halo(d_abc,nr1+1,nr2+1,    0) &
          &     +a(1,2,x)*a(1,2,y)*a(1,2,z)*dddpot_w_halo(d_abc,nr1+1,nr2+1,nr3+1)
          !
          pot_stdcg(ig1,ig2,ig3) = u+v+w-uv-vw-uw+uvw
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine perform_hermite_cubic_tfi_for_near_field_pot

  subroutine  perform_hermite_cubic_tfi_for_near_field_pot_old()
    use grids, only : pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: ig1, ig2, ig3
    real(8) :: x,y,z
    real(8) :: u, v, w, uv, vw, uw, uvw
    pot_stdcg = 0.d0
    do ig3 = 1, nr3
      do ig2 = 1, nr2
        do ig1 = 1, nr1
          x = (dble(ig1)/dble(nr1))
          y = (dble(ig2)/dble(nr2))
          z = (dble(ig3)/dble(nr3))
          !
          !u = a(0,1,x)*func[0,y,z]+a(0,2,x)*func[1,y,z]+a(1,1,x)*(func^(1,0,0))[0,y,z]+a(1,2,x)*(func^(1,0,0))[1,y,z]
          u =  a(0,1,x)*pot_w_halo(1,ig2,ig3)      &
            & +a(0,2,x)*pot_w_halo(nr1,ig2,ig3)    &
            & +a(1,1,x)*dpot_w_halo(d_a,1,ig2,ig3) &
            & +a(1,2,x)*dpot_w_halo(d_a,nr1,ig2,ig3)
          !
          !  v = a[0,1,y] func[x,0,z]+a[0,2,y] func[x,1,z]+a[1,1,y] (func^(0,1,0))[x,0,z]+a[1,2,y] (func^(0,1,0))[x,1,z]
          v =  a(0,1,y)*pot_w_halo(ig1,1,ig3)      &
            & +a(0,2,y)*pot_w_halo(ig1,nr2,ig3)    &
            & +a(1,1,y)*dpot_w_halo(d_b,ig1,1,ig3) &
            & +a(1,2,y)*dpot_w_halo(d_b,ig1,nr2,ig3)
          !
          !  w: a[0,1,z] func[x,y,0]+a[0,2,z] func[x,y,1]+a[1,1,z] (func^(0,0,1))[x,y,0]+a[1,2,z] (func^(0,0,1))[x,y,1]
          w =  a(0,1,z)*pot_w_halo(ig1,ig2,1)      &
            & +a(0,2,z)*pot_w_halo(ig1,ig2,nr3)    &
            & +a(1,1,z)*dpot_w_halo(d_c,ig1,ig2,1) &
            & +a(1,2,z)*dpot_w_halo(d_c,ig1,ig2,nr3)
          !
          !  uv: a[0,1,x] a[0,1,y] func[0,0,z]+a[0,1,x] a[0,2,y] func[0,1,z]+a[0,1,y] a[0,2,x] func[1,0,z]+a[0,2,x] a[0,2,y] func[1,1,z]+a[0,1,x] a[1,1,y] (func^(0,1,0))[0,0,z]+a[0,1,x] a[1,2,y] (func^(0,1,0))[0,1,z]+a[0,2,x] a[1,1,y] (func^(0,1,0))[1,0,z]+a[0,2,x] a[1,2,y] (func^(0,1,0))[1,1,z]+a[0,1,y] a[1,1,x] (func^(1,0,0))[0,0,z]+a[0,2,y] a[1,1,x] (func^(1,0,0))[0,1,z]+a[0,1,y] a[1,2,x] (func^(1,0,0))[1,0,z]+a[0,2,y] a[1,2,x] (func^(1,0,0))[1,1,z]+a[1,1,x] a[1,1,y] (func^(1,1,0))[0,0,z]+a[1,1,x] a[1,2,y] (func^(1,1,0))[0,1,z]+a[1,1,y] a[1,2,x] (func^(1,1,0))[1,0,z]+a[1,2,x] a[1,2,y] (func^(1,1,0))[1,1,z]
          !
          uv =  a(0,1,x)*a(0,1,y)*pot_w_halo(1,1,ig3)            &
            &  +a(0,1,x)*a(0,2,y)*pot_w_halo(1,nr2,ig3)          &
            &  +a(0,1,y)*a(0,2,x)*pot_w_halo(nr1,1,ig3)          &
            &  +a(0,2,x)*a(0,2,y)*pot_w_halo(nr1,nr2,ig3)        &
            &  +a(0,1,x)*a(1,1,y)*dpot_w_halo(d_b,1,1,ig3)       &
            &  +a(0,1,x)*a(1,2,y)*dpot_w_halo(d_b,1,nr2,ig3)     &
            &  +a(0,2,x)*a(1,1,y)*dpot_w_halo(d_b,nr1,1,ig3)     &
            &  +a(0,2,x)*a(1,2,y)*dpot_w_halo(d_b,nr1,nr2,ig3)   &
            &  +a(0,1,y)*a(1,1,x)*dpot_w_halo(d_a,1,1,ig3)       &
            &  +a(0,2,y)*a(1,1,x)*dpot_w_halo(d_a,1,nr2,ig3)     &
            &  +a(0,1,y)*a(1,2,x)*dpot_w_halo(d_a,nr1,1,ig3)     &
            &  +a(0,2,y)*a(1,2,x)*dpot_w_halo(d_a,nr1,nr2,ig3)   &
            &  +a(1,1,x)*a(1,1,y)*ddpot_w_halo(d_ab,1,1,ig3)     &
            &  +a(1,1,x)*a(1,2,y)*ddpot_w_halo(d_ab,1,nr2,ig3)   &
            &  +a(1,1,y)*a(1,2,x)*ddpot_w_halo(d_ab,nr1,1,ig3)   &
            &  +a(1,2,x)*a(1,2,y)*ddpot_w_halo(d_ab,nr1,nr2,ig3)
          !
          !  vw: a[0,1,y] a[0,1,z] func[x,0,0]+a[0,1,y] a[0,2,z] func[x,0,1]+a[0,1,z] a[0,2,y] func[x,1,0]+a[0,2,y] a[0,2,z] func[x,1,1]+a[0,1,y] a[1,1,z] (func^(0,0,1))[x,0,0]+a[0,1,y] a[1,2,z] (func^(0,0,1))[x,0,1]+a[0,2,y] a[1,1,z] (func^(0,0,1))[x,1,0]+a[0,2,y] a[1,2,z] (func^(0,0,1))[x,1,1]+a[0,1,z] a[1,1,y] (func^(0,1,0))[x,0,0]+a[0,2,z] a[1,1,y] (func^(0,1,0))[x,0,1]+a[0,1,z] a[1,2,y] (func^(0,1,0))[x,1,0]+a[0,2,z] a[1,2,y] (func^(0,1,0))[x,1,1]+a[1,1,y] a[1,1,z] (func^(0,1,1))[x,0,0]+a[1,1,y] a[1,2,z] (func^(0,1,1))[x,0,1]+a[1,1,z] a[1,2,y] (func^(0,1,1))[x,1,0]+a[1,2,y] a[1,2,z] (func^(0,1,1))[x,1,1]
          vw =  a(0,1,y)*a(0,1,z)*pot_w_halo(ig1,  1,  1)        &
            &  +a(0,1,y)*a(0,2,z)*pot_w_halo(ig1,  1,nr3)        &
            &  +a(0,1,z)*a(0,2,y)*pot_w_halo(ig1,nr2,  1)        &
            &  +a(0,2,y)*a(0,2,z)*pot_w_halo(ig1,nr2,nr3)        &
            &  +a(0,1,y)*a(1,1,z)*dpot_w_halo(d_c,ig1,  1,  1)   &
            &  +a(0,1,y)*a(1,2,z)*dpot_w_halo(d_c,ig1,  1,nr3)   &
            &  +a(0,2,y)*a(1,1,z)*dpot_w_halo(d_c,ig1,nr2,  1)   &
            &  +a(0,2,y)*a(1,2,z)*dpot_w_halo(d_c,ig1,nr2,nr3)   &
            &  +a(0,1,z)*a(1,1,y)*dpot_w_halo(d_b,ig1,  1,  1)   &
            &  +a(0,2,z)*a(1,1,y)*dpot_w_halo(d_b,ig1,  1,nr3)   &
            &  +a(0,1,z)*a(1,2,y)*dpot_w_halo(d_b,ig1,nr2,  1)   &
            &  +a(0,2,z)*a(1,2,y)*dpot_w_halo(d_b,ig1,nr2,nr3)   &
            &  +a(1,1,y)*a(1,1,z)*ddpot_w_halo(d_bc,ig1,  1,  1) &
            &  +a(1,1,y)*a(1,2,z)*ddpot_w_halo(d_bc,ig1,  1,nr3) &
            &  +a(1,1,z)*a(1,2,y)*ddpot_w_halo(d_bc,ig1,nr2,  1) &
            &  +a(1,2,y)*a(1,2,z)*ddpot_w_halo(d_bc,ig1,nr2,nr3)
          !
          !  uw = a[0,1,x] a[0,1,z] func[0,y,0]+a[0,1,x] a[0,2,z] func[0,y,1]+a[0,1,z] a[0,2,x] func[1,y,0]+a[0,2,x] a[0,2,z] func[1,y,1]+a[0,1,x] a[1,1,z] (func^(0,0,1))[0,y,0]+a[0,1,x] a[1,2,z] (func^(0,0,1))[0,y,1]+a[0,2,x] a[1,1,z] (func^(0,0,1))[1,y,0]+a[0,2,x] a[1,2,z] (func^(0,0,1))[1,y,1]+a[0,1,z] a[1,1,x] (func^(1,0,0))[0,y,0]+a[0,2,z] a[1,1,x] (func^(1,0,0))[0,y,1]+a[0,1,z] a[1,2,x] (func^(1,0,0))[1,y,0]+a[0,2,z] a[1,2,x] (func^(1,0,0))[1,y,1]+a[1,1,x] a[1,1,z] (func^(1,0,1))[0,y,0]+a[1,1,x] a[1,2,z] (func^(1,0,1))[0,y,1]+a[1,1,z] a[1,2,x] (func^(1,0,1))[1,y,0]+a[1,2,x] a[1,2,z] (func^(1,0,1))[1,y,1]
          uw =  a(0,1,x)*a(0,1,z)*pot_w_halo(  1,ig2,  1)        &
            &  +a(0,1,x)*a(0,2,z)*pot_w_halo(  1,ig2,nr3)        &
            &  +a(0,1,z)*a(0,2,x)*pot_w_halo(nr1,ig2,  1)        &
            &  +a(0,2,x)*a(0,2,z)*pot_w_halo(nr1,ig2,nr3)        &
            &  +a(0,1,x)*a(1,1,z)*dpot_w_halo(d_c,  1,ig2,  1)   &
            &  +a(0,1,x)*a(1,2,z)*dpot_w_halo(d_c,  1,ig2,nr3)   &
            &  +a(0,2,x)*a(1,1,z)*dpot_w_halo(d_c,nr1,ig2,  1)   &
            &  +a(0,2,x)*a(1,2,z)*dpot_w_halo(d_c,nr1,ig2,nr3)   &
            &  +a(0,1,z)*a(1,1,x)*dpot_w_halo(d_a,  1,ig2,  1)   &
            &  +a(0,2,z)*a(1,1,x)*dpot_w_halo(d_a,  1,ig2,nr3)   &
            &  +a(0,1,z)*a(1,2,x)*dpot_w_halo(d_a,nr1,ig2,  1)   &
            &  +a(0,2,z)*a(1,2,x)*dpot_w_halo(d_a,nr1,ig2,nr3)   &
            &  +a(1,1,x)*a(1,1,z)*ddpot_w_halo(d_ac,  1,ig2,  1) &
            &  +a(1,1,x)*a(1,2,z)*ddpot_w_halo(d_ac,  1,ig2,nr3) &
            &  +a(1,1,z)*a(1,2,x)*ddpot_w_halo(d_ac,nr1,ig2,  1) &
            &  +a(1,2,x)*a(1,2,z)*ddpot_w_halo(d_ac,nr1,ig2,nr3)
          !
          !  uvw: a[0,1,x] a[0,1,y] a[0,1,z] func[0,0,0]+a[0,1,x] a[0,1,y] a[0,2,z] func[0,0,1]+a[0,1,x] a[0,1,z] a[0,2,y] func[0,1,0]+a[0,1,x] a[0,2,y] a[0,2,z] func[0,1,1]+a[0,1,y] a[0,1,z] a[0,2,x] func[1,0,0]+a[0,1,y] a[0,2,x] a[0,2,z] func[1,0,1]+a[0,1,z] a[0,2,x] a[0,2,y] func[1,1,0]+a[0,2,x] a[0,2,y] a[0,2,z] func[1,1,1]+a[0,1,x] a[0,1,y] a[1,1,z] (func^(0,0,1))[0,0,0]+a[0,1,x] a[0,1,y] a[1,2,z] (func^(0,0,1))[0,0,1]+a[0,1,x] a[0,2,y] a[1,1,z] (func^(0,0,1))[0,1,0]+a[0,1,x] a[0,2,y] a[1,2,z] (func^(0,0,1))[0,1,1]+a[0,1,y] a[0,2,x] a[1,1,z] (func^(0,0,1))[1,0,0]+a[0,1,y] a[0,2,x] a[1,2,z] (func^(0,0,1))[1,0,1]+a[0,2,x] a[0,2,y] a[1,1,z] (func^(0,0,1))[1,1,0]+a[0,2,x] a[0,2,y] a[1,2,z] (func^(0,0,1))[1,1,1]+a[0,1,x] a[0,1,z] a[1,1,y] (func^(0,1,0))[0,0,0]+a[0,1,x] a[0,2,z] a[1,1,y] (func^(0,1,0))[0,0,1]+a[0,1,x] a[0,1,z] a[1,2,y] (func^(0,1,0))[0,1,0]+a[0,1,x] a[0,2,z] a[1,2,y] (func^(0,1,0))[0,1,1]+a[0,1,z] a[0,2,x] a[1,1,y] (func^(0,1,0))[1,0,0]+a[0,2,x] a[0,2,z] a[1,1,y] (func^(0,1,0))[1,0,1]+a[0,1,z] a[0,2,x] a[1,2,y] (func^(0,1,0))[1,1,0]+a[0,2,x] a[0,2,z] a[1,2,y] (func^(0,1,0))[1,1,1]+a[0,1,x] a[1,1,y] a[1,1,z] (func^(0,1,1))[0,0,0]+a[0,1,x] a[1,1,y] a[1,2,z] (func^(0,1,1))[0,0,1]+a[0,1,x] a[1,1,z] a[1,2,y] (func^(0,1,1))[0,1,0]+a[0,1,x] a[1,2,y] a[1,2,z] (func^(0,1,1))[0,1,1]+a[0,2,x] a[1,1,y] a[1,1,z] (func^(0,1,1))[1,0,0]+a[0,2,x] a[1,1,y] a[1,2,z] (func^(0,1,1))[1,0,1]+a[0,2,x] a[1,1,z] a[1,2,y] (func^(0,1,1))[1,1,0]+a[0,2,x] a[1,2,y] a[1,2,z] (func^(0,1,1))[1,1,1]+a[0,1,y] a[0,1,z] a[1,1,x] (func^(1,0,0))[0,0,0]+a[0,1,y] a[0,2,z] a[1,1,x] (func^(1,0,0))[0,0,1]+a[0,1,z] a[0,2,y] a[1,1,x] (func^(1,0,0))[0,1,0]+a[0,2,y] a[0,2,z] a[1,1,x] (func^(1,0,0))[0,1,1]+a[0,1,y] a[0,1,z] a[1,2,x] (func^(1,0,0))[1,0,0]+a[0,1,y] a[0,2,z] a[1,2,x] (func^(1,0,0))[1,0,1]+a[0,1,z] a[0,2,y] a[1,2,x] (func^(1,0,0))[1,1,0]+a[0,2,y] a[0,2,z] a[1,2,x] (func^(1,0,0))[1,1,1]+a[0,1,y] a[1,1,x] a[1,1,z] (func^(1,0,1))[0,0,0]+a[0,1,y] a[1,1,x] a[1,2,z] (func^(1,0,1))[0,0,1]+a[0,2,y] a[1,1,x] a[1,1,z] (func^(1,0,1))[0,1,0]+a[0,2,y] a[1,1,x] a[1,2,z] (func^(1,0,1))[0,1,1]+a[0,1,y] a[1,1,z] a[1,2,x] (func^(1,0,1))[1,0,0]+a[0,1,y] a[1,2,x] a[1,2,z] (func^(1,0,1))[1,0,1]+a[0,2,y] a[1,1,z] a[1,2,x] (func^(1,0,1))[1,1,0]+a[0,2,y] a[1,2,x] a[1,2,z] (func^(1,0,1))[1,1,1]+a[0,1,z] a[1,1,x] a[1,1,y] (func^(1,1,0))[0,0,0]+a[0,2,z] a[1,1,x] a[1,1,y] (func^(1,1,0))[0,0,1]+a[0,1,z] a[1,1,x] a[1,2,y] (func^(1,1,0))[0,1,0]+a[0,2,z] a[1,1,x] a[1,2,y] (func^(1,1,0))[0,1,1]+a[0,1,z] a[1,1,y] a[1,2,x] (func^(1,1,0))[1,0,0]+a[0,2,z] a[1,1,y] a[1,2,x] (func^(1,1,0))[1,0,1]+a[0,1,z] a[1,2,x] a[1,2,y] (func^(1,1,0))[1,1,0]+a[0,2,z] a[1,2,x] a[1,2,y] (func^(1,1,0))[1,1,1]+a[1,1,x] a[1,1,y] a[1,1,z] (func^(1,1,1))[0,0,0]+a[1,1,x] a[1,1,y] a[1,2,z] (func^(1,1,1))[0,0,1]+a[1,1,x] a[1,1,z] a[1,2,y] (func^(1,1,1))[0,1,0]+a[1,1,x] a[1,2,y] a[1,2,z] (func^(1,1,1))[0,1,1]+a[1,1,y] a[1,1,z] a[1,2,x] (func^(1,1,1))[1,0,0]+a[1,1,y] a[1,2,x] a[1,2,z] (func^(1,1,1))[1,0,1]+a[1,1,z] a[1,2,x] a[1,2,y] (func^(1,1,1))[1,1,0]+a[1,2,x] a[1,2,y] a[1,2,z] (func^(1,1,1))[1,1,1]
          !
          uvw =  a(0,1,x)*a(0,1,y)*a(0,1,z)*pot_w_halo(  1,  1,  1)          &
          &     +a(0,1,x)*a(0,1,y)*a(0,2,z)*pot_w_halo(  1,  1,nr3)          &
          &     +a(0,1,x)*a(0,1,z)*a(0,2,y)*pot_w_halo(  1,nr2,  1)          &
          &     +a(0,1,x)*a(0,2,y)*a(0,2,z)*pot_w_halo(  1,nr2,nr3)          &
          &     +a(0,1,y)*a(0,1,z)*a(0,2,x)*pot_w_halo(nr1,  1,  1)          &
          &     +a(0,1,y)*a(0,2,x)*a(0,2,z)*pot_w_halo(nr1,  1,nr3)          &
          &     +a(0,1,z)*a(0,2,x)*a(0,2,y)*pot_w_halo(nr1,nr2,  1)          &
          &     +a(0,2,x)*a(0,2,y)*a(0,2,z)*pot_w_halo(nr1,nr2,nr3)          &
          !
          &     +a(0,1,x)*a(0,1,y)*a(1,1,z)*dpot_w_halo(d_c,  1,  1,  1)     &
          &     +a(0,1,x)*a(0,1,y)*a(1,2,z)*dpot_w_halo(d_c,  1,  1,nr3)     &
          &     +a(0,1,x)*a(0,2,y)*a(1,1,z)*dpot_w_halo(d_c,  1,nr2,  1)     &
          &     +a(0,1,x)*a(0,2,y)*a(1,2,z)*dpot_w_halo(d_c,  1,nr2,nr3)     &
          &     +a(0,1,y)*a(0,2,x)*a(1,1,z)*dpot_w_halo(d_c,nr1,  1,  1)     &
          &     +a(0,1,y)*a(0,2,x)*a(1,2,z)*dpot_w_halo(d_c,nr1,  1,nr3)     &
          &     +a(0,2,x)*a(0,2,y)*a(1,1,z)*dpot_w_halo(d_c,nr1,nr2,  1)     &
          &     +a(0,2,x)*a(0,2,y)*a(1,2,z)*dpot_w_halo(d_c,nr1,nr2,nr3)     &
          &     +a(0,1,x)*a(0,1,z)*a(1,1,y)*dpot_w_halo(d_b,  1,  1,  1)     &
          &     +a(0,1,x)*a(0,2,z)*a(1,1,y)*dpot_w_halo(d_b,  1,  1,nr3)     &
          &     +a(0,1,x)*a(0,1,z)*a(1,2,y)*dpot_w_halo(d_b,  1,nr2,  1)     &
          &     +a(0,1,x)*a(0,2,z)*a(1,2,y)*dpot_w_halo(d_b,  1,nr2,nr3)     &
          &     +a(0,1,z)*a(0,2,x)*a(1,1,y)*dpot_w_halo(d_b,nr1,  1,  1)     &
          &     +a(0,2,x)*a(0,2,z)*a(1,1,y)*dpot_w_halo(d_b,nr1,  1,nr3)     &
          &     +a(0,1,z)*a(0,2,x)*a(1,2,y)*dpot_w_halo(d_b,nr1,nr2,  1)     &
          &     +a(0,2,x)*a(0,2,z)*a(1,2,y)*dpot_w_halo(d_b,nr1,nr2,nr3)     &
          &     +a(0,1,y)*a(0,1,z)*a(1,1,x)*dpot_w_halo(d_a,  1,  1,  1)     &
          &     +a(0,1,y)*a(0,2,z)*a(1,1,x)*dpot_w_halo(d_a,  1,  1,nr3)     &
          &     +a(0,1,z)*a(0,2,y)*a(1,1,x)*dpot_w_halo(d_a,  1,nr2,  1)     &
          &     +a(0,2,y)*a(0,2,z)*a(1,1,x)*dpot_w_halo(d_a,  1,nr2,nr3)     &
          &     +a(0,1,y)*a(0,1,z)*a(1,2,x)*dpot_w_halo(d_a,nr1,  1,  1)     &
          &     +a(0,1,y)*a(0,2,z)*a(1,2,x)*dpot_w_halo(d_a,nr1,  1,nr3)     &
          &     +a(0,1,z)*a(0,2,y)*a(1,2,x)*dpot_w_halo(d_a,nr1,nr2,  1)     &
          &     +a(0,2,y)*a(0,2,z)*a(1,2,x)*dpot_w_halo(d_a,nr1,nr2,nr3)     &
          !
          &     +a(0,1,x)*a(1,1,y)*a(1,1,z)*ddpot_w_halo(d_bc,  1,  1,  1)   &
          &     +a(0,1,x)*a(1,1,y)*a(1,2,z)*ddpot_w_halo(d_bc,  1,  1,nr3)   &
          &     +a(0,1,x)*a(1,1,z)*a(1,2,y)*ddpot_w_halo(d_bc,  1,nr2,  1)   &
          &     +a(0,1,x)*a(1,2,y)*a(1,2,z)*ddpot_w_halo(d_bc,  1,nr2,nr3)   &
          &     +a(0,2,x)*a(1,1,y)*a(1,1,z)*ddpot_w_halo(d_bc,nr1,  1,  1)   &
          &     +a(0,2,x)*a(1,1,y)*a(1,2,z)*ddpot_w_halo(d_bc,nr1,  1,nr3)   &
          &     +a(0,2,x)*a(1,1,z)*a(1,2,y)*ddpot_w_halo(d_bc,nr1,nr2,  1)   &
          &     +a(0,2,x)*a(1,2,y)*a(1,2,z)*ddpot_w_halo(d_bc,nr1,nr2,nr3)   &
          &     +a(0,1,y)*a(1,1,x)*a(1,1,z)*ddpot_w_halo(d_ac,  1,  1,  1)   &
          &     +a(0,1,y)*a(1,1,x)*a(1,2,z)*ddpot_w_halo(d_ac,  1,  1,nr3)   &
          &     +a(0,2,y)*a(1,1,x)*a(1,1,z)*ddpot_w_halo(d_ac,  1,nr2,  1)   &
          &     +a(0,2,y)*a(1,1,x)*a(1,2,z)*ddpot_w_halo(d_ac,  1,nr2,nr3)   &
          &     +a(0,1,y)*a(1,1,z)*a(1,2,x)*ddpot_w_halo(d_ac,nr1,  1,  1)   &
          &     +a(0,1,y)*a(1,2,x)*a(1,2,z)*ddpot_w_halo(d_ac,nr1,  1,nr3)   &
          &     +a(0,2,y)*a(1,1,z)*a(1,2,x)*ddpot_w_halo(d_ac,nr1,nr2,  1)   &
          &     +a(0,2,y)*a(1,2,x)*a(1,2,z)*ddpot_w_halo(d_ac,nr1,nr2,nr3)   &
          &     +a(0,1,z)*a(1,1,x)*a(1,1,y)*ddpot_w_halo(d_ab,  1,  1,  1)   &
          &     +a(0,2,z)*a(1,1,x)*a(1,1,y)*ddpot_w_halo(d_ab,  1,  1,nr3)   &
          &     +a(0,1,z)*a(1,1,x)*a(1,2,y)*ddpot_w_halo(d_ab,  1,nr2,  1)   &
          &     +a(0,2,z)*a(1,1,x)*a(1,2,y)*ddpot_w_halo(d_ab,  1,nr2,nr3)   &
          &     +a(0,1,z)*a(1,1,y)*a(1,2,x)*ddpot_w_halo(d_ab,nr1,  1,  1)   &
          &     +a(0,2,z)*a(1,1,y)*a(1,2,x)*ddpot_w_halo(d_ab,nr1,  1,nr3)   &
          &     +a(0,1,z)*a(1,2,x)*a(1,2,y)*ddpot_w_halo(d_ab,nr1,nr2,  1)   &
          &     +a(0,2,z)*a(1,2,x)*a(1,2,y)*ddpot_w_halo(d_ab,nr1,nr2,nr3)   &
          !
          &     +a(1,1,x)*a(1,1,y)*a(1,1,z)*dddpot_w_halo(d_abc,  1,  1,  1) &
          &     +a(1,1,x)*a(1,1,y)*a(1,2,z)*dddpot_w_halo(d_abc,  1,  1,nr3) &
          &     +a(1,1,x)*a(1,1,z)*a(1,2,y)*dddpot_w_halo(d_abc,  1,nr2,  1) &
          &     +a(1,1,x)*a(1,2,y)*a(1,2,z)*dddpot_w_halo(d_abc,  1,nr2,nr3) &
          &     +a(1,1,y)*a(1,1,z)*a(1,2,x)*dddpot_w_halo(d_abc,nr1,  1,  1) &
          &     +a(1,1,y)*a(1,2,x)*a(1,2,z)*dddpot_w_halo(d_abc,nr1,  1,nr3) &
          &     +a(1,1,z)*a(1,2,x)*a(1,2,y)*dddpot_w_halo(d_abc,nr1,nr2,  1) &
          &     +a(1,2,x)*a(1,2,y)*a(1,2,z)*dddpot_w_halo(d_abc,nr1,nr2,nr3)
          !
          pot_stdcg(ig1,ig2,ig3) = u+v+w-uv-vw-uw+uvw
        end do ! ig1
      end do ! ig2
    end do ! ig3
    return
  end subroutine perform_hermite_cubic_tfi_for_near_field_pot_old

  subroutine  finalize_tfi()
    implicit none
    if (allocated(pot_w_halo))    deallocate(pot_w_halo)
    if (allocated(dpot_w_halo))   deallocate(dpot_w_halo)
    if (allocated(ddpot_w_halo))  deallocate(ddpot_w_halo)
    if (allocated(dddpot_w_halo)) deallocate(dddpot_w_halo)
    return
  end subroutine finalize_tfi

end module transfinite_interpolation
